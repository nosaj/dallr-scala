// /**
//  * Created by Asbjørn on 20-04-14.
//  */
//
//
// $(document).ready(function () {
//     dlUserListTask();
// });
//
// var type;
//
// function setTaskAssignee(){
//     var thisUser = $(this).find(".autoCompEmail").text();
//     if(type==1){
//         $('#newTaskAssignee').val(thisUser);
//         $('#taskSearchresult').hide()
//     }
//     else{
//         $('#newTaskAssignee2').val(thisUser);
//         $('#taskSearchresult2').hide()
//     }
//     //var thisUser = $(this).find(".autoCompName").text();
//
// }
//
// function escapeHtml(text) {
//     var map = {
//         '&': '&amp;',
//         '<': '&lt;',
//         '>': '&gt;',
//         '"': '&quot;',
//         "'": '&#039;'
//     };
//     if(text == null || text == ""){
//         return text
//     }
//     else{
//         return text.replace(/[&<>"']/g, function(m) { return map[m]; });
//     }
//
// }
//
// function dlUserListTask() {
//     $.ajax({ url: "/autoComplete/findUserNames", success: function(data) {
//         $(".searchbox-task").keyup(function () {
//
//             var users = data;
//             var usersUnicoded = data;
//
//             if($(this).attr('id')=="newTaskAssignee"){
//                 var displaydiv = document.getElementById("taskSearchresult");
//                 var sTerm = document.getElementById("newTaskAssignee").value;
//                 var searchbox = document.getElementById("newTaskAssignee");
//                 type = 1;
//             }
//             else{
//                 var displaydiv = document.getElementById("taskSearchresult2");
//                 var sTerm = document.getElementById("newTaskAssignee2").value;
//                 var searchbox = document.getElementById("newTaskAssignee2");
//                 type = 2;
//
//             }
//
//             var mapObj = {
//                 æ: "ae",
//                 ø: "oe",
//                 å: "aa",
//                 Æ: "AE",
//                 Ø: "OE",
//                 Å: "AA"   };
//             sTerm = sTerm.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
//                 return mapObj[matched];
//             });
//
//             //remove old suggestions from the dropdown
//             while (displaydiv.firstChild) {
//                 displaydiv.removeChild(displaydiv.firstChild);
//             }
//             if (sTerm == "") {
//                 displaydiv.style.display = "none";
//             }
//
//             for (var i = 0; i < users.length; i = i + 1) {
//                 var regXSearch = "\\b" + sTerm;
//                 var regX = new RegExp(regXSearch, "gi");
//
//                 var firstnameUnicoded = usersUnicoded[i]["User"]["firstname"];
//                 var lastnameUnicoded = usersUnicoded[i]["User"]["lastname"];
//                 var jobTitleUnicoded = usersUnicoded[i]["User"]["jobtitle"];
//                 var emailUnicoded = usersUnicoded[i]["User"]["username"];
//
//                  firstnameUnicoded = firstnameUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
//                  return mapObj[matched];
//                  });
//                  lastnameUnicoded = lastnameUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
//                  return mapObj[matched];
//                  });
//                  jobTitleUnicoded = jobTitleUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
//                  return mapObj[matched];
//                  });
//                  emailUnicoded = emailUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
//                     return mapObj[matched];
//                 });
//
//                  var firstNameCheck = regX.test(firstnameUnicoded);
//                  var lastNameCheck = regX.test(lastnameUnicoded);
//                  var jobTitleCheck = regX.test(jobTitleUnicoded);
//                  var bothNames = firstnameUnicoded + " " + lastnameUnicoded;
//                  var bothNamesCheck = regX.test(bothNames);
//                  var emailCheck = regX.test(emailUnicoded);
//
//                 if (firstNameCheck || lastNameCheck || jobTitleCheck || bothNamesCheck|| emailCheck) {
//
//                     var userId = users[i]["User"]["_id"];
//                     var firstname = escapeHtml(users[i]["User"]["firstname"]);
//                     var lastname = escapeHtml(users[i]["User"]["lastname"]);
//                     var job = escapeHtml(users[i]["User"]["jobtitle"]);
//                     var image = users[i]["User"]["img"];
//                     var email = users[i]["User"]["username"];
//
//                     if(firstname=="?"){
//                         firstname=email;
//                         lastname="";
//                     }
//                     if(job=="?"){
//                         job="No jobtitle";
//                     }
//
//                     var boldsTerm = '<b>' + sTerm + '</b>';
//                     var final_fname = firstname;
//                     var final_lname = lastname;
//                     var final_job = job;
//                     var final_email = email;
//
//                     //either make it static so we always have a string starting with big case and folloewd by small case.
//                     //or cahnge to function to just make the entire string bold. eg on firstname check, entire fn will be bold.
//
//                     //replace queried characters with bold characters (case sensitive so far)
//                      if(firstNameCheck){ final_fname = firstname.replace(sTerm, boldsTerm);}
//                      if(lastNameCheck){ final_lname = lastname.replace(sTerm, boldsTerm);}
//                      if(jobTitleCheck){ final_job = job.replace(sTerm, boldsTerm);}
//                      if(bothNamesCheck){final_fname = bothNames.replace(sTerm, boldsTerm); final_lname = ""; }
//                      if(emailCheck){final_email = email.replace(sTerm, boldsTerm);}
//
//
//                     var myLink = document.createElement('a');
//                     myLink.className = "taskLink";
//
//                     var myNameSpan = document.createElement('span');
//                     myNameSpan.setAttribute("style", "font-size:14px;");
//                     myNameSpan.innerHTML = final_fname + " " + final_lname;
//                    // myNameSpan.innerHTML = '<br/>' + final_email;
//                     myNameSpan.className = "autoCompName";
//
//                     var myJobSpan = document.createElement('span');
//                     myJobSpan.setAttribute("style", "font-size:12px;color:#999");
//                     myJobSpan.innerHTML = '<br/>' + final_job;
//
//                     var myMailSpan = document.createElement('span');
//                     myMailSpan.setAttribute("style", "display:none");
//                     myMailSpan.innerHTML = final_email;
//                     myMailSpan.className = "autoCompEmail";
//
//                     var myIdSpan = document.createElement('span');
//                     myIdSpan.setAttribute("style", "display:none");
//                     myIdSpan.innerHTML = userId;
//                     myIdSpan.className = "autoCompId";
//
//                     var myImgDiv = document.createElement('img');
//                     var imageSrc = image;
//                     myImgDiv.setAttribute("src", imageSrc);
//
//                     var myDiv = document.createElement('div');
//                     myDiv.className = "autoCompResultTask";
//
//                     myDiv.appendChild(myImgDiv);
//                     myDiv.appendChild(myNameSpan);
//                     myDiv.appendChild(myMailSpan);
//                     myDiv.appendChild(myIdSpan);
//                     myDiv.appendChild(myJobSpan);
//
//                     myLink.appendChild(myDiv);
//                     displaydiv.appendChild(myLink);
//
//                     if (sTerm != "") {
//                         displaydiv.style.display = "block";
//                     }
//                 }
//             }
//             $('.taskLink').click(setTaskAssignee);
//         });
//     }, dataType: "json" });
// }