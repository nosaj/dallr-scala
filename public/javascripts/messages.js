/*** TODO:
 * - Make messages sorted by date.
 * - Multiple chatters (3+) should be able to write in messages.
 * - Make it get new messages through akka + querying the db.
 * - Enable channels in the blockchain branch
 * ***/



var ORGFILENAMEID = "**orgFile**";

// var visabilityState = 1;
// var bNotification = notificationSettings();

$(document).ready(function () {

    // WS TESTING START
    var mozOrChrome = window['MozWebSocket'] ? MozWebSocket : WebSocket;
    // var ws = new mozOrChrome('ws://localhost:' + location.port + '/messagesocket');
    var ws = new mozOrChrome('ws://dallr-app.westeurope.cloudapp.azure.com:' + location.port + '/messagesocket'); // PRODUCTION
    ws.onopen = function (message) {
        var jsonToSend = JSON.stringify({"type": "subscribe"});
        ws.send(jsonToSend);
    };

    ws.onmessage = function (e) {
        jsonMessage = JSON.parse(e.data);
        if (jsonMessage.type == "messageSent") {
            if (jsonMessage.status != "failure") {
                console.log(jsonMessage);
                displayMsg(jsonMessage, jsonMessage.parentID.$oid);
                console.log(jsonMessage);
                $('html').removeClass('wait-mouse-cursor');
                $('#sendMessage').show();
                $('.write-message .spinner').hide(0);
                BCValidation(jsonMessage.parentID.$oid, jsonMessage._id.$oid);
            } else {
                $(".message-to p:not(.send-message-to)").remove();
                $(".input-container #new-message-input").val("");

                $(".messages.block.write-message").hide();
                $(".right-container.container .emptyUsernames").show();

                $('.messages-container').show();
                $('.message-to.input-container').hide();
                $('.write-message-user').hide();

                $('#cancelSendMessage').hide();
                $('.write-message .spinner').hide(0);
                noty({text: 'Error sending message!', timeout: 2000, type: 'error'});
                $('html').removeClass('wait-mouse-cursor');
                $('#sendMessage').show();
            }
        } else if (jsonMessage.type == "newMessage") {
            var newestMessage = jsonMessage.data.messages[jsonMessage.data.messages.length - 1];
            displayMsg(newestMessage, jsonMessage.data._id.$oid, true)
            BCValidation(jsonMessage.data._id.$oid, newestMessage._id.$oid);
        }
    };


    // WS TESTING END

//
//     /******* VARIABLES *********************************************************************/
//
//         // var socket =  io.connect('http://55.55.55.55:3000'); // we get from socket.io.js - DEVELOPMENT VERSION
// //    var socket =  io.connect('http://dallr.com:3000'); // we get from socket.io.js - PRODUCTION VERSION
//
//     var $chat = $('#chat');
//
//     //Selecting users for private chat
//     var id = null;
//
    var clickedID = '';
    var clickedUsername = '';
    var clickedUserImg = '';
    var clickedUserEmail = '';
//
    var myName = '';
    var sMyID = $('#sessionUser').val();
    var myUserImg = $(".profile-picture.small-profile-picture img").attr("src");
    var myUserEmail = '';
//
//     var optionUserText;
//     var optionUserName;
    var firstLaunch = true;
    var connectionOn = true;
    var jsonMessages;
    var unverifiedMessages = 0;
//
//     var originalFileName = '';
//
//     // socket.emit('new message user', 1, function(data){
//     //
//     //     if (!jQuery.isEmptyObject(data)) {
//     //
//     //         if(data.firstname === "?" || data.firstname === "?"){
//     //
//     //             sMyID = data._id;
//     //             var alternativeName = data.username;
//     //             alternativeName = alternativeName.slice(0, alternativeName.indexOf("@"));
//     //             myName = alternativeName;
//     //             if(data.img === undefined){
//     //                 myUserImg = "img/noimage.gif";
//     //             } else {
//     //                 myUserImg = data.img;
//     //             }
//     //
//     //
//     //         } else {
//     //             sMyID = data._id;
//     //             myName = data.firstname + " " + data.lastname;
//     //             if(data.img === undefined){
//     //                 myUserImg = "img/noimage.gif";
//     //             } else {
//     //                 myUserImg = data.img;
//     //             }
//     //         }
//     //
//     //         myUserEmail = data.username;
//     //
//     //     } else{
//     //         $("body").html('Something went wrong.');
//     //     }
//     //
//     // });
//     //
//
//     /******* Page load actions *********************************************************************/
//
//
//     /******* Tipsy ************************************************************************************/
    $('#message-input').autosize();
    $('.attach-icon-span').tipsy({offset: 5});

    $('.add-message-icon span').tipsy({offset: 5});

//
//     /******* INTERFACE Related EVENTS *********************************************************************/
//
//     $('#add-file-overlay .tag-add-button').click(saveUploadOverlay);
//     $('#add-file-overlay .tag-cancel-button').click(cancelUploadOverlay);
//     //$('#add-file-overlay input[type="text"]').click(initUploadFile(event));
//     $('#add-file-overlay input[type="text"]').click(function(event){
//         event.stopPropagation();
//         $('#add-file-overlay input[type=file]').click();
//     });
//     $('#add-file-overlay input[type=file]').change(showFileName);
//     $('.input-icon-attach').click(initUploadOverlay);
//     $('#overlay,#add-file-overlay .tag-cancel-button').click(hideUploadOverlay);
//     $('.menu-box-tab span').click(triggerInput());
//     $('.menu-box-messages li').click(toggleSelectedUserMessage);
    $('.add-message-icon span').click(initNewMessage);
    // $('.add-channel-icon span').click(initNewChannel);
    getUsers();
    getMessages();
    $('.cancelwallpost.button').click(cancelNewMessage);
    $('.input-container').on('click', "#cancelSendMessage", cancelNewMessage);
//     addActiveLink();
//
//     $('body').on({
//         mouseenter: function () {
//             $('.chatters-menu-number', this).hide();
//         },
//         mouseleave: function () {
//             $('.chatters-menu-number', this).show();
//         }
//     }, '.menu-box-menu li:not(:first-child)');
//
//     /******* SOCKET Related EVENTS *********************************************************************/
//
    $(".write-message").on("click", "button#sendMessage", function () {
        sendAndWrite();
    });
//
//     $('body').on("keyup",'#message-input', function(event){
//         if (event.keyCode === 13) {
//             sendAndWrite();
//         }
//     });
//
//     $(".right-container").on("click", "#new-chatters", function(){
//         createNewConversation();
//     });
//
//     $('body').on("keyup",'#new-message-input', function(event){
//         if (event.keyCode === 13) {
//             createNewConversation();
//         }
//     });
//
    $(".menu-box-menu").on("click", "li:not(:first)", function () { // TODOMESSAGE: MAKE THIS ONLY SHOW/HIDE INSTEAD OF APPEND.
        cancelNewMessage();
        chooseChatter($(this));
    });

//     $( ".message-user-search" ).keyup( function(){
//         var inputText = $(".message-user-search").val();
//
//         $('.menu-box-tab .message-name-user').each(function(){
//
//             if(inputText === $(this).text().slice(0, inputText.length)){
//                 $(this).parent().parent().parent().show();
//             } else {
//                 $(this).parent().parent().parent().hide();
//             }
//
//         });
//
//     });
//
// //    ##THIS the function to search inside messages + (start new conversation) it is atm linked to the #searchbox which it should not.
// //    Todo find another place to use it, if needed
//
// //    $("body").on("keyup", "#searchbox.message-new-user-search", function(){
// //        var inputText = $(".message-new-user-search").val();
// //        if(inputText.length === 0){
// //
// //            $(".write-message-user img").each(function(){
// //                $(this).show();
// //            });
// //        } else {
// //            initNewMessage();
// //            $(".write-message-user img").each(function(){
// //
// //                if(inputText === $(this).attr("original-title").toString().slice(0, inputText.length)){
// //                    $(this).show();
// //                } else {
// //                    $(this).hide();
// //                }
// //            });
// //        }
// //
// //    });
//
//     /*******CREATE a type helper function ***************/
//
    $("body").on("click", ".write-message-user img", function (event) {
        var name = $(this).attr('original-title');
        var id = $(this).attr('original-id');
        clickedID = $(this).attr('original-email');
//        var inputVal = $(".input-container.message-to input.name").val();
        var inputVal = $(".input-container #message-to-input-id p:not(.send-message-to)").text();

        var inputArray = inputVal.split(',');
        inputArray = $.map(inputArray, $.trim);


        if (inputArray.length !== 0) {

            var bExists = false;

            for (var i = 0; i < inputArray.length; i++) {

                if (id === inputArray[i]) {
                    noty({
                        text: 'You hava already choosen this user', timeout: 2000
                    });

                    bExists = true;

                    return;
                }

            }
        }

        if (!bExists) {
            var nameFromID = $('.write-message-user img[original-id="' + id + '"]').attr('original-title');
            $("#message-to-input").append("<p><i class='fa fa-times'></i>" + nameFromID + ",</p>");
            $("#message-to-input-id").append("<p><i class='fa fa-times'></i>" + id + ",</p>");

        }

        event.stopImmediatePropagation();
        return false;

    });

    $("body").on("click", "#message-to-input p i", function () {
        // TODO:MAKE USERS CLICKABLE AGAIN AFTER BEING REMOVED
        $(this).parent().remove();
    });



//     $(".menu-box-menu").on("click", "a.btn-archive", function (event) {
//         $('a.btn-archive').hide();
//         var sConversationID = '';
//         sConversationID = $(this).parent().parent().attr('id');
//
//         noty({
//             text: 'Are you sure you want to archive this conversation?',
//             buttons: [
//                 {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
//                     archiveConversation(sMyID, sConversationID);
//                     $('a.btn-archive').show();
//                     $noty.close();
//
//
//                 }},
//                 {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
//                     $('a.btn-archive').show();
//                     $noty.close();
//                 }}
//             ]
//         });
//     });
//
//
//
//
//
//
    function getMessages() {
        $.ajax({
            url: "/getmessages"
        }).success(function (result) {
            jsonMessages = result;
            renderMessages(result);
            appendMessagesToDOM();
            // $('.bc-pass').tipsy({offset: 5});
            // $('.bc-fail').tipsy({offset: 5});
            // $('.bc-new').tipsy({offset: 5});
        }).fail(function (jqXHR, textStatus, errorThrown) {

        }).always(function () {

        });
    }

    function getUsers() {
        $.ajax({
            url: "/getusers"
        }).success(function (result) {
            renderUsers(result);
        }).fail(function (jqXHR, textStatus, errorThrown) {

        }).always(function () {

        });
    }


    function renderUsers(users) {
        var sessionUser = $('#sessionUser').val();
        var numberOfChatters = 0;
        for (var i = 0; i < users.length; i++) {
            if (sessionUser !== users[i].username) {
                if (users[i].firstname === "?" || users[i].lastname === "?") {
                    numberOfChatters++;
                    var img = users[i].image;
                    img = img.trim();
                    var alternativeName = users[i].username;
                    alternativeName = alternativeName.slice(0, alternativeName.indexOf("@"));
                    $('.write-message-user').append('<img original-id="' + users[i]._id.$oid + '" original-title="' + alternativeName + '" original-email="' + users[i].username + '" title="' + alternativeName + '" width="60" height="60" src="' + img + '" alt="Profile-Image">');
                    $('.write-message-user.messages img').tipsy({offset: 5});
                } else {
                    numberOfChatters++;
                    var img = users[i].image;
                    img = img.trim();
                    $('.write-message-user').append('<img original-id="' + users[i]._id.$oid + '" original-title="' + users[i].firstname + " " + users[i].lastname + '" original-email="' + users[i].username + '"title="' + users[i].firstname + " " + users[i].lastname + '" width="60" height="60" src="' + img + '" alt="Profile-Image">');
                    $('.write-message-user.messages img').tipsy({offset: 5});
                }
            } else {
                // sMyID = users[i]._id.$oid;
                myName = users[i].firstname + ' ' + users[i].lastname;
                myUserEmail = users[i].username;
            }
        }

        if (numberOfChatters === 0) {
            $('.write-message-user').append('<p>"No other users yet."</p>');
        }
    }

    function diffDaysFunc(date1, date2) {
        return date1.getDay() - date2.getDay();
    }

    function renderMessages(data) {
        if (data.length === 0) {
            $(".messages.block.write-message").hide();
            $(".right-container.container .emptyUsernames").show();
        } else {
            $(".messages.block.write-message").show();
            $(".right-container.container .emptyUsernames").hide();
        }

        $('.menu-box-menu li:not(:first-child)').remove();
        var chatterID = '';
        var chatterName = '';
        var iChatterLength = '';
        var mostRecentChatID = '';
        if ($(".menu-box-menu > li").length > 0) {
            $('.menu-box-menu > li').not(":first").remove();
        }

        for (var i = 0; i < data.length; i++) {
            var sMultiChatterID = '';
            var sMultiChatterName = '';
            var status = '';
            var last_message = data[i].messages[data[i].messages.length - 1];
            var d = '';
            var today = new Date();
            var diffDay = '';
            var validatedTopMessage;
            //= data[i].errorBC === "exists" ? "<i class='bc-top bc-pass fa fa-link' aria-hidden='true' original-title='Verified By Blockchain'></i>" : data[i].errorBC === "new" ? "<i class='bc-top bc-new fa fa-link' aria-hidden='true' original-title='Awaiting Blockchain Verification'></i>" : "<i class='bc-top bc-fail fa fa-link' aria-hidden='true' original-title='Failed Blockchain Verification'></i>";

            var output = '';
            var isArchived = false;

            if (checkIfArchived(data[i].users, sMyID)) {
                continue;
            }

            for (var j = 0; j < data[i].users.length; j++) {

                iChatterLength = data[i].users.length;
                last_message.msg = last_message.msg.replace("&lt;br/&gt;", ' ');

                if (data[i].users[j].username !== sMyID && data[i].users.length === 2) {

                    chatterID = data[i].users[j].username;
                    // chatterID = data[i].users[j].username.replace("@", "AT").replace(".", "DOT");

                    chatterName = data[i].users[j].username;

                    if (i === 0) {
                        mostRecentChatID = chatterID;
                    }

                    if (data[i].online === 1) {
                        status = "online";
                    } else {
                        status = "offline";
                    }

                    if (last_message.date.$date !== undefined) {
                        d = new Date(last_message.date);
                    } else {
                        d = new Date();
                    }
                    // diffDay = diffDaysFunc(today, d)
                    // diffDay = Math.abs(today.diffDays(d));

                    output += '<li>';
                    output += '<div class="menu-box-tab">';
                    output += '<div original-chatter="' + chatterID + '" class="chat-person-container">';
                    var imageLink = $('.write-message-user img[original-id="' + chatterID + '"]').attr('src');

                    if (imageLink == undefined) {
                        imageLink = '/assets/images/noImage.gif';
                    }
                    output += '<img class="profileImage" width="40" height="40" src="' + imageLink + '" alt="Profile-Image">';
                    output += '<span class="message-name-user">' + fitString(chatterName, 15) + '</span>';
                    output += '<div class="message-time-icon">';
                    output += '<span class="fontawesome-time scnd-font-color"></span>';
                    output += '</div>';
                    output += '<input class="convoID" id="' + data[i]._id + '"type="hidden"/>';

                    if (diffDay < 1) {
                        output += '<span class="message-date-user">' + addZero(d.getHours()) + ":" + addZero(d.getMinutes()) + '</span>';
                    } else {
                        output += '<span class="message-date-user">' + Math.floor(diffDay) + " days ago" + '</span>';
                    }

                    if (last_message.from === chatterID) {

                        var archiveCheck = checkForArchived(last_message.msg);

                        if (archiveCheck === "none") {
                            output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString(last_message.msg, 15) + '</span>';
                        } else if (archiveCheck === "archived") {
                            output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone left conversation", 15) + '</span>';
                        } else if (archiveCheck === "unarchived") {
                            output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone joined the conversation", 15) + '</span>';
                        }

                    } else {

                        var archiveCheck = checkForArchived(last_message.msg);

                        if (archiveCheck === "none") {
                            output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString(last_message.msg, 15) + '</span>';
                        } else if (archiveCheck === "archived") {
                            output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone left conversation", 15) + '</span>';
                        } else if (archiveCheck === "unarchived") {
                            output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone joined the conversation", 15) + '</span>';
                        }

                    }


                    output += '<a class="chatters-menu-number"></a>';
                    output += "<i id='" + data[i]._id + "_bc-top' class='bc-top fa fa-link' aria-hidden='true'></i>";
                    // output += '<div class="msg-archive"><a class="btn-archive" href="#"><i class="fa fa-times"></i></a></div>';FOR DELETEING MESSAGES
                    output += '</div>';
                    output += '</div>';
                    output += '</li>';
                    $('#messages-panel .menu-box-menu').append(output);
                    $("#" + data[i]._id).siblings(".bc-top").tipsy({offset: 5});


                } else if (data[i].users.length > 2) {
                    sMultiChatterID += "_" + data[i].users[j].id.toString();

                    if (hasWhiteSpace(data[i].users[j].name)) {
                        sMultiChatterName += data[i].users[j].name.substr(0, data[i].users[j].name.indexOf(' ')) + ", ";
                    } else {
                        sMultiChatterName += data[i].users[j].name + ", ";
                    }
                }
            }

            if (iChatterLength > 2) {

                chatterID = sMultiChatterID;
                chatterName = sMultiChatterName.substr(0, sMultiChatterName.length - 2);

                if (i === 0) {
                    mostRecentChatID = sMultiChatterID;
                }

                if (data[i].online === 1) {
                    status = "online";
                } else {
                    status = "offline";
                }


                if (last_message.date.$date !== undefined) {
                    d = new Date(last_message.date);
                } else {
                    d = new Date();
                }

                // diffDay = Math.abs(today.diffDays(d));
                output += '<li>';
                output += '<div class="menu-box-tab">';
                output += '<div id="' + chatterID + '" class="chat-person-container">';
                output += '<img class="profileImage" width="40" height="40" src="img/group.png" alt="Profile-Image">';
                output += '<span class="message-name-user" title="' + chatterName + '">' + fitString(chatterName, 15) + '</span>';
                output += '<div class="message-time-icon">';
                output += '<span class="fontawesome-time scnd-font-color"></span>';
                output += '</div>';

                if (diffDay < 1) {
                    output += '<span class="message-date-user">' + addZero(d.getHours()) + ":" + addZero(d.getMinutes()) + '</span>';
                } else {
                    output += '<span class="message-date-user">' + Math.floor(diffDay) + " days ago" + '</span>';
                }

                if (last_message.from === chatterID) {
                    var archiveCheck = checkForArchived(last_message.msg);

                    if (archiveCheck === "none") {
                        output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString(last_message.msg, 15) + '</span>';
                    } else if (archiveCheck === "archived") {
                        output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone left conversation", 15) + '</span>';
                    } else if (archiveCheck === "unarchived") {
                        output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone joined the conversation", 15) + '</span>';
                    }
                } else {
                    var archiveCheck = checkForArchived(last_message.msg);

                    if (archiveCheck === "none") {
                        output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString(last_message.msg, 15) + '</span>';
                    } else if (archiveCheck === "archived") {
                        output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone left conversation", 15) + '</span>';
                    } else if (archiveCheck === "unarchived") {
                        output += '<span class="message-preview-user"><i class="fa fa-arrow-right"></i>' + fitString("someone joined the conversation", 15) + '</span>';
                    }
                }

                output += '<a class="chatters-menu-number"></a>';
                output += '<div class="msg-archive"><a class="btn-archive" href="#"><i class="fa fa-times"></i></a></div>';
                output += '</div>';
                output += '</div>';
                output += '</li>';

                $('.menu-box-menu').append(output);
            }
        }

        if (clickedID !== "") {
            pickChatter(clickedID);

        }

        //Activate first conversation
        setTimeout(function () {
            //TODOMESSAGE: MAKE THIS ONLY MAKE FIRST MESSAGE VISIBLE AND NOT APPEND IT. BY FIXING CLICKED FUNCTION
            if (firstLaunch) {
                clickedID = String(mostRecentChatID);
                clickedUsername = $("img[original-id='" + mostRecentChatID + "']").attr('original-title');
                clickedUserEmail = $("img[original-id='" + mostRecentChatID + "']").attr('original-email');

                if (clickedUsername === undefined) {
                    clickedUsername = "No name";
                }

                clickedUserImg = $("img[original-id='" + mostRecentChatID + "']").attr('src');

                $('textarea#message-input').val('');
                if (mostRecentChatID != "") {

                    $(".search-users-li").next().click();
                }

                if (clickedID != "") {
                    // socket.emit('choose chatter', clickedID, function(data){
                    //     $chat.append('<span class="error">' + data + '</span><br/>');
                    //     var imgUrl = $('.write-message-user img[original-id="' + clickedID + '"]').attr('src');
                    //     clickedUserImg = $(this).attr('src', imgUrl);
                    //
                    // });
                }

                $('#message-input').val(''); //clear messageBox

                firstLaunch = false;
            }

        }, 1000);

        $('.menu-box-menu  span.message-name-user').tipsy({offset: 5});
    }

    //     /******* SOCKET LISTENERS *********************************************************************/


//     // socket.on('load old message', function(docs){
//     //
//     //     $('#messageBox').empty();
//     //
//     //     users = [];
//     //
//     //     for (var i = 0; i < docs[0].users.length; i++) {
//     //         if (docs[0].users[i].id !== sMyID) {
//     //             users.push(docs[0].users[i]);
//     //         }
//     //     }
//     //
//     //     docs[0].messages.reverse();
//     //
//     //     for (var k = docs[0].messages.length - 1; k >= 0; k--) {
//     //         displayMsg(docs[0].messages[k]);
//     //     }
//     //
//     // });
//
//     // socket.on('conversationinfo', function (docs) {
//     //     var today = new Date();
//     //
//     //     for (var i = 0; i < docs.length; i++) {
//     //
//     //         var date = new Date(docs[i].created);
//     //
//     //         var diffDay = Math.abs(today.diffDays(date));
//     //
//     //         if (diffDay < 1) {
//     //             $('.menu-box-menu #' + docs[i]._id + ' .message-date-user').text(date.toString("HH:mm"));
//     //         } else {
//     //             $('.menu-box-menu #' + docs[i]._id + ' .message-date-user').text(Math.floor(diffDay) + " days ago");
//     //         }
//     //
//     //         $('.menu-box-menu #' + docs[i]._id + ' .message-preview-user').text(fitString(docs[i].lastmsg, 15));
//     //
//     //     }
//     //
//     // });
//
//
//     // socket.on('unreadCount', function (data) {
//     //
//     //     objData = JSON.parse(data);
//     //
//     //     for (var i = 0; i < objData.length; i++) {
//     //
//     //         if (objData[i].count === 0) {
//     //             $('div#' + objData[i].id + ' .chatters-menu-number').text('');
//     //         } else if(objData[i].count !== 0){
//     //             $('div#' + objData[i].id + ' .chatters-menu-number').text(objData[i].count);
//     //         }
//     //
//     //     }
//     //
//     // });
//
//     // socket.on('new message', function (data, cb) {
//     //
//     //
//     //     optionUserText = $("#availableUsers option[value='" + clickedID + "']").text();
//     //     optionUserImg = $("img[original-id='" + data.from +"']").attr('src');
//     //     if(optionUserImg == undefined){
//     //         optionUserImg = "/img/noImage.gif";
//     //     }
//     //     optionUserName = optionUserText.split(':')[0];
//     //     if(visabilityState === 0){
//     //         notification(data.nick, data.msg, optionUserImg);
//     //         var snd = new Audio("../notification.mp3");
//     //         snd.play();
//     //     }
//     //
//     //     if (clickedID === data.toUser) {
//     //         displayMsg(data);
//     //         cb(true);
//     //     } else {
//     //         if(data.toUser !== undefined){
//     //             var date = new Date();
//     //             $("#" + data.toUser + " span.message-preview-user").html('<i class="fa fa-arrow-left"></i>' + fitString(data.msg, 15));
//     //             $("#" + data.toUser + " span.message-date-user").html(date.toString("HH:mm"));
//     //
//     //         }
//     //         cb(false);
//     //     }
//     //
//     // });
//
//
//     /******* FUNCTIONS *********************************************************************/
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function renderDate(date) {
        var h = addZero(date.getHours());
        var m = addZero(date.getMinutes());
        var d = addZero(date.getDate());
        var mo = addZero(date.getMonth() + 1);
        var y = addZero(date.getFullYear());
        return h + ":" + m + " " + d + "/" + mo + "/" + y;
    }

    function displayMsg(data, messageID, awaitingVerification) {
        awaitingVerification = typeof awaitingVerification !== 'undefined' ? awaitingVerification : false;
        if (!$("#convo_" + messageID).length) {
            $("div.right-container").append("<div class='messageBox' id='convo_" + messageID + "'></div>");
        }
        var dateObj;
        if (data.date !== undefined) {
            dateObj = new Date(1490004477130);

        } else {
            dateObj = new Date();
        }
        var messageBlockChain = data.errorBC;
        console.dir(messageBlockChain);
        var nestedMessageID = data._id.$oid;
        if (messageBlockChain != true)
            unverifiedMessages++;
        var validatedMessage = messageBlockChain === true ? "<i id='verified_bc_" + nestedMessageID + "' class='bc-pass fa fa-link' aria-hidden='true' original-title='Verified By Blockchain'></i>" : messageBlockChain === "new" || awaitingVerification ? "<i id='verified_bc_" + nestedMessageID + "' class='bc-new fa fa-link' aria-hidden='true' original-title='Awaiting Blockchain Verification'></i>" : "<i id='verified_bc_" + nestedMessageID + "' class='bc-fail fa fa-link' aria-hidden='true' original-title='Failed Blockchain Verification'></i>";
        // if(users.length > 1){
        clickedUsername = $("img[original-id='" + data.from + "']").attr('original-title');
        clickedUserEmail = $("img[original-id='" + data.from + "']").attr('original-email');
        if (clickedUsername === undefined) {
            clickedUsername = data.nick;
            clickedUserImg = $("img[original-title='" + data.nick + "']").attr('src');
            clickedUserEmail = $("img[original-title='" + data.nick + "']").attr('original-email');
            if (clickedUserImg == undefined) {
                clickedUserImg = '/assets/images/noImage.gif';
            }
            // } else {
            //     clickedUserId = $("img[original-id='" + data.from +"']").attr('src');
            //     clickedUserImg = $("img[original-title='" + data.nick +"']").attr('src');
            // }
            //
            // if(clickedUsername == undefined || clickedUsername == "No name"){
            //     clickedUsername = "No longer in this platform";
            //     clickedUserEmail = '';
            //     clickedUserImg =  "/img/noImage.gif";
            // }
        }


//         //TODO this part could be swapped with a more solid solution
//         // Atm we a looking um the user we using for two purposes
//         // Getting image
//         // Getting name so we can compare to see who is myself and conversationist


        data.msg = replaceURLWithHTMLLinks(data.msg);
        data.msg = data.msg.replace("&lt;br/&gt;", '<br/>');

        // if (clickedID !== '') {
        var lastMessageName = $("#convo_" + messageID + " .messages-container .middle-text-div a").first().text();
        var lastMessageID = $('#convo_' + messageID + ' .messages-container a').first().attr('class');
            var archiveCheck = checkForArchived(data.msg);
        var lastMsgArch = $("#convo_" + messageID + " .messages-container").first().hasClass("archived-conversation");

            // Todo check if this should be strings instead of int

            if (sMyID !== data.from) {

                if (archiveCheck === "archived") {
                    $("#convo_" + messageID).prepend('<div class="messages-container archived-conversation">' +
                        '<div class="messages block individual-message">' +
                        '<div class="input-container"><p class="messageText"><i class="fa fa-minus-circle"></i>' + clickedUsername + ' ' + validatedMessage + ' ' + data.msg + '</p></div>' +
                        '</div>' +
                        '</div>');
                    $("#convo_" + messageID + " .bc").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                    return;
                } else if (archiveCheck === "unarchived") {
                    $("#convo_" + messageID).prepend('<div class="messages-container archived-conversation">' +
                        '<div class="messages block individual-message">' +
                        '<div class="input-container"><p class="messageText"><i class="fa fa-plus-circle"></i>' + clickedUsername + ' ' + validatedMessage + ' ' + data.msg + '</p></div>' +
                        '</div>' +
                        '</div>');
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                    return;
                }

//                if(lastMessageName === clickedUsername && lastMsgArch === false){
                if (lastMessageID === data.from && lastMsgArch === false) {
                    $("#convo_" + messageID + " .messages-container .messageText").first().append("<br><br>" + validatedMessage + ' ' + data.msg);
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                } else {
                    if (clickedUsername === undefined || clickedUsername === "No name") {
                        clickedUsername = $('.menu-box-tab #5465c1d40f8709ef0f8b4568 .message-name-user').text();
                        clickedUserImg = $('.menu-box-tab #5465c1d40f8709ef0f8b4568 img').attr('src');
                    }
                    var prependOutput = '';
                    prependOutput += '<div class="messages-container">';
                    prependOutput += '<div class="messages block individual-message">';
                    prependOutput += '<div class="input-container">';
                    prependOutput += '<div class="profileTopBarRight">';
                    prependOutput += '<div class="message-time-icon">';
                    prependOutput += '<span class="fontawesome-time scnd-font-color"></span>';
                    prependOutput += '<span class="message-date-user">' + renderDate(dateObj) + '</span>';
                    prependOutput += '</div>';
                    prependOutput += '</div>';
                    prependOutput += '<div class="profileTopBarLeft">';
                    if (clickedUserImg == undefined) {
                        clickedUserImg = '/assets/images/noImage.gif';
                    }
                    prependOutput += '<img class="profileImage" alt="Profile-Image" src="' + clickedUserImg + '">';
                    prependOutput += '<div class="middle-text-div">';
                    prependOutput += '<a href="/profiles/' + data.from + '" class="' + data.from + '">' + data.from + '</a>';
                    prependOutput += '</div>';
                    prependOutput += '</div>';
                    prependOutput += '<div class="profileMiddleBar">';
                    prependOutput += '<blockquote>';
                    prependOutput += '<p class="messageText">' + validatedMessage + ' ' + data.msg + '</p>';
                    prependOutput += '</blockquote>';
                    prependOutput += '</div>';
                    prependOutput += '</div>';
                    prependOutput += '</div>';
                    prependOutput += '</div>';
                    $("#convo_" + messageID).prepend(prependOutput);
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                }
            } else {
                console.log(data.msg);

                if (archiveCheck === "archived") {
                    $("#convo_" + messageID).prepend('<div class="messages-container archived-conversation">' +
                        '<div class="messages block individual-message">' +
                        '<div class="input-container"><p class="messageText"><i class="fa fa-minus-circle"></i>' + myName + ' ' + validatedMessage + ' ' + data.msg + '</p></div>' +
                        '</div>' +
                        '</div>');
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                    return;
                } else if (archiveCheck === "unarchived") {
                    $("#convo_" + messageID).prepend('<div class="messages-container archived-conversation">' +
                        '<div class="messages block individual-message">' +
                        '<div class="input-container"><p class="messageText"><i class="fa fa-plus-circle"></i>' + myName + ' ' + validatedMessage + ' ' + data.msg + '</p></div>' +
                        '</div>' +
                        '</div>');
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                    return;
                }

                if (lastMessageID === sMyID && lastMsgArch === false) {
                    $("#convo_" + messageID + " .messages-container .messageText").first().append("<br><br>" + validatedMessage + ' ' + data.msg);
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                } else {
                    $("#convo_" + messageID).prepend('' +
                        '<div class="messages-container">' +
                        '<div class="messages block individual-message myself">' +
                        '<div class="input-container">' +
                        '<div class="profileTopBarRight">' +
                        '<div class="message-time-icon">' +
                        '<span class="fontawesome-time scnd-font-color"></span>' +
                        '<span class="message-date-user">' + renderDate(dateObj) + '</span>' +
                        '</div>' +
                        '</div>' +
                        '<div class="profileTopBarLeft">' +
                        '<img class="profileImage" alt="Profile-Image" src="' + myUserImg + '">' +
                        '<div class="middle-text-div">' +
                        '<a href="/profiles/' + myUserEmail + '" class="' + sMyID + '">' + myUserEmail + '</a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="profileMiddleBar">' +
                        '<blockquote>' +
                        '<p class="messageText">' + validatedMessage + ' ' + data.msg + '</p>' +
                        '</blockquote>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '');
                    $("#convo_" + messageID + " .bc-pass").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-fail").tipsy({offset: 5});
                    $("#convo_" + messageID + " .bc-new").tipsy({offset: 5});
                }
            }

        // } else {
        //     console.error("Display msg did not fullfill (clickedID !== '')");
        // }

    }

    function sendAndWrite() {
        var msg = $('textarea#comment').val();
//         var file = $('#add-file-overlay #my_file')[0].files[0];
//         // TODO: ADD FILE UPLOADS
//         var data = new FormData();
//
//         if (file && connectionOn == true){
//             data.append("data[Post][file]", file);
//             $('.messages #sendMessage').fadeOut(0);
//             $('.write-message .spinner').fadeIn(0);
//             $.ajax({
//                 type: "POST",
//                 url: "/messages/add",
//                 cache: false,
//                 dataType: "text",
//                 processData: false, // Don't process the files
//                 contentType: false, // Set content type to false as jQuery will tell the server its a query string request
//                 data: data
//             }).done(function (result) {
//                     sendMessage(msg, result);
//                 }).fail(function (jqXHR, textStatus, errorThrown) {
//                     noty({text: 'Error creating a message!', timeout: 4000, type: 'error'});
//                 }).always(function () {
// //                HIDE THE THROBBER & ADD EVENT LISTENER AFTER COMPLETION
//                     $('.write-message .spinner').fadeOut(0);
//                     $('.messages #sendMessage').fadeIn(0);
//                 });
//         } else {
//             sendMessage(msg);
//         }

        sendMessage(msg);
    }

    function appendMessagesToDOM() {
        var id;
        for (var i = 0; i < jsonMessages.length; i++) {
            id = jsonMessages[i]._id;
            if (jsonMessages[i].users.length === 2) {
                unverifiedMessages = 0;
                for (var j = 0; j < jsonMessages[i].messages.length; j++) {
                    // alert("here with i="+jsonMessages[i].users[0].username+" and "+jsonMessages[i].users[1].username);
                    displayMsg(jsonMessages[i].messages[j], id);
                }
                if (unverifiedMessages > 0)
                    $("#" + id + "_bc-top").addClass("bc-fail").attr("original-title", "Message has Failed Blockchain Verification");
                else
                    $("#" + id + "_bc-top").addClass("bc-pass").attr("original-title", "Message is Verified by Blockchain");
            }
        }
    }

    function chooseChatter(data) {
        clickedID = data.find(".chat-person-container").attr('original-chatter');
        $(".menu-box-menu .selected-user-message").removeClass("selected-user-message");
        data.addClass("selected-user-message");
        $(".messageBox").hide();
        $("#convo_" + data.find(".convoID").attr('id')).show();
    }

    function findMessagesByID(convoID) {
        for (var i = 0; i < jsonMessages.length; i++) {
            var id = jsonMessages[i]._id.$oid;
            if (id == convoID) {
                $('#messageBox').html("");
                for (var j = 0; j < jsonMessages[i].messages.length; j++) {
                    displayMsg(jsonMessages[i].messages[j]);
                }
            }
        }
    }
//     function createNewConversation(){
//         var inputVal = $(".input-container #message-to-input-id p:not(.send-message-to)").text();
//         inputVal = removeLastComma(inputVal);
//         var msg = $("textarea#new-message-input.text-input").val().trim();
//         var file = $('#add-file-overlay #my_file')[0].files[0];
//
//
//         var data = new FormData();
//
//         if (file){
//             data.append("data[Post][file]", file);
//             $('.messages #sendMessage').fadeOut(0);
//             $('.write-message .spinner').fadeIn(0);
//             $.ajax({
//                 type: "POST",
//                 url: "/messages/add",
//                 cache: false,
//                 dataType: "text",
//                 processData: false, // Don't process the files
//                 contentType: false, // Set content type to false as jQuery will tell the server its a query string request
//                 data: data
//             }).done(function (result) {
//                     sendMessageToNewConversation(msg, inputVal, result);
//                 }).fail(function (jqXHR, textStatus, errorThrown) {
//                     noty({text: 'Error creating a message!', timeout: 2000, type: 'error'});
//                 }).always(function () {
// //                HIDE THE THROBBER & ADD EVENT LISTENER AFTER COMPLETION
//                     $('.write-message .spinner').fadeOut(0);
//                     $('.messages #sendMessage').fadeIn(0);
//                 });
//         } else {
//             sendMessageToNewConversation(msg, inputVal);
//         }
//
//     }

    function fitString(string, maxStringLength) {
        if (string != undefined) {
            var newStr = string.replace(/\s+/g, '');
            if (newStr.length > maxStringLength) {
                string = string.substring(0, maxStringLength) + "...";
            }
            return string;
        } else {
            return string;
        }
    }

//
//     function getLastObjectInArray(array){
//         return array[array.length - 1];
//     }
//
//     function pickChatter(id){
//         $( ".menu-box-menu .selected-user-message" ).removeClass("selected-user-message");
//         $( ".menu-box-menu #"+ id +"" ).parent().parent().addClass("selected-user-message");
//
//         clickedID = id;
//
//         socket.emit('choose chatter', id, function(data){
//             $chat.append('<span class="error">' + data + '</span><br/>');
//             var imgUrl = $('.write-message-user img[original-id="' + clickedID + '"]').attr('src');
//             clickedUserImg = $(this).attr('src', imgUrl);
//         });
//     }
//
//     function pickChatterWithAddedCallback(id,cb){
//
//         pickChatter(id);
//
//         var lastArchived = $('#messageBox .archived-conversation').first().text()
//         if(lastArchived.indexOf('added') != -1){
//             cb(true);
//         } else if(lastArchived.indexOf('added') == -1){
//             cb(false);
//         }
//
//     }
//
//     function pickNewChatter(){
//         $( ".menu-box-menu .selected-user-message" ).removeClass("selected-user-message");
//         $( ".menu-box-menu li:nth-child(2)" ).addClass("selected-user-message");
//     }

    function BCValidation(parentMessageID, messageId) {
        //ajax message controller
        var counter = 0;
        var BCLongPolling = window.setInterval(function () {
            $.ajax({
                url: "/getBCValidation?parentId=" + parentMessageID + "&messageId=" + messageId
            }).success(function (result) {
                console.log(result);
                if (result === "true") {
                    // alert("#verified_bc_"+messageId);
                    $("#verified_bc_" + messageId).removeClass("bc-new").removeClass("bc-fail").addClass("bc-pass").attr("original-title", "Message is Verified by Blockchain");
                    $('.bc-pass').tipsy({offset: 5});
                    clearInterval(BCLongPolling);
                } else if (counter >= 10) {
                    $("#verified_bc_" + messageId).removeClass("bc-new").removeClass("bc-pass").addClass("bc-fail").attr("original-title", "Failed Blockchain Verification");
                }
                counter++;
            }).fail(function (jqXHR, textStatus, errorThrown) {

            }).always(function () {

            });
        }, 1000);
    }


    function sendMessage(message, file) {
        "use strict";
        $('#sendMessage').hide();
        $('.write-message .spinner').fadeIn(0);
        if (checkStringLength(message)) {
            if (clickedID !== '') {
                if (file !== undefined) {
                    message += '<br/>' + file + ORGFILENAMEID + originalFileName;
                }
                var newMessage = {
                    toUser: clickedID,
                    newMessage: message,
                    fromUser: sMyID
                };
                var jsonToSend = JSON.stringify({"type": "sendMsg", "to": clickedID, "message": message});
                ws.send(jsonToSend);
                if ($('.message-name-user').length == 0)
                    location.reload();
                $(".message-to p:not(.send-message-to)").remove();
                $(".input-container #new-message-input").val("");
                $(".messages.block.write-message-user").hide();
                $('.messages-container').show();
                $('.message-to.input-container').hide();
                $('#cancelSendMessage').hide();
                $('.write-message .spinner').hide(0);

                //$('#messageBox').hide(0);
                //$("#convo_"+clickedID).show();
            }
            //
            //         $.ajax({
            //             type: "POST",
            //             url: addURL
            //         }).done(function (result) {
            //             if (result) {
            //                 message = htmlEntities(message);
            //                 var myMessage = {
            //                     nick: myName,
            //                     msg: message,
            //                     from: sMyID,
            //                     date: {$date: new Date()}
            //                 };
            //                 $('#comment').val(''); //clear messageBox
            //                 $('#add-file-overlay input[type=file]').val("");
            //                 $('#add-file-overlay .text-input').val("");
            //                 $(".attach-icon-span").css({'color': '#9099b7'}).attr("original-title", "Attach a file");
            //                 displayMsg(myMessage, result.$oid);
            //                 $('#sendMessage').fadeIn(0);
            //                 $('.write-message .spinner').fadeOut(0);
            //             } else {
            //                 $(".message-to p:not(.send-message-to)").remove();
            //                 $(".input-container #new-message-input").val("");
            //
            //                 $(".messages.block.write-message").hide();
            //                 $(".right-container.container .emptyUsernames").show();
            //
            //                 $('.messages-container').show();
            //                 $('.message-to.input-container').hide();
            //                 $('.write-message-user').hide();
            //
            //                 $('#cancelSendMessage').hide();
            //                 $('.write-message .spinner').hide(0);
            //
            //                 noty({
            //                     text: 'You are not connected, try to refresh this page', timeout: 3000
            //                 });
            //                 connectionOn = false;
            //             }
            //         }).fail(function (jqXHR, textStatus, errorThrown) {
            //             console.dir(jqXHR);
            //             console.dir(textStatus);
            //             console.dir(errorThrown);
            //             $(".message-to p:not(.send-message-to)").remove();
            //             $(".input-container #new-message-input").val("");
            //
            //             $(".messages.block.write-message").hide();
            //             $(".right-container.container .emptyUsernames").show();
            //
            //             $('.messages-container').show();
            //             $('.message-to.input-container').hide();
            //             $('.write-message-user').hide();
            //
            //             $('#cancelSendMessage').hide();
            //             $('.write-message .spinner').hide(0);
            //             noty({text: 'Error sending message!', timeout: 2000, type: 'error'});
            //             connectionOn = false;
            //         }).always(function () {
            //             $('html').removeClass('wait-mouse-cursor');
            //             $('#sendMessage').show();
            //         });
            //     } else {
            //         $('#sendMessage').fadeIn(0);
            //         $('.write-message .spinner').fadeOut(0);
            //         noty({
            //             text: 'You must select an recipient', timeout: 3000
            //         });
            //     }
            //     $('textarea#message-input').val('');
            // } else {
            //     var count = $('textarea#comment').val().length;
            //     $('#sendMessage').fadeIn(0);
            //     $('.write-message .spinner').fadeOut(0);
            //     noty({
            //         text: 'You cannot write more than 800 characters in a message, you have used ' + count + ' characters',
            //         timeout: 5000
            //     });
        }
    }

//     function sendMessageToNewConversation(msg, val, file){
//         if(checkStringLength(msg)){
//             $('#new-chatters').fadeOut(0);
//             $('.write-message .spinner').fadeIn(0);
//             if(msg !== ''){
//                 if(file !== undefined){
//                     msg += '<br/>' + file + ORGFILENAMEID + originalFileName;
//                 }
//                 if(val.indexOf(',') === -1)
//                 {
//                     var id = val;
//                     if(id !== undefined){
//                         var name = $("img[original-id='" + val +"']").attr('original-title');;
//
//                         var iUserIsInMenu = $('.menu-box-tab #'+ id +'').length;
//
//                         if(iUserIsInMenu !== 0){
//
//                             cancelNewMessage();
//                             pickChatterWithAddedCallback(id, function(bData){
//                                 if(bData == false){
//                                     socket.emit('Unarchive opposite conversation', id, function(data){
//                                         if(data){
//                                             clickedID = id;
//                                             $noty.close();
//                                             noty({text: 'Conversation is unarchived',timeout: 2000, type: 'success'});
//                                             cancelNewMessage();
//
//                                         } else {
//                                             noty({text: 'Something went wrong, try again',timeout: 4000, type: 'error'});
//
//                                         }
//
//                                         $('#new-chatters').fadeIn(0);
//                                         $('.write-message .spinner').fadeOut(0);
//
//                                     });
//                                 }
//                             });
//                             sendAndWrite();
//
//
//
//                         } else {
//                             var info = {
//                                 "msgToID" : id,
//                                 "msgToName" : name,
//                                 "msg" : msg
//                             };
//
//                             socket.emit('message a new user', info, function(data){
//
//                                 if(!data){
//                                     noty({
//                                         text: 'This conversation already exists, but is archived, you want to unarchive it?',
//                                         buttons: [
//                                             {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
//
//                                                 socket.emit('Unarchive conversation', id, function(data){
//                                                     $('#new-chatters').fadeIn(0);
//                                                     $('.write-message .spinner').fadeOut(0);
//
//                                                     if(data){
//                                                         clickedID = id;
//                                                         $noty.close();
//                                                         noty({text: 'Conversation is unarchived',timeout: 2000, type: 'success'});
//                                                         cancelNewMessage();
//                                                     } else {
//                                                         noty({text: 'Something went wrong, try again or refresg browser',timeout: 3000, type: 'error'});
//                                                     }
//
//                                                 });
//                                             }
//                                             },
//                                             {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
//                                                 $noty.close();
//                                             }
//                                             }
//                                         ]
//                                     });
//                                 } else {
//                                     $('#new-message-input').val('');
//                                     $('#message-input').val('');
//                                     cancelNewMessage();
//                                     pickChatter(id);
//                                 }
//
//                                 $('#new-chatters').fadeIn(0);
//                                 $('.write-message .spinner').fadeOut(0);
//                             });
//                         }
//                     } else {
//                         noty({
//                             text: 'No user with that this name ' + val, timeout: 4000
//                         });
//                     }
//
//                 } else {
//                     //to check if conversation id exists
//                     val += "," + sMyID;
//                     var inputArray = val.split(',');
//                     inputArray = $.map(inputArray, $.trim);
//
//                     if(!checkIfArrayIsUnique(inputArray)){
//
//                         noty({
//                             text: 'Get rid of duplicates names',timeout: 4000
//                         });
//
//                         return;
//
//                     }
//
//                     if(checkIdsExist(inputArray, sMyID)){
//                         var chattersArray = [];
//                         var aConversationID = [];
//
//                         var aname = '';
//                         var aid = '';
//
//                         for (var i = 0; i < inputArray.length; i++) {
//                             aid = inputArray[i];
//                             if(aid === sMyID){
//                                 aname = myName;
//                             } else {
//                                 aname = $("img[original-id='" + aid +"']").attr('original-title');
//                             }
//
//                             aConversationID.push(aid);
//                             if(aid !== sMyID){
//                                 chattersArray.push({id: aid, name: aname});
//                             }
//
//                         }
//
//                         chattersArray.push({msg: msg});
//
//                         aConversationID = aConversationID.sort();
//                         for (var l = 0; l < aConversationID.length; l++) {
//                             aConversationID[l] = "_" + aConversationID[l];
//                         }
//
//                         var sConversationID = aConversationID.join("");
//
//                         var testing = createID(inputArray, myName, sMyID);
//
//
//
//                         var iConversationIdlength = $('.menu-box-tab #'+ sConversationID +'').length;
//
//                         if(iConversationIdlength === 0){
//
//                             socket.emit('message a new user', chattersArray, function(bValid){
//                                 if(bValid == "socket undefined"){
//                                     noty({text: 'Something went wrong, try again or refreshing the browser', timeout: 3000, type: 'error'});
//                                 } else {
//
//                                     if(typeof bValid === "object"){
//
//                                         if(bValid.exists === true){
//                                             noty({
//                                                 text: 'This conversation already exists, but is archived, you want to unarchive it?',
//                                                 buttons: [
//                                                     {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
//
//                                                         socket.emit('Unarchive conversation', {chattersArray: chattersArray, id: sConversationID}, function(data){
//
//                                                             clickedID = sConversationID;
//
//                                                             if(data){
//                                                                 $noty.close();
//                                                                 noty({text: 'Conversation is unarchived', timeout: 2000, type: 'success'});
//                                                                 cancelNewMessage();
//
//                                                             } else {
//                                                                 noty({text: 'Something went wrong, try again', timeout: 4000, type: 'error'});
//                                                             }
//
//                                                             $('#new-chatters').fadeIn(0);
//                                                             $('.write-message .spinner').fadeOut(0);
//                                                         });
//                                                     }},
//                                                     {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
//                                                         $noty.close();
//                                                     }
//                                                     }
//                                                 ]
//                                             });
//                                         } else {
//                                             $("textarea#new-message-input").val(''); //clear messageBox
//                                             $("input.name.text-input").val('');
//                                             cancelNewMessage();
//
//                                             setTimeout( function(){
//                                                 pickChatter(sConversationID);
//                                             }, 500);
//                                         }
//
//                                     } else if (typeof bValid === "boolean"){
//                                         if(bValid){
//                                             $("textarea#new-message-input").val(''); //clear messageBox
//                                             $("input.name.text-input").val('');
//                                             cancelNewMessage();
//
//                                             setTimeout( function(){
//                                                 pickChatter(sConversationID);
//                                             }, 500);
//                                         }
//                                     } else {
// //                                    Error log
//                                         console.error("Error");
//                                     }
//
//                                 }
//
//                             });
//
//                         } else {
//
//                             $('#new-chatters').fadeIn(0);
//                             $('.write-message .spinner').fadeOut(0);
//                             cancelNewMessage();
//
//                             noty({
//                                 text: 'Conversation with these people already exists',
//                                 timeout: 5000
//                             });
//
//                             $('#new-chatters').fadeIn(0);
//                             $('.write-message .spinner').fadeOut(0);
//                             pickChatter(sConversationID);
//
//                         }
//                     }
//                 }
//             } else {
//                 noty({
//                     text: 'Input can not be left blank',
//                     timeout: 5000,
//                     type: 'error'
//                 });
//
//                 $('#new-chatters').fadeIn(0);
//                 $('.write-message .spinner').fadeOut(0);
//                 $("textarea#new-message-input.text-input").focus();
//             }
//         } else {
//
//             var count = $("textarea#new-message-input").val().length;
//             noty({
//                 text: 'You cannot write more than 800 characters in a message, you have used '+ count +' characters',
//                 timeout: 5000,
//                 type: 'error'
//             });
//
//             $('#new-chatters').fadeIn(0);
//             $('.write-message .spinner').fadeOut(0);
//         }
//
//         $('#new-chatters').fadeIn(0);
//         $('.write-message .spinner').fadeOut(0);
//     }
//
//     function showFileName() {
//         "use strict";
//
//         $('#add-file-overlay .text-input').val($(this).val());
//         originalFileName = $(this).val();
//     }
//
//     function archiveConversation(userID, convID){
//         var archiveObj = {userid: userID, convid: convID};
//
//         socket.emit('archive conversation', archiveObj, function(data){
//
//             if(data){
//                 $("#messageBox").empty();
//                 $(".menu-box-tab #" + convID +"").parent().parent().remove();
//                 $(".menu-box-tab #" + convID +"").parent().parent().remove();
//                 var newConversation = $(".menu-box-menu li:nth-child(2) div div").attr("id");
//                 var optionTwoNewConversation = $(".menu-box-menu li:last-child div div").attr("id");
//
//                 if(newConversation !== undefined){
//                     pickChatter(newConversation);
//                 } else if(optionTwoNewConversation !== undefined){
//                     pickChatter(optionTwoNewConversation);
//                     pickChatter(optionTwoNewConversation);
//                 } else {
//                     clickedID = "";
//                 }
//                 noty({text: 'Conversation is unarchived',timeout: 3000, type: 'success'});
//             } else {
//                 noty({
//                     text: 'Something went wrong try again or refresh the browser', timeout: 3000
//                 });
//             }
//
//         });
//
//         setTimeout(checkIfUsernamesBoxIsEmpty, 500);
//
//     }
//
//     /******* END FUNCTIONS *********************************************************************/
//
});
//
// function addActiveLink() {
//     "use strict";
//     $('.main-messages-link').addClass('activelink');
// }
// function initUploadFile(event) {
//     "use strict";
//
//     $('#add-file-overlay input[type=file]').click();
// }
//
// function initUploadOverlay() {
//     "use strict";
//     var docHeight = $(document).height();
//     $("#overlay").height(docHeight).fadeIn();
//     $("#add-file-overlay").fadeIn();
// }
// function hideUploadOverlay() {
//     "use strict";
//     $("#add-file-overlay").fadeOut();
//     $("#overlay").fadeOut();
// }
// function triggerInput() {
//     "use strict";
//     $(this).parent().parent().find('input[type="text"]').focus();
// }
//
// function toggleSelectedUserMessage() {
//     "use strict";
//
//     var varThis = $(this);
//
//     if (varThis.hasClass('search-users-li') === false) {
//         $('.selected-user-message').removeClass('selected-user-message');
//         varThis.addClass('selected-user-message');
//     }
// }
//
function initNewMessage() {
    // return;
    "use strict";

    $(".message-to p:not(.send-message-to)").remove();
    $(".input-container #new-message-input").val("");

    $(".messages.block.write-message").show();
    $(".right-container.container .emptyUsernames").hide();

    $('.messages-container').fadeOut();
    $('.selected-user-message').removeClass('selected-user-message');
    $('.message-to.input-container').fadeIn();
    $('#message-input').attr('id', 'new-message-input');
    $('.write-message-user').fadeIn();

    $('#cancelSendMessage').fadeIn().css("display", "inline-block");

    // $( "#sendMessage" ).attr("id","new-chatters");
}

function initNewChannel() {

    $(".message-to p:not(.send-message-to)").remove();
    $(".input-container #new-message-input").val("");

    $(".messages.block.write-message").show();
    $(".right-container.container .emptyUsernames").hide();

    $('.messaging-form').fadeOut();
    $('.input-container').fadeOut();
    $('.selected-user-message').removeClass('selected-user-message');
    $('#topics li.active').removeClass('.active');
    $('.new-channel-form').fadeIn();
}


function cancelNewMessage() {
    "use strict";
    $('.menu-box-messages .menu-box-menu li:eq(1)').addClass('selected-user-message');
    $('.messages-container').fadeIn();
    $('.write-message-user').fadeOut();
    $('#new-message-input').attr('id', 'message-input');
    $('.message-to.input-container').fadeOut();
    $('#cancelSendMessage').fadeOut();
    $("#new-chatters").attr("id", "sendMessage");

    setTimeout(checkIfUsernamesBoxIsEmpty, 500);
}

function checkIfUsernamesBoxIsEmpty() {
    if ($(".menu-box-menu li:last-child").attr("class") === "search-users-li") {
        hideWriteBox();
    }
}

function hideWriteBox() {
    $(".messages.block.write-message").hide();
    $(".emptyUsernames").show();
}

// function unique(arr) {
//     "use strict";
//     var o={},
//         r=[],
//         n = arr.length,
//         i;
//
//     for( i=0 ; i<n ; ++i ){
//         o[arr[i]] = null;
//     }
//
//     for( i in o ){
//         r.push(i);
//     }
//
//     return r;
//
// }
//
// function checkIfArrayIsUnique(arr) {
//     "use strict";
//     var map = {}, i, size;
//
//     for (i = 0, size = arr.length; i < size; i++){
//         if (map[arr[i]]){
//             return false;
//         }
//
//         map[arr[i]] = true;
//     }
//
//     return true;
// }
//
// function checkNamesExist(array, myName){
//     "use strict";
//
//     for (var i = 0; i < array.length; i++) {
//
//         if(array[i] !== myName){
//             var nameExists = $("img[original-title='" + array[i] +"']").length;
//             if(nameExists === 0){
//                 noty({
//                     text: array[i] + 'is not a user',
//                     timeout: 5000,
//                     type: 'error'
//                 });
//
//                 return false;
//
//             }
//         }
//     }
//
//     return true;
// }
//
// function checkIdsExist(array, myName){
//     "use strict";
//
//     for (var i = 0; i < array.length; i++) {
//
//         if(array[i] !== myName){
//             var nameExists = $("img[original-id='" + array[i] +"']").length;
//             if(nameExists === 0){
//                 noty({
//                     text: array[i] + 'is not a user',
//                     timeout: 5000,
//                     type: 'error'
//                 });
//
//                 return false;
//
//             }
//         }
//     }
//
//     return true;
// }

function checkStringLength(string) {
    "use strict";
    var allowedLength = 800;
    if (typeof string === "string") {
        if (string.length < allowedLength && string.length > 0) {
            return true;
        } else {
            return false;
        }
    } else {
        console.error("CheckStringLength was not undefined or string");
        return false;
    }
}

// function hasWhiteSpace(s) {
//     "use strict";
//     return s.indexOf(' ') >= 0;
// }
//
// function removeLastComma(str){
//     "use strict";
//     str = str.replace(/,\s*$/, "");
//     return str;
// }
//
// //SAVE THE FILE SO IT IS READY TO BE ADDED WHEN A NEW POST IS CREATED
// function saveUploadOverlay() {
//     "use strict";
//
//     var attachedText = $('#add-file-overlay .text-input').val();
//     $(".attach-icon-span").css({'color': '#FFF'}).attr("original-title", attachedText + " attached");
//     $("#add-file-overlay").fadeOut();
//     $("#overlay").fadeOut();
// }
//
//
// //CANCEL THE UPLOAD FILE OVERLAY
// function cancelUploadOverlay() {
//     "use strict";
//
//     $('#add-file-overlay input[type=file]').val("");
//     $("#add-file-overlay").fadeOut(function () {
//         $(".attach-icon-span").css({'color': '#9099b7'}).attr("original-title", "Attach a file");
//         $('#add-file-overlay .text-input').val("");
//     });
//     $("#overlay").fadeOut();
// }


function replaceURLWithHTMLLinks(text) {
    "use strict";
    var exp = /(\b(https?|http|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    var orgfilename = '';
    var lastLink = '';
    // This is for prettifying amazon appended filename and making them more readable
    if (text.indexOf(ORGFILENAMEID) >= 0) {
        orgfilename = text.slice(text.indexOf(ORGFILENAMEID), text.length);
        if (orgfilename.indexOf("fakepath") >= 0) {
            orgfilename = orgfilename.replace(ORGFILENAMEID + "C:\\fakepath\\", "");
        } else {
            orgfilename = orgfilename.replace(ORGFILENAMEID, "");
        }
        text = text.substring(text.indexOf(ORGFILENAMEID), -1);
        text = text.replace(/(^\s+|\s+$)/g, '');

        lastLink = text.slice(text.lastIndexOf("https://"), text.length);
        text = text.substring(text.lastIndexOf("https://"), -1);

    }

    text = text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
    if (orgfilename !== "" || orgfilename !== '') {
        text = text + "<span class='attached-icon-span entypo-attach scnd-font-color'></span><span class='file-link'><a href='" + lastLink + "' target='_blank'>" + orgfilename + "</a></span>";
    }
    return text;

}

function formatLastMessageStr(text) {
    "use strict";
    if (text != undefined) {
        var n = text.lastIndexOf("br");

        if (n !== -1) {
            text = text.slice(n + 3, text.length);
        }
        return text;
    } else {
        return text;
    }

}

function checkIfArchived(array, id) {
    var bIsArchived = false;

    for (var j = 0; j < array.length; j++) {
        if (array[j].id === id && array[j].archive === 1) {
            bIsArchived = true;
        }
    }

    return bIsArchived;
}
//
function checkForArchived(message) {
    if (message.indexOf("<span class='archived'>") === -1 && message.indexOf("<span class='unArchived'>") === -1) {
        return "none";
    } else if (message.indexOf("<span class='archived'>") > -1) {
        return "archived";
    } else if (message.indexOf("<span class='unArchived'>") > -1) {
        return "unarchived";
    }
}
//
// function createID(array, myName, sMyID){
//     var chattersArray = [];
//     var aConversationID = [];
//
//     var aname = '';
//     var aid = '';
//
//     for (var i = 0; i < array.length; i++) {
//         aname = array[i];
//         if(aname === myName){
//             aid = sMyID;
//         } else {
//             aid = $("img[original-title='" + aname +"']").attr('original-id');
//         }
//
//         aConversationID.push(aid);
//         if(aname !== myName){
//             chattersArray.push({id: aid, name: aname});
//         }
//
//     }
//
//     aConversationID = aConversationID.sort();
//     for (var l = 0; l < aConversationID.length; l++) {
//         aConversationID[l] = "_" + aConversationID[l];
//     }
//
//     var sConversationID = aConversationID.join("");
//
//     return sConversationID;
// }

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

// Array.prototype.clean = function(deleteValue) {
//     for (var i = 0; i < this.length; i++) {
//         if (this[i] == deleteValue) {
//             this.splice(i, 1);
//             i--;
//         }
//     }
//     return this;
// };
//
// // Notification and Visability API
//
//
// // Visability
// var hidden, state, visibilityChange;
// if (typeof document.hidden !== "undefined") {
//     hidden = "hidden";
//     visibilityChange = "visibilitychange";
//     state = "visibilityState";
// } else if (typeof document.mozHidden !== "undefined") {
//     hidden = "mozHidden";
//     visibilityChange = "mozvisibilitychange";
//     state = "mozVisibilityState";
// } else if (typeof document.msHidden !== "undefined") {
//     hidden = "msHidden";
//     visibilityChange = "msvisibilitychange";
//     state = "msVisibilityState";
// } else if (typeof document.webkitHidden !== "undefined") {
//     hidden = "webkitHidden";
//     visibilityChange = "webkitvisibilitychange";
//     state = "webkitVisibilityState";
// }
//
// function handleVisibilityChange() {
//     if (document.hidden) {
//         visabilityState = 0;
//     } else  {
//         visabilityState = 1;
//     }
// }
//
// document.addEventListener(visibilityChange, handleVisibilityChange, false);
//
// // Visability END
//
// // Notification
//
// function notification(name, message, image){
//     if(bNotification == "true" || bNotification == true){
//         if (!('Notification' in window)) {
//             noty({
//                 text: 'Notifications do not work on this browser', timeout: 4000
//             });
//         } else {
//
//             title = name + ' sent you a message';
//             options = {
//                 body: message,
//                 tag: 'Message',
//                 icon: image
//             };
//
//             Notification.requestPermission(function() {
//                 var notification = new Notification(title, options);
//                 setTimeout(function(){
//                     notification.close();
//                 }, 3000);
//             });
//
//         }
//     }
// }
//
// function notificationSettings() {
//     var notificationValue = true;
//     if (localStorage.length != 0) {
//         notificationValue = localStorage.getItem('notification');
//     } else {
//         notificationValue = true;
//     }
//
//     return notificationValue;
// }
//
// // Notification END
