/** THIS JS FILE IS BROKEN INTO THE FOLLOWING SECTIONS:
 *  GLOBAL VARIABLES & EVENT LISTENERS
 *  WEBSOCKET FUNCTIONALITY
 *  LOADING FUNCTIONALITY
 *  FILTER FUNCTIONALITY
 *  POSTS & COMMENTS FUNCTIONALITY
 *  RENDERING OF POSTS & COMMENTS
 *  NOTIFICATIONS AND VISIBILITY
 *  GENERAL FUNCTIONALITY
 */


/** -------------------------------------- */
/** GLOBAL VARIABLES & EVENT LISTENERS   */
/** ---------------------------------- */

//CREATED BY JASON SPASOVSKI

//SETTING THE GLOBAL VARIABLES
var originalMessage;
var originalComment;
var originalTag;
var originalFileName;
var pageNumber = 2;
var lastResults = false;
var lastLoadedDate = new Date().toISOString();
var visabilityState = 1;
var bNotification = notificationSettings();

//SETTING THE EVENT LISTENERS
$(document).ready(function () {
    loadAllPosts();
    $(window).scrollTop(0);
    setDatePicker();
    $('#wallpost-input').autosize();
    $('.CommentMessage').autosize();
    $('.attach-icon-span').tipsy({offset: 5});
    $('.tag-icon-span').tipsy({offset: 5});
    $('.editPost span').tipsy({offset: 5});
    $('.deletePostLink span').tipsy({offset: 5});
    $('.bookmark-wallpost span').tipsy({offset: 5});
    $('.deleteComment span').tipsy({offset: 5});
    $('.editComment span').tipsy({offset: 5});
    $('li.hide').click(toggleList);
    $('.wall-posts a.createwallpost.button').click(insertPost);
    $('a.create-comment.comment.button').click(insertComment);
    $('.edit-wallpost span').click(initEditPost);
    $('.wallpost-edit-container a.comment.wallpost-edit-link').click(editPost);
    $('.wallpost-cancel-link').click(initEditPostCancel);
    $('.edit-comment span').click(initCommentEdit);
    $('.comment-edit-container a.comment-edit-link').click(editComment);
    $('.comment-cancel-link').click(initCommentCancel);
    $('.deletePostLink .delete-icon span').click(initDeletePost);
    $('.deleteComment .delete-icon span').click(initDeleteComment);
    $('.input-icon-tag').click(initTagOverlay);
    $('#add-tag-overlay .tag-cancel-button').click(cancelTagOverlay);
    $('#add-tag-overlay .tag-add-button').click(saveTagOverlay);
    $('#add-file-overlay input[type="text"]').click(initUploadFile);
    $('#add-file-overlay input[type=file]').change(showFileName);
    $('.input-icon-attach').click(initUploadOverlay);
    $('#add-file-overlay .tag-add-button').click(saveUploadOverlay);
    $('#add-file-overlay .tag-cancel-button').click(cancelUploadOverlay);
    $('.menu-box-tab span').click(triggerInput);
    $('input[type="button"].filter-input.bookmark-input').click(toggleBookmarkFilter);
    $('.show-comments-header').click(showMoreComments);
    $('.bookmark-wallpost span').click(toggleBookmark);
    $(window).scroll(checkScroll);
    $('#filter-posts-byname').keyup(function () {
        filterPosts('.middle-text-div a', $(this));
    });
    $('#filter-posts-job').keyup(function () {
        filterPosts('.middle-text-div span.job-title', $(this));
    });
    $('#filter-posts-tags').keyup(function () {
        filterPosts('span.wallpost-tags', $(this));
    });
    $('#datepicker-from').keyup(function (e) {
        checkForDeleteDate(e.keyCode, $(this))
    });
    $('#datepicker-to').keyup(function (e) {
        checkForDeleteDate(e.keyCode, $(this))
    });
    addActiveLink();
    $('#add-file-overlay input[type=file]').val("");
    $('#add-file-overlay .text-input').val("");
    $('#add-tag-overlay .text-input').val("");

    /** --------------------------- */
    /** WEBSOCKET FUNCTIONALITY   */
    /** ----------------------- */
    var mozOrChrome = window['MozWebSocket'] ? MozWebSocket : WebSocket;
    var ws = new mozOrChrome('ws://localhost:9000/postssocket');

    ws.onopen = function (message) {
    };
    ws.onmessage = function (e) {
        console.log(e.data);

        renderPosts(JSON.parse(e.data), false);
        // renderPosts(e.data, false);
        // console.dir(e.data);
    };
// var socket =  io.connect('http://55.55.55.55:3000'); // we get from socket.io.js - DEVELOPMENT VERSION
//    var socket =  io.connect('http://dallr.com:3000'); // we get from socket.io.js - PRODUCTION VERSION

//    INTERVAL WHICH WILL CHECK THE DB FOR NEW WALLPOSTS
    setInterval(fetchNewWallposts, 10000);
    function fetchNewWallposts() {
        var tempLastLoadedDate = lastLoadedDate;
        lastLoadedDate = new Date().toISOString();
        ws.send(tempLastLoadedDate);
//        EMIT THE EMAIL SO THAT POSTS MADE BY THIS USER WILL NOT BE FETCHED
//         socket.emit('check posts',{date:tempLastLoadedDate});
    }

//        LISTEN FOR NEW POSTS FROM app.js
//     socket.on('listen for posts', function (posts) {
// //        RENDER JSON RESULTS
//         renderPosts(posts);
//     });

    // socket.on('listen for deletes', function (deletes) {
    //     removeObjectFromDom(deletes)
    // });

});
/** --------------------------- */
/** LOADING FUNCTIONALITY     */
/** ----------------------- */

//DYNAMIC LOADING OF WALLPOSTS AS THE USER SCROLLS DOWN THE PAGE
function checkScroll() {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 160) {
        $(window).unbind('scroll');
//         $.ajax({
//             type: "POST",
//             dataType: "json",
//             url: "/posts/loadScrolledPosts",
//             data: { "data[pageNumber]": pageNumber}
//         }).done(function (result) {
// //          RESULT CONTAINS THE NEW POSTS IN JSON WHICH ARE PASSED TO renderPosts()
//             renderPosts(result, true);
// //          PAGE NUMBER IS INCREASED SO THE NEXT 6 RESULTS WILL BE LOADED NEXT TIME
//             pageNumber++;
// //          IF RESULTS ARE LESS THAN 6 lastResults will be set to true
//             lastResults = result.length < 6;
//         }).fail(function (jqXHR, textStatus, errorThrown) {
//             noty({text: 'Error recieving new posts!', timeout: 2000, type: 'error'});
//         }).always(function () {
//             if(lastResults == false)
// //                ADD THE EVENT LISTENER IF THERE ARE MORE RESULTS
//                 $(window).scroll(checkScroll);
//         });
        console.log("Load scrolled posts AJAX")
    }
}

function loadAllPosts() {
    $('html').addClass('wait-mouse-cursor');
    var addURL = "/loadposts";

    $.ajax({
        type: "POST",
        dataType: "json",
        url: addURL
    }).done(function (result) {
        $(window).unbind('scroll');
        lastResults = true;
        renderPosts(result, true);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        noty({text: 'Error recieving posts!', timeout: 2000, type: 'error'});
    }).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}

// function removeObjectFromDom(deletes) {
//     for (var i = 0; i < deletes.length; i++) {
//         var id = deletes[i].deletedId;
//         if ($('#' + id).length) {
//             $('#' + id).remove();
//         }
//     }
// }

//THIS FUNCTION WILL LOAD ALL WALLPOSTS THAT ARE NOT ALEADY LOADED BY USING THE PAGENUMBER VARIABLE
// function loadAllOtherPosts(target, inputField) {
//     console.log("AJAX LOAD OTHER POSTS")
//
//     $('html').addClass('wait-mouse-cursor');
//     $.ajax({
//         type: "POST",
//         dataType: "json",
//         url: "/posts/loadAllOtherPosts",
//         data: { "data[pageNumber]": pageNumber}
//     }).done(function (result) {
//             $(window).unbind('scroll');
//             lastResults = true;
//             renderPosts(result, true);
// //          THIS FUNCTION IS CALLED BY FILTERDATES OR JUST NORMAL FILTERS
// //          CHECK WHICH FUNCTION IS CALLED AND CREATE A CALLBACK TO THE APPROPRIATE FUNCTION
//         if(typeof inputField ==  "object"){
//             filterPosts(target,inputField);
//             $(inputField).keyup(function(){
//                 filterPosts(target, inputField);
//             });
//         }else if(inputField == "bookmark"){
//             toggleBookmarkFilter();
//         }else{
//             filterPostsByDate(target,inputField);
//         }
//
//     }).fail(function (jqXHR, textStatus, errorThrown) {
//         noty({text: 'Error recieving new posts!', timeout: 2000, type: 'error'});
//     }).always(function(){
//         $('html').removeClass('wait-mouse-cursor');
//     });
// }
//FUNCTION IS TRIGGERED WHEN USER DELETES FROM OR TO DATE FILTER
function checkForDeleteDate(eCode, thisObject) {
//    CHECK IF THE KEY PRESSED IS BACKSPACE OR DELETE
    if (eCode == 8 || eCode == 46) {
        thisObject.val("");
        thisObject.next().val("");
        thisObject.datepicker("hide").blur();
        if (thisObject.attr("id") == "datepicker-to") {
            var originalFromVal = $('#datepicker-from').val();
            $('#datepicker-from').datepicker("option", "maxDate", 0).val(originalFromVal);
        } else {
            var originalToVal = $('#datepicker-to').val();
            $('#datepicker-to').datepicker("option", "minDate", -800).val(originalToVal);
        }
//        SHOW ALL POSTS THAT WERE HIDDEN BY THE DELETED DATE
        filterPostsByDate($('#datepicker-from-secs').val(), $('#datepicker-to-secs').val())
    }
}

/** --------------------------- */
/** FILTER FUNCTIONALITY      */
/** ----------------------- */

//THIS FUNCTION WORKS WITH ALL TEXT FILTERS THAT ARE NOT DATES
function filterPosts(target, inputField) {
    var query = inputField.val();
    var addThisHiddenClass = inputField.attr("id") + "-hidden";
//    CHECK IF THE ALL RESULTS ARE LOADED
    if (lastResults == false) {
        $(inputField).unbind('keyup');
        loadAllOtherPosts(target, inputField);
    } else {
//        HIDE ALL POSTS THAT DO NOT MATCH THE QUERY
        $(target).not(':contains("' + query + '")').each(function () {
            $(this).closest('.wallposts-container').addClass(addThisHiddenClass);
        });
//        SHOW ALL POSTS THAT DO NOT MATCH THE QUERY
        $(target + ':contains("' + query + '")').each(function () {
            $('#noresults').fadeOut(0);
            $(this).closest('.wallposts-container').removeClass(addThisHiddenClass);
        });
//        IF NO RESULTS ARE FOUND THEN SHOW THE "NO RESULTS" DIV
        if ($('.wallposts-container').not(".hideByDate").not(".filter-posts-job-hidden").not(".filter-posts-byname-hidden").not(".filter-posts-tags-hidden").length == 0)
            $('#noresults').fadeIn(0);

    }
}

//THIS FUNCTION WORKS WITH TO AND FROM DATE FILTERS
function filterPostsByDate(fromDate, toDate) {
//    CHECK IF THE ALL RESULTS ARE LOADED
    if (lastResults == false) {
        loadAllOtherPosts(fromDate, toDate);
    } else {
//        SWITCH CASE DEPENDING ON ONE OR TWO DATES ARE GIVEN
        switch (true) {
            case fromDate != "" && toDate != "":
//              EACH CASE WILL FIRST HIDE ALL POSTS THAT DO NOT MATCH THE DAY OF DATE SELECTED (86400000 MILLISECONDS IN A DAY)
//              EACH CASE WILL THEN SHOW ALL POSTS THAT MATCH THE DAY OF DATE SELECTED (86400000 MILLISECONDS IN A DAY)
                $('.hiddenLastModified').filter(function () {
                    return (parseInt($(this).val() / 86400000) < fromDate / 86400000 || parseInt($(this).val() / 86400000) > toDate / 86400000)
                }).each(function () {
                    $(this).closest('.wallposts-container').addClass('hideByDate');
                });
                $('.hiddenLastModified').filter(function () {
                    return (parseInt($(this).val() / 86400000) >= fromDate / 86400000 && parseInt($(this).val() / 86400000) <= toDate / 86400000)
                }).each(function () {
                    $(this).closest('.wallposts-container').removeClass('hideByDate');
                });
                break;
            case fromDate != "":
                $('.hiddenLastModified').filter(function () {
                    return (parseInt($(this).val() / 86400000) < fromDate / 86400000)
                }).each(function () {
                    $(this).closest('.wallposts-container').addClass('hideByDate');
                });
                $('.hiddenLastModified').filter(function () {
                    return (parseInt($(this).val() / 86400000) >= fromDate / 86400000)
                }).each(function () {
                    $(this).closest('.wallposts-container').removeClass('hideByDate');
                });
                break;
            case toDate != "":
                $('.hiddenLastModified').filter(function () {
                    return (parseInt($(this).val() / 86400000) > toDate / 86400000)
                }).each(function () {
                    $(this).closest('.wallposts-container').addClass('hideByDate');
                });
                $('.hiddenLastModified').filter(function () {
                    return (parseInt($(this).val() / 86400000) <= toDate / 86400000)
                }).each(function () {
                    $(this).closest('.wallposts-container').removeClass('hideByDate');
                });
                break;
            default :
                $('.wallposts-container').removeClass('hideByDate');
        }

//        IF NO RESULTS ARE FOUND WE WILL SHOW THE "NO RESULTS DIV"
        if ($('.wallposts-container').not(".hideByDate").not(".filter-posts-job-hidden").not(".filter-posts-byname-hidden").not(".filter-posts-tags-hidden").length == 0)
            $('#noresults').fadeIn(0);
        else
            $('#noresults').fadeOut(0);
    }
}

//FUNCTION TO RESTRICT OTHER DATEPICKERS DEPENDING ON DATE SELECTED
function setDatePicker() {
//    IF DATEPICKERFROM IS SELECTED IT WILL RESTRICT DATEPICKERTO MIN DATE
    $('#datepicker-from').datepicker({
        dateFormat: 'yy-mm-dd', changeMonth: true, minDate: -800, maxDate: "0", onSelect: function (arg) {
            var orgDate = $('#datepicker-to').val();
            var selectedDate = new Date(arg);
            var endDate = new Date(selectedDate.getTime());

            $("#datepicker-to").datepicker("option", "minDate", endDate);
            $("#datepicker-to").val(orgDate);

            var formattedDate = new Date(arg);
            $(this).val("From: " + formattedDate.toDateString());
            $("#datepicker-from-secs").val(formattedDate.getTime());
//      FILTER THE DOM BY THE DATES SELECTED
            filterPostsByDate(formattedDate.getTime(), $("#datepicker-to-secs").val());
        }
    });
//    IF DATEPICKERTO IS SELECTED IT WILL RESTRICT DATEPICKERFROM MAX DATE
    $('#datepicker-to').datepicker({
        dateFormat: 'yy-mm-dd', changeMonth: true, minDate: -800, maxDate: "0", onSelect: function (arg) {
            $(this).val("To: " + arg);
            var orgDate = $('#datepicker-from').val();
            var selectedDate = new Date(arg);
            var startDate = new Date(selectedDate.getTime());

            $("#datepicker-from").datepicker("option", "maxDate", startDate);
            $("#datepicker-from").val(orgDate);

            var formattedDate = new Date(arg);
            $(this).val("To: " + formattedDate.toDateString());
//      FILTER THE DOM BY THE DATES SELECTED
            filterPostsByDate($("#datepicker-from-secs").val(), formattedDate.getTime());
            $("#datepicker-to-secs").val(formattedDate.getTime());
        }
    });
}

//TOGGLE THE FILTERS BOX
function toggleList() {
    if ($('.hide-filters span').hasClass('fontawesome-arrow-down')) {
        $('.menu-box').animate({height: 450});
        $('.hidable-filters').fadeIn(300);
        $('.hide-filters span').removeClass('fontawesome-arrow-down').addClass('fontawesome-arrow-up');
    } else {
        $('.menu-box').animate({height: 90});
        $('.hidable-filters').fadeOut(300);
        $('.hide-filters span').removeClass('fontawesome-arrow-up').addClass('fontawesome-arrow-down');
    }
}

/** ------------------------------------ */
/** POSTS & COMMENTS FUNCTIONALITY    */
/** ------------------------------- */

function insertPost() {
//    UNBIND EVENT LISTENER SO ONLY ONE POST CAN BE SENT AT A TIME
    $(".wall-posts a.createwallpost.button").unbind("click");
    var postContent = $('#wallpost-input').val();
    var tag = $('#add-tag-overlay .text-input').val();
    var validated = validate(postContent);
    // var file = $('#add-file-overlay #my_file')[0].files[0];

//    BECAUSE A FILE IS ADDED IT WORKED BEST WITH SENT AS FORMDATA
//     var data = new FormData();
//     data.append("data[Post][content]", postContent);
//     data.append("data[Post][tag]", tag);
//     if (file)
//         data.append("data[Post][file]", file);
    if (validated == true) {
        $('.wall-posts a.createwallpost.button').fadeOut(0);
        $('.write-post .spinner').fadeIn(0);
        var addURL = "/insertpost?content=" + postContent + "&tag=" + tag;
        $.ajax({
            type: "POST",
            url: addURL,
            cache: false,
            dataType: "json"
        }).done(function (result) {
            if (result != false) {
                var id = "IDGOESHERE";
//              RENDER THE INDIVIDUAL WALLPOST WITH THE APPROPRIATE RESULTS
                if (id.length) {
                    renderPosts(result, false);
                    noty({text: 'Wallpost sucessfully created!', timeout: 2000, type: 'success'});
                    cancelTagOverlay();
                    cancelUploadOverlay();
                    $('#' + id + ' .CommentMessage').autosize();
                    $('#wallpost-input').val("").height(72);
                } else {
                    noty({text: 'Error creating wallpost!', timeout: 2000, type: 'error'});
                }
            } else {
                noty({text: 'Error creating wallpost!', timeout: 2000, type: 'error'});
            }
        })
            .fail(function (jqXHR, textStatus, errorThrown) {
                noty({text: 'Error creating wallpost!', timeout: 2000, type: 'error'});
            })
            .always(function () {
//                HIDE THE THROBBER & ADD EVENT LISTENER AFTER COMPLETION
                $('.write-post .spinner').fadeOut(0);
                $('.wall-posts a.createwallpost.button').fadeIn(0).bind('click', insertPost);
            });
    } else {
        noty({text: validated, timeout: 2000, type: 'error'});
        $('.wall-posts a.createwallpost.button').bind('click', insertPost);
    }
}

//SHOW THE EDIITNG FIELD FOR WALLPOSTS
function initEditPost() {
    $(this).parent().parent().parent().parent().find('span.attached-file-link').fadeOut(0);
    $(this).parent().parent().parent().parent().find('.input-icon-attached').fadeOut(0);
    $(this).parent().parent().parent().parent().find('.wallpost-edit-container').fadeIn(0);
    var currentMessage = $(this).parent().parent().parent().parent().find('p:first');
    var currentTag = $(this).parent().parent().parent().parent().find('.wallpost-tags');
    originalMessage = currentMessage.html();
    originalTag = currentTag.text();
    var inputBox = $("<textarea/>").val(currentMessage.text()).addClass("wallpostMessage").addClass("form-control").autosize();
    var inputBoxTag = $("<input class='name text-input' type='text' placeholder='project tag' maxlength='26'>").val(currentTag.text()).addClass("wallpostTag").width(currentTag.width() <= 135 ? currentTag.width() + 10 : 145);
    currentMessage.replaceWith(inputBox);
    currentTag.replaceWith(inputBoxTag);
}

//CANCEL THE EDIITNG FIELD FOR WALLPOSTS
function initEditPostCancel() {
    $('.input-icon-attached').fadeIn(0);
    $('span.attached-file-link').fadeIn(0);
    $('.wallpost-edit-container').fadeOut(0);
    var currentMessage = $(this).parent().parent().parent().find('textarea.wallpostMessage');
    var pWallpostTag = $("<p></p>").html(originalMessage).removeClass("form-control");
    var currentTag = $(this).parent().parent().parent().parent().find('.wallpostTag');
    var pTag = $("<span></span>").html(originalTag).removeClass("wallpostTag").addClass("wallpost-tags");
    currentMessage.replaceWith(pWallpostTag);
    currentTag.replaceWith(pTag);
    $('.edit-icon span').fadeIn(0);
    $('.delete-icon span').fadeIn(0);
}

//FUNCTION TO EDIT THE WALLPOSTS
function editPost() {
//  SAVING $THIS INTO A VARIABLE BECAUSE INSIDE OF THE AJAX CALL $THIS IS NO LONGER THIS OBJECT
    var thisObject = $(this);
    var postId = $(this).parent().parent().parent().parent().parent().parent().attr("id");
    var postContent = $(this).parent().parent().parent().find('.wallpostMessage').val();
    var postTag = $(this).parent().parent().parent().parent().find('.wallpost-bottom-container .wallpostTag').val();
    var validated = validate(postContent);
    if (validated == true) {
        $('html').addClass('wait-mouse-cursor');
        $.ajax({
            type: "POST",
            url: "/editpost?postId=" + postId + "&content=" + postContent + "&tag=" + postTag
        }).done(function (data) {
            if (data == 'success') {
//                  HIDE THE EDIITNG FIELDS AFTER POST IS UPDATED
                noty({text: 'Post sucessfully updated!', timeout: 2000, type: 'success'});
                thisObject.parent().parent().parent().parent().find('span.attached-file-link').fadeIn(0);
                thisObject.parent().parent().parent().parent().find('.input-icon-attached').fadeIn(0);
                thisObject.parent().parent().fadeOut(0);
                var currentMessage = thisObject.parent().parent().parent().find('textarea.wallpostMessage');
                postContent = postContent.replace(/\r\n/g, "\r\n<br />").replace(/\n/g, "\n<br />");
                var pWallpostTag = $("<p></p>").html(postContent).removeClass("form-control");
                var currentTag = thisObject.parent().parent().parent().parent().find('.wallpostTag');
                var pTag = $("<span></span>").html(postTag).removeClass("wallpostTag").addClass("wallpost-tags");
                currentMessage.replaceWith(pWallpostTag);
                currentTag.replaceWith(pTag);
                thisObject.parent().parent().parent().parent().find('span.lastModified').text(" modified post 1 second ago.");
            } else {
                noty({text: 'Error updating post!', timeout: 2000, type: 'error'});
            }
        })
            .fail(function () {
                noty({text: 'Error updating post!', timeout: 2000, type: 'error'});
            }).always(function () {
            $('html').removeClass('wait-mouse-cursor');
        })
    } else {
        noty({text: validated, timeout: 2000, type: 'error'});
    }

}

//INITIALIZING THE DELETION OF WALLPOST BY USING NOTY
function initDeletePost() {
    var thisElement = $(this);
    noty({
        text: 'Do you wish to delete this wallpost?',
        buttons: [{
            addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {
                $noty.close();
                deletePost(thisElement);
            }
        }, {
            addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                $noty.close();
            }
        }]
    });
}

//FUNCTION TO DELETE WALLPOSTS
function deletePost(thisElement) {
    $('html').addClass('wait-mouse-cursor');
    var postId = thisElement.parent().parent().parent().parent().parent().parent().attr("id");
    console.log("AJAX DELETE POST HERE")
//
    $.post("deletepost?postId=" + postId,
        function (data) {
            if (data == 'success') {
//              IF POST IS DELETED FROM DATABASE REMOVE IT FROM THE DOM
                noty({text: 'Wallpost sucessfully deleted!', timeout: 2000, type: 'success'});
                thisElement.parent().parent().parent().parent().parent().parent().remove();
            } else {
                noty({text: 'Error deleting wallpost!', timeout: 2000, type: 'error'});
            }
        }
    ).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}

//FUNCTION TO CREATE NEW COMMENTS
function insertComment() {
//  UNBIND THE EVENT LISTENER SO ONLY ONE COMMENT CAN BE CREATED AT A TIME
    $('a.create-comment.comment.button').unbind('click');
    var postId = $(this).parent().parent().parent().attr("id");
    var textArea = $(this).parent().find('.CommentMessage');
    var postContent = textArea.val();
//    postContent = postContent.replace(/\r\n/g, "<br />").replace(/\n/g, "<br />");
    var validated = validate(postContent);

    if (validated == true) {
        $('html').addClass('wait-mouse-cursor');
        var addURL = "/insertcomment?postId=" + postId + "&content=" + postContent;
        $.ajax({
            type: "POST",
            url: addURL,
            dataType: "json",
        }).done(function (data) {
            if (!jQuery.isEmptyObject(data)) {
                noty({text: 'Comment sucessfully created!', timeout: 2000, type: 'success'});
                renderComments(data);
                // renderPosts(data, false);
                textArea.val("").height(48);
            } else {
                noty({text: 'Error creating comment!', timeout: 2000, type: 'error'});
            }
        })
            .fail(function () {
                noty({text: 'Error creating comment!', timeout: 2000, type: 'error'});
            })
            .always(function () {
                $('html').removeClass('wait-mouse-cursor');
//              ADD EVENT LISTENER AFTER COMPLETION
                $('a.create-comment.comment.button').bind('click', insertComment);
            });
    } else {
        noty({text: validated, timeout: 2000, type: 'error'});
        $('a.create-comment.comment.button').bind('click', insertComment);
    }
}

//SHOW THE EDIITNG FIELD FOR COMMENTS
function initCommentEdit() {
    $(this).parent().parent().parent().parent().parent().parent().find('.comment-edit-container').fadeIn(0);
    var currentMessage = $(this).parent().parent().parent().parent().parent().parent().find('p');
    originalComment = currentMessage.html();
    $('.comment-editlink').fadeOut(0);
    var inputBox = $("<textarea/>").val(currentMessage.text()).addClass("wallpostMessage").addClass("comment-form-control").autosize();
    currentMessage.replaceWith(inputBox);
}

//CANCEL THE EDITING FIELDS FOR COMMENTS
function initCommentCancel() {
    $('.comment-edit-container').fadeOut(0);
    var currentMessage = $(this).parent().parent().parent().find('textarea');
    var pTag = $("<p></p>").html(originalComment).removeClass("comment-form-control");
    currentMessage.replaceWith(pTag);
    $('.comment-editlink').fadeIn(0);
}

//FUNCTION TO EDIT COMMENTS
function editComment() {
//  SAVING $THIS INTO A VARIABLE BECAUSE INSIDE OF THE AJAX CALL $THIS IS NO LONGER THIS OBJECT
    var thisObject = $(this);
    var postId = $(this).parent().parent().parent().parent().parent().attr("id");
    var commentPostId = $(this).parent().parent().parent().attr("id");
    var commentContent = $(this).parent().parent().parent().find('.wallpostMessage').val();
    var validated = validate(commentContent);
    if (validated == true) {
        $('html').addClass('wait-mouse-cursor');
        $.ajax({
            type: "POST",
            url: "/editcomment?commentId=" + commentPostId + "&content=" + commentContent
        }).done(function (data) {
            if (data == 'success') {
//                  HIDE THE EDIITNG FIELDS AFTER COMMENT IS UPDATED
                noty({text: 'Comment sucessfully updated!', timeout: 2000, type: 'success'});
                $('.comment-edit-container').fadeOut(0);
                var currentMessage = thisObject.parent().parent().parent().find('textarea.wallpostMessage');
                commentContent = commentContent.replace(/\r\n/g, "\r\n<br />").replace(/\n/g, "\n<br />");
                var pWallpostTag = $("<p class='wallpostComment'></p>").html(commentContent).removeClass("comment-form-control");
                currentMessage.replaceWith(pWallpostTag);
                thisObject.parent().parent().parent().find('span.lastPosted').text(" modified comment 1 second ago.");
            } else {
                noty({text: 'Error updating comment!', timeout: 2000, type: 'error'});
            }
        })
            .fail(function () {
                noty({text: 'Error updating comment!', timeout: 2000, type: 'error'});
            }).always(function () {
            $('html').removeClass('wait-mouse-cursor');
        })
    } else {
        noty({text: validated, timeout: 2000, type: 'error'});
    }
}

//INITIALIZING THE DELETION OF COMMENTS BY USING NOTY
function initDeleteComment() {
    var thisElement = $(this);
    noty({
        text: 'Do you wish to delete this comment?',
        buttons: [{
            addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {
                // this = button element
                // $noty = $noty element
                $noty.close();
                deleteComment(thisElement);
            }
        }, {
            addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                $noty.close();
            }
        }]
    });
}

//FUNCTION TO DELETE COMMENTS
function deleteComment(thisElement) {
    $('html').addClass('wait-mouse-cursor');
    var postId = thisElement.parent().parent().parent().parent().parent().parent().parent().parent().attr("id");
    var hiddenPostId = thisElement.parent().parent().parent().parent().parent().parent().parent().parent().parent().attr("id");
    var commentId = thisElement.parent().parent().parent().parent().parent().parent().attr("id");
    var realPostId = postId ? postId : hiddenPostId;

    $.post("deletecomment?postId=" + realPostId + "&commentId=" + commentId,
        function (data) {
            if (data == 'success') {
//              IF COMMENT IS DELETED FROM DATABASE REMOVE IT FROM THE DOM
                noty({text: 'Comment sucessfully deleted!', timeout: 2000, type: 'success'});
                thisElement.parent().parent().parent().parent().parent().parent().remove();
            } else {
                noty({text: 'Comment deleting wallpost!', timeout: 2000, type: 'error'});
            }
        }
    ).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}

//TOGGLE BOOKMARKS
function toggleBookmarkFilter() {
    if (lastResults == false) {
        loadAllOtherPosts(false, 'bookmark');
    } else {
        if ($('input[type="button"].filter-input.bookmark-input').val() == "Bookmarks Disabled") {
            $('.menu-box-tab span.fontawesome-bookmark').addClass("white");
            $('.wallposts-container').addClass('hideByBookmark');
            $('span.selected-bookmark').parent().parent().parent().parent().parent().parent().removeClass('hideByBookmark');
            $('input[type="button"].filter-input.bookmark-input').val("Bookmarks Enabled").css({'color': '#fff'});
        } else {
            $('.menu-box-tab span.fontawesome-bookmark').removeClass("white");
            $('.wallposts-container').removeClass('hideByBookmark');
            $('input[type="button"].filter-input.bookmark-input').val("Bookmarks Disabled").css({'color': '#afb3c2'});
        }
    }
}

//TOGGLE BOOKMARKS THOUGH WALLPOST
function toggleBookmark() {
    var thisObject = $(this);
    $('html').addClass('wait-mouse-cursor');
    thisObject.unbind("click");
    if ($(this).hasClass("selected-bookmark")) {
        var postId = $(this).parent().parent().parent().parent().parent().parent().attr("id");
        $.post("/removebookmark?postId=" + postId,
            function (data) {
                if (data == 'success') {
                    thisObject.removeClass("selected-bookmark");
                } else {
                    noty({text: 'Problem adding your bookmark!', timeout: 2000, type: 'error'});
                }
            }
        ).always(function () {
                $('html').removeClass('wait-mouse-cursor');
                thisObject.bind('click', toggleBookmark);
            }
        );
    } else {
        var postId = $(this).parent().parent().parent().parent().parent().parent().attr("id");
        $.post("/addbookmark?postId=" + postId,
            function (data) {
                if (data == 'success') {
                    thisObject.addClass("selected-bookmark");
                } else {
                    noty({text: 'Problem adding your bookmark!', timeout: 2000, type: 'error'});
                }
            }
        ).always(function () {
                $('html').removeClass('wait-mouse-cursor');
                thisObject.bind('click', toggleBookmark);
            }
        );
    }
}

//HARDCODE THE POSTS PAGE AS THE SELECTED LINK
function addActiveLink() {
    $('.main-posts-link').addClass('activelink');
}

//INITIALIZING THE TAG OVERLAY
function initTagOverlay() {
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
    $("#add-tag-overlay").fadeIn();
}

//CANCLING THE TAG OVERLAY
function cancelTagOverlay() {
    $("#add-tag-overlay").fadeOut();
    $("#overlay").fadeOut(function () {
        $('#add-tag-overlay .text-input').val("");
        $(".tag-icon-span").css({'color': '#9099b7'}).attr("original-title", "Tag a project")
    });
}

//SAVING TAGS SO THEY WILL BE ADDED WHEN A NEW POST IS ADDED
function saveTagOverlay() {
    var tagText = $('#add-tag-overlay .text-input').val();
    $(".tag-icon-span").css({'color': '#fff'}).attr("original-title", "Tag:" + tagText);
    $("#add-tag-overlay").fadeOut();
    $("#overlay").fadeOut();
}

//ADDING TRIGGER TO MAKE CLICKING INPUT BOX TO MAKE IT TRIGGER FILE UPLOADING
function initUploadFile() {
    $('#add-file-overlay input[type=file]').trigger('click');
}

//FUNCTION TO SHOW THE CORRECT FILE NAME IN THE INPUT BOX IN THE UPLOAD FILE OVERLAY
function showFileName() {
    $('#add-file-overlay .text-input').val($(this).val());
    originalFileName = $(this).val();
}

//INITIALIZING THE UPLOAD FILE OVERLAY
function initUploadOverlay() {
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
    $("#add-file-overlay").fadeIn();
}

//CANCEL THE UPLOAD FILE OVERLAY
function cancelUploadOverlay() {
    $('#add-file-overlay input[type=file]').val("");
    $("#add-file-overlay").fadeOut(function () {
        $(".attach-icon-span").css({'color': '#9099b7'}).attr("original-title", "Attach a file");
        $('#add-file-overlay .text-input').val("");
    });
    $("#overlay").fadeOut();
}

//SAVE THE FILE SO IT IS READY TO BE ADDED WHEN A NEW POST IS CREATED
function saveUploadOverlay() {
    var attachedText = $('#add-file-overlay .text-input').val()
    $(".attach-icon-span").css({'color': '#FFF'}).attr("original-title", attachedText + " attached");
    $("#add-file-overlay").fadeOut();
    $("#overlay").fadeOut();
}

//MAKE CLICKING ICONS TRIGGER CLICKING INPUTS
function triggerInput() {
    $(this).parent().parent().find('input[type="text"]').focus();
}

//CLICK TO VIEW MORE COMMENTS
function showMoreComments() {
    $(this).parent().find('.more-comments-1').show("blind", 600);
    $(this).removeClass('show-comments-header').text("Comments:");
}

function differenceBetweenDates(dateTime) {
    var currentTime = new Date();
    var timeDifference = (currentTime.getTime() - dateTime.getTime()) / 60000;
    var returnString;
    if (timeDifference <= 2) {
        returnString = " just now";
    } else if (timeDifference < 60) {
        returnString = Math.floor(timeDifference) + " minutes ago";
    } else if (timeDifference < 1440) {
        if (Math.floor(timeDifference / 60) == 1) returnString = "1 hour ago";
        else returnString = Math.floor(timeDifference / 60) + " hours ago";
    } else {
        returnString = Math.floor(timeDifference / 1440) + " days ago";
    }
    return returnString;
}

/** ------------------------------------ */
/** RENDERING OF POSTS & COMMENTS     */
/** ------------------------------- */

function renderPosts(result, controllerLoaded) {
    controllerLoaded = typeof controllerLoaded !== 'undefined' ? controllerLoaded : false;
    for (var i = 0; i < result.length; i++) {
        var newCommentIds = [];
        var newCommentBool = false;
        var editCommentBool = false;
        var newPosttContent = false;
        var newCommentContent = false;
        var newPostBool = result[i].Post != undefined ? false : true;
        var obj = result[i];
        var objId = obj._id.$oid;
        var postDomObjectExists = $("#" + objId).length ? true : false;
        var multipleCommentsFromController = false;
        // CHECK THE POST CONTAINS COMMENTS
        if (obj["comments"] != undefined) {
//          HIDE ALL OTHER COMMENTS BESIDES THE LAST 3
            var commentHtml = obj.comments.length > 3 ? "<h4 class='show-comments-header'> View " + (obj.comments.length - 3) + " more comments </h4>" : "<h4>Comments:</h4>";
            for (var ii = 0; ii < obj.comments.length; ii++) {
                newCommentBool = false;
//              CREATE THE HTML TO BE ADDED FOR EACH COMMENT
                var commentObj = obj.comments[ii];
                var shouldShowHideComments = (obj.comments.length > 3 && (obj.comments.length - 4) >= ii);
                if (shouldShowHideComments && controllerLoaded) multipleCommentsFromController = true;
                commentHtml += shouldShowHideComments ? "<div class='more-comments-1'>" : "";
                var thisCommentId = commentObj._id.$oid;
                var commentDomObjectExists = $("#" + thisCommentId).length ? true : false;

//                CHECKING THE POST WHICH CONTAINS THE COMMENT IS VISIBLE / THE COMMENT IS NOT ALREADY VISIBLE AND IS NOT LOADED VIA SCROLLING
                if (postDomObjectExists && !commentDomObjectExists && !controllerLoaded) {
                    commentHtml = "";
                    newCommentBool = true;
                }
                //IF THE NEW COMMENT IS IDENTICLE TO THE OLD COMMENT THEN BREAK
                newCommentContent = htmlEntities($("#" + thisCommentId + " .wallpostComment").text().replace(/\s*\n\s*/g, "")) == htmlEntities(commentObj.content.replace(/\s*\n\s*/g, "")) ? false : true;
                if (commentDomObjectExists && !newCommentContent) {
                    continue;
                }
                var isCommentCreator = commentObj.creator.username == $("#userEmail").val();
                if (commentDomObjectExists && newCommentContent) {
                    $("#" + thisCommentId + " .lastPosted").text(" modified comment 1 second ago");
                    $("#" + thisCommentId + " .wallpostComment").html(commentObj.content);
                    editCommentBool = true;
                    continue;
                } else {
                    var timeSinceComment = differenceBetweenDates(new Date(commentObj.modified.$date));
                    newCommentIds.push(thisCommentId);
                    commentHtml += '<div class="commentDiv" id="' + thisCommentId + '">' +
                        '<img class="commentProfileImage" alt="Profile-Image" src="' + commentObj.creator.image + '">' +
                        '<div class="topBarComments">' +
                        '<div class="commentPostedAtBy">' +
                        '<a href="/profiles/' + commentObj.creator.email + '">' + commentObj.creator.firstname + '</a>' +
                        '<span class="lastPosted"> ' + (commentObj.modified.$date > commentObj.created.$date ? ' modified comment ' : ' created comment ') + timeSinceComment + '</span>' +
                        '<div class="commentLinks">' +
                        (isCommentCreator ? '<a class="deleteComment" href="javascript:void(0)">\n<div class="delete-icon">\n<span original-title="Delete" class="fontawesome-remove scnd-font-color"></span>\n</div>\n</a>\n<a class="editComment" href="javascript:void(0)">\n<div class="edit-icon edit-comment">\n<span original-title="Edit" class="fontawesome-edit scnd-font-color"></span>\n</div>\n</a>' : '') +
                        '</div>' +
                        '<span class="comment-job-title">' + commentObj.creator.jobtitle + '</span>' +
                        '</div>' +
                        '</div>' +
                        '<blockquote>' +
                        '<p class="wallpostComment">' + commentObj.content + '</p>' +
                        '</blockquote>' +
                        '<div class="comment-edit-container" style="display: none;">' +
                        '<div class="center-container">' +
                        '<a class="comment button comment-edit-link" href="javascript:void(0)">UPDATE</a>' +
                        '<a class="comment button comment-cancel-link" href="javascript:void(0)">CANCEL</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    if (postDomObjectExists && !commentDomObjectExists && !controllerLoaded) {
                        $("#" + objId + " .addComment").before(commentHtml);
                        commentHtml = "";
                    }
                    commentHtml += (obj.comments.length > 3 && (obj.comments.length - 4) >= ii ) ? "</div>" : "";
                    if (!controllerLoaded) {
                        newCommentBool = true;
                    }
                }

            }
        }

//      //IF THE NEW POST IS IDENTICAL TO THE OLD COMMENT THEN BREAK
        newPosttContent = obj.content == $("#" + objId + " .wallpostMessage").text() ? false : true;
        if (obj.content != undefined) {
            if ($("#" + objId + " .wallpostMessage").length) {
                var newContentStripped = htmlEntities(obj.content.replace(/\//g, "").replace(/\s*\n\s*/g, "").replace(/ /g, ''));

                var currentContentStripped = htmlEntities($("#" + objId + " .wallpostMessage").html().replace(/\//g, "").replace(/\s*\n\s*/g, "").replace(/ /g, ''))
                newPosttContent = newContentStripped == currentContentStripped ? false : true;
            }
        }

        if (postDomObjectExists && !newCommentContent && newPosttContent) {
            $("#" + objId + " .lastModified").html(" modified post 1 second ago");
            $("#" + objId + " .wallpostMessage").html(obj.content);
            $("#" + objId + " .wallpost-tags").text(obj.tag);
        }

        var modifiedDate = new Date(obj.modified.$date);
        var isPostCreator = $('#userEmail').val() == obj.creator.username;
        if (!newCommentBool && !postDomObjectExists) {

            var bookmarkBool = false;
        for (var j = 0; j < obj.bookmarks.length; j++)
            if (obj.bookmarks[j].username == $('#userEmail').val())
                bookmarkBool = true;
        var timeSincePost = differenceBetweenDates(new Date(obj.modified.$date));
        var htmlToRender = '' +
            '<div class="wallposts-container" id="' + objId + '">' +
            '<div class="wall-posts block individual-wallpost">' +
            '<div class="input-container">' +
            '<div class="profileTopBarLeft">' +
            '<img class="profileImage" width="60" height="60" alt="Profile-Image" src="' + obj.creator.image + '">' +
            '<div class="middle-text-div">' +
            '<a href="/profiles/' + obj.creator.username + '">' + obj.creator.firstname + '</a>' +
            '<span class="lastModified">' + (obj.modified.$date > obj.created.$date ? ' modified post ' : ' created post ') + timeSincePost + '</span>' +
            // '<span class="lastModified">' + (newPostBool ? " created post 1 second ago" : " " + obj.created) + '</span>' +
            '<span class="job-title">' + obj.creator.jobtitle + '</span>' +
            '<input class="hiddenLastModified" type="hidden" value="' + modifiedDate.getTime() + '">' +
            // '<input class="hiddenLastModified" type="hidden" value="' + (controllerLoaded ? obj.modified.sec * 1000 : modifiedDate.getTime()) + '">' +
            '</div>' +
            '</div>' +
            '<div class="profileTopBarRight wallPostLinks9" style="display: block;">' +
            (isPostCreator ? '<input class="deletePostId" type="hidden" value="86">\n<a class="deletePostLink" href="javascript:void(0)">\n<div class="delete-icon">\n<span original-title="Delete" class="fontawesome-remove scnd-font-color"></span>\n</div>\n</a><a class="editPost" href="javascript:void(0)">\n<div class="edit-icon edit-wallpost">\n<span original-title="Edit" class="fontawesome-edit scnd-font-color"></span>\n</div>\n</a>\n' : '') +
            '<a class="bookmarkWallpost" href="javascript:void(0)">' +
            '<div class="edit-icon bookmark-wallpost">' +
            // '<span original-title="Bookmark" class="fontawesome-bookmark scnd-font-color selected-bookmark"></span>' +
            '<span original-title="Bookmark" class="fontawesome-bookmark scnd-font-color ' + (bookmarkBool ? "selected-bookmark" : "") + '"></span>' +
            '</div>' +
            '</a>' +
            '</div>' +
            '<div class="profileMiddleBar">' +
            '<blockquote>' +
            '<p class="wallpostMessage">' + obj.content + '</p>' +
            '</blockquote>' +
            '<div class="wallpost-edit-container" style="display: none;">' +
            '<div class="center-container">' +
            '<a class="comment button wallpost-edit-link" href="javascript:void(0)">UPDATE</a>' +
            '<a class="comment button wallpost-cancel-link" href="javascript:void(0)">CANCEL</a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="wallpost-bottom-container">'
            +
            // + (obj.hasOwnProperty('attached') ? '<div class="input-icon-attached">' : "")
            // + (obj.hasOwnProperty('attached') ? '<span class="attached-icon-span entypo-attach scnd-font-color"></span>' : "")
            // + (obj.hasOwnProperty('attached') ? '</div>' : "")
            // + (obj.hasOwnProperty('attached') ? '<a target="_blank" href="' + obj.attached.link + '">' : "")
            // + (obj.hasOwnProperty('attached') ? '<span class="attached-file-link">' + obj.attached.name + '</span>' : "")
            // + (obj.hasOwnProperty('attached') ? '</a>' : "") +
            '<span class="wallpost-tags">' + obj.tag + '</span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="commentsContainer">' +
            (commentHtml != undefined ? commentHtml : "") +
            '<div class="addComment">' +
            '<div class="comment-icon">' +
            '<span class="fontawesome-comment scnd-font-color"></span>' +
            '</div>' +
            '<textarea class="CommentMessage CommentMessagepost Comment postComment" maxlength="600" placeholder="Write comment here..."></textarea>' +
            '<a class="create-comment comment button" href="javascript:void(0)">POST</a>' +
            '</div>' +
            '</div>' +
            '</div>';
            $('.wall-posts.write-post.dropdown-bar').next().after(htmlToRender);
        // newPostBool && !controllerLoaded || !postDomObjectExists && !controllerLoaded ? $('.wall-posts.write-post.dropdown-bar').next().after(htmlToRender) : $('#add-tag-overlay').before(htmlToRender);

        // ADD EVENT LISTENERS FROM NEWLY APPENDED WALLPOSTS
        $('#' + objId + ' .CommentMessage').autosize();
        $('#' + objId + ' a.create-comment.comment.button').click(insertComment);
        $('#' + objId + ' .edit-wallpost span').click(initEditPost);
        $('#' + objId + ' .edit-wallpost span').tipsy({offset: 5});
        $('#' + objId + ' .wallpost-edit-link').click(editPost);
        $('#' + objId + ' .wallpost-cancel-link').click(initEditPostCancel);
        $('#' + objId + ' .wall-posts .delete-icon span').tipsy({offset: 5});
        $('#' + objId + ' .wall-posts .delete-icon span').click(initDeletePost);
        $('#' + objId + ' .bookmark-wallpost span').click(toggleBookmark);
        $('#' + objId + ' .bookmark-wallpost span').tipsy({offset: 5});

        // }
        // if (newCommentBool && !editCommentBool && newCommentContent || multipleCommentsFromController) {
            for (var iii = 0; iii < newCommentIds.length; iii++) {
                var thisCommentId = newCommentIds[iii];
                $('#' + thisCommentId + ' .comment-edit-container a.comment-edit-link').click(editComment);
                $('#' + thisCommentId + ' .comment-cancel-link').click(initCommentCancel);
                $('#' + thisCommentId + ' .deleteComment .delete-icon span').click(initDeleteComment);
                $('#' + thisCommentId + ' .edit-comment span').click(initCommentEdit);
            }
            $('#' + objId + ' .show-comments-header').click(showMoreComments);

            //ADDING THE LISTENER IF NEW COMMENTS SHOULD BE ADDED
            commentHtml = "";
        // }
        }
    }
//    Notification's
//     if (newCommentBool) {
//         if (visabilityState === 0) {
//             var notificationfHeader = commentObj.creator.firstname + " commented on " + obj.content;
//             notification(notificationfHeader, commentObj.content, obj.creator.img);
//             var snd = new Audio("../notification.mp3");
//             snd.play();
//         }
//     } else if (newPostBool) {
//         if (visabilityState === 0) {
//             notification(obj.creator.firstname + " created a new post", obj.content, obj.creator.img);
//             var snd = new Audio("../notification.mp3");
//             snd.play();
//         }
//     }
}

function renderComments(result) {
    var newCommentIds = [];
    var commentHtml = "";
    for (var i = 0; i < result.length; i++) {
        var commentObj = result[i];
        var objId = commentObj.postId;
        var thisCommentId = commentObj._id.$oid;
        var commentDomObjectExists = $("#" + thisCommentId).length ? true : false;

        //IF THE NEW COMMENT IS IDENTICLE TO THE OLD COMMENT THEN BREAK
        var isCommentCreator = commentObj.creator.username == $("#userEmail").val();
        var timeSincePost = differenceBetweenDates(new Date(commentObj.modified.$date));

        newCommentIds.push(thisCommentId);
        commentHtml += '<div class="commentDiv" id="' + thisCommentId + '">' +
            '<img class="commentProfileImage" alt="Profile-Image" src="' + commentObj.creator.image + '">' +
            '<div class="topBarComments">' +
            '<div class="commentPostedAtBy">' +
            '<a href="/profiles/' + commentObj.creator.email + '">' + commentObj.creator.firstname + '</a>' +
            '<span class="lastPosted"> created comment ' + timeSincePost + '</span>' +
            '<div class="commentLinks">' +
            (isCommentCreator ? '<a class="deleteComment" href="javascript:void(0)">\n<div class="delete-icon">\n<span original-title="Delete" class="fontawesome-remove scnd-font-color"></span>\n</div>\n</a>\n<a class="editComment" href="javascript:void(0)">\n<div class="edit-icon edit-comment">\n<span original-title="Edit" class="fontawesome-edit scnd-font-color"></span>\n</div>\n</a>' : '') +
            '</div>' +
            '<span class="comment-job-title">' + commentObj.creator.jobtitle + '</span>' +
            '</div>' +
            '</div>' +
            '<blockquote>' +
            '<p class="wallpostComment">' + commentObj.content + '</p>' +
            '</blockquote>' +
            '<div class="comment-edit-container" style="display: none;">' +
            '<div class="center-container">' +
            '<a class="comment button comment-edit-link" href="javascript:void(0)">UPDATE</a>' +
            '<a class="comment button comment-cancel-link" href="javascript:void(0)">CANCEL</a>' +
            '</div>' +
            '</div>' +
            '</div>';
        if (!commentDomObjectExists) {
            $("#" + objId + " .addComment").before(commentHtml);
            commentHtml = "";
        }
    }
    for (var i = 0; i < newCommentIds.length; i++) {
        var thisCommentId = newCommentIds[i];
        $('#' + thisCommentId + ' .comment-edit-container a.comment-edit-link').click(editComment);
        $('#' + thisCommentId + ' .comment-cancel-link').click(initCommentCancel);
        $('#' + thisCommentId + ' .deleteComment .delete-icon span').click(initDeleteComment);
        $('#' + thisCommentId + ' .edit-comment span').click(initCommentEdit);
    }
    //ADDING THE LISTENER IF NEW COMMENTS SHOULD BE ADDED
    commentHtml = "";
}

/** ---------------------------- */
/** NOTIFICATION & VISIBLITY   */
/** ------------------------ */

// Visability
var hidden, state, visibilityChange;
if (typeof document.hidden !== "undefined") {
    hidden = "hidden";
    visibilityChange = "visibilitychange";
    state = "visibilityState";
} else if (typeof document.mozHidden !== "undefined") {
    hidden = "mozHidden";
    visibilityChange = "mozvisibilitychange";
    state = "mozVisibilityState";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
    state = "msVisibilityState";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
    state = "webkitVisibilityState";
}

function handleVisibilityChange() {
    if (document.hidden) {
        visabilityState = 0;
    } else {
        visabilityState = 1;
    }
}

document.addEventListener(visibilityChange, handleVisibilityChange, false);

// Visability END

// Notification

function notification(name, message, image) {
    if (bNotification == "true" || bNotification == true) {
        if (!('Notification' in window)) {
            noty({
                text: 'Notifications do not work on this browser', timeout: 4000
            });
        } else {
            title = name;
            options = {
                body: message,
                tag: 'Message',
                icon: image
            };
            Notification.requestPermission(function () {
                var notification = new Notification(title, options);
                setTimeout(function () {
                    notification.close();
                }, 3000);
            });
        }
    }
}

function notificationSettings() {
    var notificationValue = true;
    if (localStorage.length != 0) {
        notificationValue = localStorage.getItem('notification');
    } else {
        notificationValue = true;
    }
    return notificationValue;
}

/** ---------------------------- */
/** GENERAL FUNCTIONALITY      */
/** ------------------------ */

//OVERWRITE JQUERY ":contains" WILL NOT LONGER BE CASE SENSITIVE
// NEW selector
jQuery.expr[':'].Contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
};

// OVERWRITES old selecor
jQuery.expr[':'].contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
};

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

//VALIDATE BOTH NEW POSTS & COMMENTS
function validate(text) {
    if (text.length < 2) {
        return "Minimum length is 2 characters!";
    }
    else if (text.length > 600) {
        return "Maximum length is 600 characters!";
    }
    else {
        return true;
    }
}