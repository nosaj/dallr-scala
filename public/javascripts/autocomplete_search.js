// /**
//  * Created by Asbjørn on 20-04-14.
//  */


$(document).ready(function () {
    dlUserList();
});

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    if (text == null || text == "") {
        return text
    }
    else {
        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }
}

function dlUserList() {
    $.ajax({
        url: "/getusers", success: function (data) {
            $(".searchboxx").keyup(function () {

                var users = data;
                var usersUnicoded = data;
                var displaydiv = document.getElementById("searchresult");
                var sTerm = document.getElementById("searchbox").value;
                var searchbox = document.getElementById("searchbox");

                var mapObj = {
                    æ: "ae",
                    ø: "oe",
                    å: "aa",
                    Æ: "AE",
                    Ø: "OE",
                    Å: "AA"
                };
                sTerm = sTerm.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
                    return mapObj[matched];
                });

                //remove old suggestions from the dropdown
                while (displaydiv.firstChild) {
                    displaydiv.removeChild(displaydiv.firstChild);
                }
                if (sTerm == "") {
                    displaydiv.style.display = "none";
                }

                for (var i = 0; i < users.length; i = i + 1) {
                    var regXSearch = "\\b" + sTerm;
                    var regX = new RegExp(regXSearch, "gi");

                    var firstnameUnicoded = usersUnicoded[i].firstname;
                    var lastnameUnicoded = usersUnicoded[i].lastname;
                    var jobTitleUnicoded = usersUnicoded[i].jobtitle;
                    var emailUnicoded = usersUnicoded[i].username;

                    firstnameUnicoded = firstnameUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
                        return mapObj[matched];
                    });
                    lastnameUnicoded = lastnameUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
                        return mapObj[matched];
                    });
                    jobTitleUnicoded = jobTitleUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
                        return mapObj[matched];
                    });
                    emailUnicoded = emailUnicoded.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
                        return mapObj[matched];
                    });

                    var firstNameCheck = regX.test(firstnameUnicoded);
                    var lastNameCheck = regX.test(lastnameUnicoded);
                    var jobTitleCheck = regX.test(jobTitleUnicoded);
                    var bothNames = firstnameUnicoded + " " + lastnameUnicoded;
                    var bothNamesCheck = regX.test(bothNames);
                    var emailCheck = regX.test(emailUnicoded);

                    if (firstNameCheck || lastNameCheck || jobTitleCheck || bothNamesCheck || emailCheck) {

                        // var userId = users[i]._id.$id;
                        var firstname = escapeHtml(users[i].firstname);
                        var lastname = escapeHtml(users[i].lastname);
                        var job = escapeHtml(users[i].jobtitle);
                        var image = users[i].image;
                        var email = users[i].username;

                        if (firstname == "?") {
                            firstname = email;
                            lastname = "";
                        }
                        if (job == "?") {
                            job = "No job title";
                        }

                        var boldsTerm = '<b>' + sTerm + '</b>';
                        var final_fname = firstname;
                        var final_lname = lastname;
                        var final_job = job;
                        var final_email = email;

                        //replace queried characters with bold characters (case sensitive so far)
                        if (firstNameCheck) {
                            final_fname = firstname.replace(sTerm, boldsTerm);
                        }
                        if (lastNameCheck) {
                            final_lname = lastname.replace(sTerm, boldsTerm);
                        }
                        if (jobTitleCheck) {
                            final_job = job.replace(sTerm, boldsTerm);
                        }
                        if (bothNamesCheck) {
                            final_fname = bothNames.replace(sTerm, boldsTerm);
                            final_lname = "";
                        }

                        //either make it static so we always have a string starting with big case and folloewd by small case.
                        //or cahnge to function to just make the entire string bold. eg on firstname check, entire fn will be bold.

                        var myLink = document.createElement('a');

                        myLink.setAttribute("href", "/profiles/" + final_email);

                        var myNameSpan = document.createElement('span');
                        myNameSpan.setAttribute("style", "font-size:16px;");
                        myNameSpan.innerHTML = final_fname + " " + final_lname;
                        // myNameSpan.innerHTML = '<br/>' + final_email;
                        myNameSpan.className = "autoCompName";

                        var myJobSpan = document.createElement('span');
                        myJobSpan.setAttribute("style", "font-size:14px;color:#999");
                        myJobSpan.innerHTML = '<br/>' + final_job;

                        var myIdSpan = document.createElement('span');
                        myIdSpan.setAttribute("style", "display:none");
                        // myIdSpan.innerHTML = userId;
                        myIdSpan.className = "autoCompId";
                        //is this used for anything?

                        var myImgDiv = document.createElement('img');
                        // var imageSrc = '/img/' + image;
                        var imageSrc = image;
                        myImgDiv.setAttribute("src", imageSrc);

                        var myDiv = document.createElement('div');
                        myDiv.className = "autoCompResult";
                        myDiv.appendChild(myImgDiv);
                        myDiv.appendChild(myNameSpan);
                        myDiv.appendChild(myIdSpan);
                        myDiv.appendChild(myJobSpan);

                        myLink.appendChild(myDiv);
                        displaydiv.appendChild(myLink);

                        if (sTerm != "") {
                            displaydiv.style.display = "block";
                        }
                    }
                }
            });
        }, dataType: "json"
    });
}

