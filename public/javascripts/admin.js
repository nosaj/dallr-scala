/**
 * Created by Jason on 19-08-14.
 */

var bNotificationSetting = localNoficationSettings()

$(document).ready(function () {
    $('.dropdown-bar').fadeIn(0);
    $('.admin-page .right-container-single .user-invite-button-admin').click(toggleUserInvites);
    $('.admin-page .right-container-single .browser-noty-button-admin').click(enableNotifications);
    $(".dropdown-bar li:first-child a").removeClass("settings-sublink").attr("href", "/admin");
    $(".dropdown-bar .setting-admin-link").text("Admin");
    $( ".dropdown-bar" ).fadeIn(0);

    if(bNotificationSetting == "true" || bNotificationSetting == true){
        $("#notification-settings").addClass("selected-bookmark");
        $("#notification-settings").val("Browser notifications enabled");
        $("#notification-settings").parent().find('span').addClass("selected-bookmark");
    } else {
        $("#notification-settings").removeClass("selected-bookmark");
        $("#notification-settings").val("Browser notifications disabled");
        $("#notification-settings").parent().find('span').removeClass("selected-bookmark");
    }
    loadInvites();
    loadAdmins();
});

function initDemoteAdmin(){
    var thisObject = $(this);
    if( $('.admin-user-container').length < 2){
        noty({text: 'There must always be atleast 1 admin at all times', timeout: 2000, type: 'error'});
    }else{
        var userId = $(this).parent().find('.userId').val();
        noty({
            text: 'Do you wish to demote this user from admin?',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                    $noty.close();
                    demoteAdmin(userId,thisObject);
                }
                },
                {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });
    }
}

function demoteAdmin(userId,thisObject){
    $.ajax({
        type: "POST",
        url: "/demoteadmin?userid=" + userId,
    }).done(function (result) {
        if (result == 'success') {
                thisObject.parent().fadeOut();
            noty({text: 'User successfully demoted!', timeout: 2000, type: 'success'});
            }else{
            noty({text: 'Admin could not be demoted!', timeout: 2000, type: 'error'});
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            noty({text: 'Error creating wallpost!', timeout: 2000, type: 'error'});
        })
        .always(function () {

        });

}
function disabledInvite() {
    noty({text: 'Public inviting has been disabled by the admin', timeout: 2000, type: 'error'});
}
function toggleUserInvites() {
    var thisObject = $(this);
    thisObject.unbind('click');
    $('html').addClass('wait-mouse-cursor');
    $.ajax({
        type: "POST",
        url: "/toggleinvites"
    }).done(function (result) {
        if (result == 'success') {
            if (thisObject.hasClass("selected-bookmark")) {
                thisObject.removeClass("selected-bookmark");
                thisObject.val("Public user invites disabled");
                thisObject.parent().find('span').removeClass("selected-bookmark");
                $('.invite-sublink').removeClass('invite-sublink').addClass('disabled-invite-sublink').unbind("click").bind('click', disabledInvite);
                ;
                noty({text: 'Public user invites sucessfully disabled!', timeout: 2000, type: 'success'});
            } else {
                thisObject.addClass("selected-bookmark");
                thisObject.val("Public user invites enabled");
                thisObject.parent().find('span').addClass("selected-bookmark");
                $('.disabled-invite-sublink').addClass('invite-sublink').removeClass('disabled-invite-sublink').unbind("click").bind('click', initInviteOverlay);
                noty({text: 'Public user invites sucessfully enabled!', timeout: 2000, type: 'success'});
            }
                return true;
            }else{
                noty({text: 'Public user invites could not be enabled!', timeout: 2000, type: 'error'});
                return false;
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            noty({text: 'User invites could not be set!', timeout: 2000, type: 'error'});
            return false;
        }).always(function(){
        thisObject.bind('click', toggleUserInvites);
            $('html').removeClass('wait-mouse-cursor');
        });
}

function localNoficationSettings(){

    var notificationValue = true;
    if (localStorage.length != 0) {
        notificationValue = localStorage.getItem('notification');
    } else {
        notificationValue = true;
    }

    return notificationValue;

}

function enableNotifications(){
    if ($(this).hasClass("selected-bookmark")) {
        $(this).removeClass("selected-bookmark");
        $(this).val("Browser notifications disabled");
        $(this).parent().find('span').removeClass("selected-bookmark");

        notifcationDisabled();

    } else {
        $(this).addClass("selected-bookmark");
        $(this).val("Browser notifications enabled");
        $(this).parent().find('span').addClass("selected-bookmark");

        notifcationEnabled()

    }
}

function notifcationDisabled() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        try {
            localStorage.setItem('notification', false);
        } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
                alert('Quota exceeded!');
            }
        }
    } else {
        localStorageLimitExceeded();
    }
}

function notifcationEnabled() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        try {
            localStorage.setItem('notification', true);
        } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
                localStorageLimitExceeded();
            }
        }
    } else {
        localStorageLimitExceeded();
    }
}

function localStorageLimitExceeded(){
    noty({text: 'Cannot store user preferences as your browser do not support local storage', timeout: 2000, type: 'error'});
}

function loadInvites() {
    $('html').addClass('wait-mouse-cursor');
    var addURL = "/loadinvites";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: addURL
    }).done(function (result) {
        renderInvitesHtml(result)
    }).fail(function (jqXHR, textStatus, errorThrown) {
        noty({text: 'Error loading invites', timeout: 2000, type: 'error'});
    }).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}

function loadAdmins() {
    $('html').addClass('wait-mouse-cursor');
    var addURL = "/loadadmins";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: addURL
    }).done(function (result) {
        renderAdminsHtml(result)
    }).fail(function (jqXHR, textStatus, errorThrown) {
        noty({text: 'Error loading admins', timeout: 2000, type: 'error'});
    }).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}
function renderInvitesHtml(invites) {
    for (var i = 0; i < invites.length; i++) {
        var userInvited = invites[i].invitedEmail;
        var invitedBy = invites[i].userEmail;
        var hasJoined = invites[i].userJoined;
        var invitedDate = formatDateTime(invites[i].modified.$date, hasJoined);
        prependInvite(userInvited, invitedBy, invitedDate, hasJoined)
    }
    $('.admin-page .left-container .menu-box-menu .icon').tipsy({offset: 0});

}
function renderAdminsHtml(admins) {
    for (var i = 0; i < admins.length; i++) {
        var name = admins[i].firstname + " " + admins[i].lastname;
        var id = admins[i]._id.$oid;
        var image = admins[i].image;
        prependAdmin(name, id, image)
    }
    $('.admin-page .middle-container .icon').tipsy({offset: 0});
    $('.admin-page .middle-container .fontawesome-circle-arrow-down').click(initDemoteAdmin);
}

function prependInvite(userInvited, invitedBy, invitedDate, hasJoined) {
    var thumbsUpOrDown = hasJoined ? "fontawesome-thumbs-up" : "fontawesome-thumbs-down";
    var content = '' +
        '<li>' +
        '<div class =\"menu-box-tab\">' +
        '<span>' + userInvited + '</span>' +
        '<div class=\"icon-container\">' +
        '<span original-title=\"Invited by ' + invitedBy + '\" class=\"icon fontawesome-info-sign scnd-font-color\"></span>' +
        '<span original-title=\"' + invitedDate + '\" class=\"icon ' + thumbsUpOrDown + ' scnd-font-color"></span>' +
        '</div>' +
        '</div>' +
        '</li>';
    $('.left-container ul.menu-box-menu').prepend(content);
}
function prependAdmin(name, id, image) {
    var content = '' +
        '<div class="admin-user-container">' +
        '<input type="hidden" class="userId" value="' + id + '">' +
        '<div class="profile-picture big-profile-picture clear">' +
        '<img width="150px" src="' + image + '" alt="' + name + '">' +
        '</div>' +
        '<div class="profile-name">' +
        '<h1 class="user-name">' + name + '</h1>' +
        '</div>' +
        '<span original-title="Demote from admin" class="icon fontawesome-circle-arrow-down scnd-font-color" original-title="Accept"></span>' +
        '</div>';

    $('.middle-container div.menu-box').append(content);
}

function formatDateTime(date, hasJoined) {
    var dateObj = new Date(date);
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var joinedOrInvited = hasJoined ? "Joined" : "Invited";
    return joinedOrInvited + " on the " + dateObj.getDate() + " " + monthNames[dateObj.getMonth()] + " " + dateObj.getFullYear() + " " + dateObj.getHours() + ":" + dateObj.getMinutes();
}
