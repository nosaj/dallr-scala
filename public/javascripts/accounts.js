/**
 * Created by Jason on 17-08-14.
 */
$(document).ready(function () {
    $('#forgotPassword').click(initForgotPassword);
    $("#login-container .tag-add-button").click(greenButton);
    $("#login-container .tag-cancel-button").click(redButton);
    $('#joinOrCreate').click(initSignup);
    $('#login-container input').keypress(function (e) {
        if (e.which == 13) {
            if($('#login-container h2.titular').text() == "LOGIN"){
                login();
            }else if($('#login-container h2.titular').text() == "SIGNUP USER"){
                signup();
            }
            $('form#login').submit();
            return false;    //<---- Add this line
        }
    });

});
function initForgotPassword(){
    $("#login-container .titular").text("RESET PASSWORD");
    $("#login-container input[type='password']").fadeOut(0);
    $("#login-container .password-icon").fadeOut(0);
    $("#login-container .company-password-icon").fadeOut(0);
    $("#login-container .user-email-signup").fadeIn(0);
    $("#login-container .email-icon").fadeIn(0);
    $("#login-container .select-question-signup").fadeIn(0);
    $("#login-container .select-question-icon").fadeIn(0);
    $("#login-container .answer-question-signup").fadeIn(0);
    $("#login-container .answer-question-icon").fadeIn(0);
    $(this).fadeOut(0);
    $('#login-captcha').fadeOut(0);
    $("#login-container .tag-add-button").addClass('reset-password-button').text("SEND");
    $("#login-container .tag-cancel-button").addClass('cancel-password-button').text("CANCEL");

}
function cancelForgotPassword(email,password){
    email = email || "";
    password = password || "";
    $("#login-container .titular").text("LOGIN");
    //$("#login-container input[type='text']").val(email).fadeIn(0);
    $("#login-container .login-email input[type='text']").val(email).fadeIn(0);
    $("#login-container .email-icon").fadeIn(0);
    $("#login-container input[type='password']").val(password).fadeIn(0);
    $("#login-container .password-icon").fadeIn(0);
    $("#login-container .company-password-signup").fadeOut(0);
    $("#login-container .company-password-icon").fadeOut(0);
    $("#forgotPassword").fadeIn(0);
    $('#login-captcha').fadeIn(0);
    $("#login-container .tag-add-button").removeClass('reset-password-button').text("LOGIN");
    $("#login-container .tag-cancel-button").removeClass('cancel-password-button').text("SIGNUP");
    $("#login-container select ").fadeOut(0);
    $("#login-container .company-icon ").fadeOut(0);
    $("#login-container .company-name-signup ").fadeOut(0);
    $("#login-container .tag-add-button").unbind('click');
    $("#login-container .tag-add-button").click(greenButton);
    $('html').removeClass('wait-mouse-cursor');
}

function greenButton(){
    if($('.tag-add-button').text() == "SEND")
        resetPassword();
    else if ($('.tag-add-button').text() == "CREATE" || $('.tag-add-button').text() == "JOIN") {
        signup();
    }else
        login();
}
function redButton(){
    if($(this).text() == "CANCEL")
        cancelForgotPassword();
    else
        initSignup();
}

function resetPassword(){
var answer = $('#securityAnswer').val();
    if(resetPwValidation()){
        if(checkWhitespace(answer)){
            noty({
                text: 'Your answer contains leading or trailing white spaces. do you want to continue?',
                buttons: [
                    {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                        $noty.close();
                        resetPasswordPostAction();
                    }},
                    {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                        $noty.close();
                    }}
                ]
            });
        }
        else{
            resetPasswordPostAction();
        }
    }
}

function resetPasswordPostAction(){
    $("#login-container .tag-add-button").unbind('click');
    $('html').addClass('wait-mouse-cursor');
    var username = $('#newUserEmail').val();
    var question = $('#securityQuestion').val();
    var answer = $('#securityAnswer').val()
    // TODO: MAKE THIS WORK WITH PLAY
    $.post("/resetpassword?email=" + username + "&sQuestion=" + question + "&sAnswer=" + answer,
        function (data) {
            if (data) {
                if (data == "success") {
                    noty({text: "Password successfully reset and sent to your email", timeout: 2000, type: 'success'});
                    $('html').removeClass('wait-mouse-cursor');
                    setTimeout(function () {
                        location.href = "/login"
                    }, 2000);
                }
                else {
                    noty({text: "Error resetting your password!", timeout: 4000, type: 'error'});
                    //bind reset button
                    //stop loading curser
                    $('html').removeClass('wait-mouse-cursor');
                    $("#login-container .tag-add-button").click(greenButton);
                }
            }
            else {
                $('html').removeClass('wait-mouse-cursor');
                noty({text: 'Sorry, but something went wrong!', timeout: 2000, type: 'error'});
            }
        }
    );
}

function resetPwValidation(){
    var email = $('#newUserEmail').val();
    var question = $('#securityQuestion').val();
    var answer = $('#securityAnswer').val()

    if(email == null || email == "" || answer == null || answer == ""){
        noty({text: 'You need to enter both email and security answer!',timeout: 2000, type: 'error'});
        return false;
    }
    if(question == 0){
        noty({text: 'You need to choose your security question!',timeout: 2000, type: 'error'});
        return false;
    }

    if(!email.match(/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/)){
        noty({text: 'This is not a valid email!',timeout: 2000, type: 'error'});
        return false
    }
    return true;
}
function validateEmailAndPw() {
    var email = $('#newUserEmail').val();
    var password = $('#newUserPass').val();
    var captcha = $('#login-captcha');
    var captchafield =  $('#newCaptchaPass').val();

    if(email == null || email == "" || password == null || password == ""){
        noty({text: 'You need to enter both username and password!',timeout: 2000, type: 'error'});
        return false;
    }
    if (!password.match(/^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%]{6,20}$/)  ) {
        //must contain a number, a letter and be between 6 and 20 in length. allowed characters also set
        noty({text: 'Passwords need to be 6-20 characters long and contain both a letter and number.',timeout: 2000, type: 'error'});
        return false;
    }
    if(captcha.is(":visible") ){
        if(captchafield == null || captchafield == "" ){
            noty({text: 'You need to enter the Captcha',timeout: 2000, type: 'error'});
            return false
        }
    }
    if(!email.match(/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/)){
        noty({text: 'This is not a valid email!',timeout: 2000, type: 'error'});
        return false
    }
    return true;
}

function checkWhitespace(check){
    if (check.match(/^[ \s]+|[ \s]+$/)  ) {
        return true;
    }
    return false;
}

function validateCompNameAndPw() {
    var companypass = $('#newCompanyPass').val();
    var companyname = $('#newCompanyName').val();
    var question = $('#securityQuestion').val();
    var answer = $('#securityAnswer').val()

    if(answer == null || answer == ""){
        noty({text: 'You need to enter both email and security answer!',timeout: 2000, type: 'error'});
        return false;
    }
    if(question == 0){
        noty({text: 'You need to choose your security question!',timeout: 2000, type: 'error'});
        return false;
    }

    if(companyname == null || companyname == "" || companypass == null || companypass == ""){
        noty({text: 'You need to enter both a name and password for your company!',timeout: 2000, type: 'error'});
        return false;
    }
    if (!companypass.match(/^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%]{6,20}$/)  ) {
        //must contain a number, a letter and be between 6 and 20 in length. allowed characters also set
        noty({text: 'Passwords need to be 6-20 characters long and contain both a letter and number.',timeout: 2000, type: 'error'});
        return false;
    }
    return true;
}

function login(){
    //TESTING DB
    var username = $('#newUserEmail').val();
    var password = $('#newUserPass').val();
    var addURL = "/logmein";
    $.ajax({
        url: addURL,
        method: 'POST',
        dataType: 'xml text json html',
        cache: false,
        data: {username: username, password: password}
    }).done(function(data) {
        console.log(data.responseText);
        var responseData = JSON.parse(data);
        var response = responseData['response'];
        console.log(responseData);
        console.log(response);
        if(data.responseText == "correct") {
            window.href.location = "/posts";
        }else if(data.responseText == "incorrect"){
            noty({text: 'Incorrect username/password combination', timeout: 2000, type: 'error'});
        }else{
            noty({text: 'Sorry, but something went wrong' ,timeout: 4000, type: 'error'});
        }
    }).fail(function(err) {
        if(err.responseText == "correct" && err.status == 200){
            window.location = document.location.origin+"/posts";
        }else if(err.responseText == "incorrect"){
            noty({text: 'Incorrect username/password combination', timeout: 2000, type: 'error'});
        }else{
            noty({text: 'Sorry, but something went wrong' ,timeout: 4000, type: 'error'});
        }
    });

}

function initSignup(){
    $("#login-container select").fadeIn(0);
    $("#forgotPassword").fadeOut(0);
    var whatToDo = $('#joinOrCreate').val();
    if(whatToDo=='join'){
        $("#login-container .titular").text("SIGNUP USER");
        $("#login-container .company-name-signup").fadeIn(0);
        $("#login-container .company-password-signup").fadeIn(0);
        $("#login-container .company-name-signup").val('');
        $("#login-container .company-password-signup").val('');
        $("#login-container .company-icon ").fadeIn(0);
        $("#login-container .user-email-signup").fadeIn(0);
        $("#login-container .user-password-signup").fadeIn(0);
        $("#login-container .email-icon").fadeIn(0);
        $("#login-container .password-icon").fadeIn(0);
        $("#login-container .company-password-icon").fadeIn(0);
        $('#login-captcha').fadeOut(0);
        $("#login-container .tag-add-button").removeClass('reset-password-button').text("JOIN");
        $("#login-container .tag-cancel-button").removeClass('cancel-password-button').text("CANCEL");
    } else if (whatToDo == 'create') {
        $("#login-container .titular").text("SIGNUP COMPANY");
        $("#login-container .company-name-signup").fadeIn(0);
        $("#login-container .company-password-signup").fadeIn(0);
        $("#login-container .company-name-signup").val('');
        $("#login-container .company-password-signup").val('');
        $("#login-container .user-email-signup").fadeIn(0);
        $("#login-container .user-password-signup").fadeIn(0);
        $("#login-container .email-icon").fadeIn(0);
        $("#login-container .company-icon ").fadeIn(0);
        $("#login-container .password-icon").fadeIn(0);
        $("#login-container .company-password-icon").fadeIn(0);
        $('#login-captcha').fadeOut(0);
        $("#login-container .tag-add-button").removeClass('reset-password-button').text("CREATE");
        $("#login-container .tag-cancel-button").removeClass('cancel-password-button').text("CANCEL");
    }
}

function signup(){
    if(validateEmailAndPw() && validateCompNameAndPw()){
        var sAnswer = $('#securityAnswer').val();
        var companyPassword = $('#newCompanyPass').val();
        var companyPasswordCheck = $('#newCompanyPass2').val();
        var password = $('#newUserPass').val();
        var iContainWhiteSpace=null;
        if(companyPassword==companyPasswordCheck){
            if(checkWhitespace(sAnswer)){ iContainWhiteSpace="answer" }
            if(iContainWhiteSpace != null){
                noty({
                    text: 'Your '+iContainWhiteSpace+' contains leading or trailing white spaces. do you want to continue?',
                    buttons: [
                        {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                            $noty.close();
                            signupPostAction();
                        }},
                        {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                            $noty.close();
                        }}
                    ]
                });
            }
            else{
                signupPostAction();
            }
        }
        else{
            noty({text: "Company password and password check did not match", timeout: 3000, type: 'error'});
        }


    }
}

function signupPostAction(){
    var companyName = $('#newCompanyName').val();
    var companyPassword = $('#newCompanyPass').val();
    var username = $('#newUserEmail').val();
    var password = $('#newUserPass').val();
    var whatToDo = $('#joinOrCreate').val();
    var sQuestion = $('#securityQuestion').val();
    var sAnswer = $('#securityAnswer').val();
    if (whatToDo == 'join') {
        $.ajax({
            url: "/join",
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                companyName: companyName,
                companyPassword: companyPassword,
                username: username,
                password: password,
                sQuestion: sQuestion,
                sAnswer: sAnswer
            }
        }).done(function (data) {
            console.log(data.responseText);
            var responseData = JSON.parse(data);
            var response = responseData['response'];
            console.log(responseData);
            console.log(response);
            if (data.responseText == "success") {
                cancelForgotPassword($('#login-container .login-email').val(), $('#login-container input[type="password"]').val())
                noty({text: 'You have successfully signed up', timeout: 2000, type: 'success'});
            } else if (data.responseText == "userExists") {
                noty({text: 'Sorry, that user already exists. Try logging in', timeout: 4000, type: 'error'});
            } else if (data.responseText == "companyDoesNotExists") {
                noty({text: 'Sorry, that company does not exist. Try creating instead', timeout: 4000, type: 'error'});
            } else {
                noty({text: 'Sorry, but something went wrong', timeout: 4000, type: 'error'});
            }
        }).fail(function (err) {
            if (err.responseText == "success" && err.status == 200) {
                cancelForgotPassword($('#login-container .login-email').val(), $('#login-container input[type="password"]').val())
                noty({text: 'You have successfully signed up', timeout: 2000, type: 'success'});
            }
            else if (err.responseText == "userExists") {
                noty({text: 'Sorry, that user already exists. Try logging in', timeout: 4000, type: 'error'});
            } else if (err.responseText == "companyDoesNotExists") {
                noty({text: 'Sorry, that company does not exist. Try creating instead', timeout: 4000, type: 'error'});
            }
            else {
                noty({text: 'Sorry, but something went wrong', timeout: 4000, type: 'error'});
            }
        });
    } else if (whatToDo == 'create') {
        $.ajax({
            url: "/create",
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                companyName: companyName,
                companyPassword: companyPassword,
                username: username,
                password: password,
                sQuestion: sQuestion,
                sAnswer: sAnswer
            }
        }).done(function(data) {
            console.log(data.responseText);
            var responseData = JSON.parse(data);
            var response = responseData['response'];
            if(data.responseText == "success"){
                cancelForgotPassword($('#login-container .login-email').val(),$('#login-container input[type="password"]').val())
                noty({text: 'You and your company are now signed up' ,timeout: 2000, type: 'success'});
            }else if(data.responseText == "userExists"){
                noty({text: 'Sorry, that user already exists. Try logging' ,timeout: 4000, type: 'error'});
            }else if(data.responseText == "companyExists"){
                noty({text: 'Sorry, that company already exists. Try joining instead' ,timeout: 4000, type: 'error'});
            }else{
                noty({text: 'Sorry, but something went wrong' ,timeout: 4000, type: 'error'});
            }
        }).fail(function(err) {
            if(err.responseText == "success" && err.status == 200){
                cancelForgotPassword($('#login-container .login-email').val(),$('#login-container input[type="password"]').val())
                noty({text: 'You and your company are now signed up' ,timeout: 2000, type: 'success'});
            } else if (err.responseText == "userExists") {
                noty({text: 'Sorry, that user already exists. Try logging in' ,timeout: 4000, type: 'error'});
            } else if (err.responseText == "companyExists") {
                noty({text: 'Sorry, that company already exists. Try joining instead' ,timeout: 4000, type: 'error'});
            }else{
                noty({text: 'Sorry, but something went wrong' ,timeout: 4000, type: 'error'});
            }
        });
    }
}

