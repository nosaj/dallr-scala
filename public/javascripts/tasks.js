/**
 * Created by Jason on 12-08-14.
 */
var isSelectedOne;
var isSelectedTwo;
var isSelectedThree;
var isSelectedFour;
var isSelectedFive;
var filterMyTasksOnlyVar;
var filterUnassignedTasksOnlyVar;
var filterNewTasks8Var = false;
var filterNewTasks15Var = false;
var filterNewTasks30Var = false;
var sessionUser;
var lastLoadedDate = new Date().toISOString();
var thisTime = new Date();
var timeBuffer = new Date();
////////////////////////////////
// SET SESSION USER with redis?
// save user in ui? would be there after session expire? does it matter?
////////////////////////////////
$(document).ready(function () {
    sessionUser = $('#userEmail').val();
    // var socket =  io.connect('http://55.55.55.55:3000'); // we get from socket.io.js - DEVELOPMENT VERSION
//    var socket =  io.connect('http://dallr.com:3000'); // we get from socket.io.js - PRODUCTION VERSION
    $('.tasks-page .menu-box-menu li span').tipsy({offset: 5});
    $('.tasks-page .fontawesome-info-sign').click(initDescriptionOverlay);
    $('.tasks-page .delete-task-icon').click(initDeleteTask);
    $('.tasks-page .fontawesome-plus-sign').click(initAdoptTask);
    $('.tasks-page .fontawesome-minus-sign').click(initLeaveTask);
    //$('#overlay, #task-information-overlay a').click(hideDescriptionOverlay);
   // $('#overlay, #task-information-overlay .tag-add-button').click(hideDescriptionOverlay);
    //$('#overlay, #task-information-overlay .tag-edit-button').click(initEditOverlay);
    $('.new-task-button').click(initNewTaskOverlay);
    $('#new-task-overlay .tag-cancel-button').click(hideNewTaskOverlay);
    $('#new-task-overlay .tag-add-button').click(insertTask);
    $('.task-filter-button').click(initFilterTaskOverlay);
    $('#filter-task-overlay .button.tag-cancel-button').click(cancelFilterTaskOverlay);
    $('#filter-task-overlay .tag-add-button').click(saveFilterTaskOverlay);
    $('#filter-task-overlay input[type="button"]').click(toggleFilterTasks);
    $('#new-task-overlay textarea').autosize();
    $("#taskSearch").keyup(searchForTask);
    initDraggableTasks();
    addActiveLink();
    loadAllTasks();
    // setSessionUser();


    /** --------------------------- */
    /** WEBSOCKET FUNCTIONALITY   */
    /** ----------------------- */
    var mozOrChrome = window['MozWebSocket'] ? MozWebSocket : WebSocket;
    var ws = new mozOrChrome('ws://localhost:9000/taskssocket')

    ws.onopen = function (message) {

    };
    ws.onmessage = function (e) {
        console.log(e.data);
        var parsedJson = JSON.parse(e.data);
        renderTasksHtml(parsedJson, false);
        // renderPosts(e.data, false);
        // console.dir(e.data);
    };


    setInterval(fetchNewTasks, 10000);
    function fetchNewTasks() {
        var tempLastLoadedDate = lastLoadedDate;
        lastLoadedDate = new Date().toISOString();
        ws.send(tempLastLoadedDate);
    }
    // // LISTEN FOR NEW TASKS FROM app.js
    // socket.on('listen for tasks', function (tasks) {
    //    updateTasks(tasks);
    // });
    // socket.on('listen for deletes', function (deletedObjects) {
    //     removeDeletedTasks(deletedObjects);
    // });

});

function loadAllTasks() {
    $('html').addClass('wait-mouse-cursor');
    var addURL = "/loadtasks";

    $.ajax({
        type: "POST",
        dataType: "json",
        url: addURL
    }).done(function (result) {
        $(window).unbind('scroll');
        // lastResults = true;
        // renderPosts(result, true);
        renderTasksHtml(result)
    }).fail(function (jqXHR, textStatus, errorThrown) {
        noty({text: 'Error recieving posts!', timeout: 2000, type: 'error'});
    }).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}
function localStorageLimitExceeded(){
    noty({text: 'Cannot store user preferences as your browser do not support local storage', timeout: 2000, type: 'error'});
}

// function setSessionUser(){
//     $.post( "/accounts/setSessionUser",
//         function(data) {
//             if(data){
//                 sessionUser=data;
//             }
//
//         });
// }
function addActiveLink() {
    $('.main-tasks-link').addClass('activelink');
}
function initDraggableTasks() {
    $('.tasks-page .container li').draggable({ revert: 'invalid', cursor: 'move', containment: "document", zIndex: 999 });

    $('.left-container').droppable({ accept: ".menu-box-menu li", hoverClass: "drop-hover", drop: function (event, ui) {
        ui.draggable.css({"left":0,"top":0});
        var assignee=ui.draggable.find('.taskAssignee').val();
        if(assignee==sessionUser){
            updateTaskStatus(1, ui.draggable, this);
            $(this).find('ul').prepend(ui.draggable);

            callFilters();
            searchForTask();
        }
        return true;
    }});
    $('.middle-container').droppable({ accept: ".menu-box-menu li", hoverClass: "drop-hover", drop: function (event, ui) {
        ui.draggable.css({"left":0,"top":0});
        var assignee=ui.draggable.find('.taskAssignee').val();
        if(assignee==sessionUser){
            updateTaskStatus(2, ui.draggable, this);
            $(this).find('ul').prepend(ui.draggable);

            callFilters();
        searchForTask();
        }
        return true;
    }});
    $('.right-container-single').droppable({ accept: ".menu-box-menu li", hoverClass: "drop-hover", drop: function (event, ui) {
        ui.draggable.css({"left":0,"top":0});
        var assignee=ui.draggable.find('.taskAssignee').val();
        if(assignee==sessionUser){
            updateTaskStatus(3, ui.draggable, this);
        $(this).find('ul').prepend(ui.draggable);
        callFilters();
        searchForTask();
        }
        return true;
    }});
}
function initDescriptionOverlay() {
    $('.labelclass').hide();
    $('#task-information-overlay .readOnlyToggle').attr('readonly', true);
    var taskId = $(this).parent().find('.taskId').val();
    var taskDesc = $(this).parent().find('.taskDesc').text();
    var taskCreator = $(this).parent().find('.taskCreator').val();
    var taskCreated = $(this).parent().find('.taskCreated').val();
    var taskExtra = $(this).parent().find('.taskExtra').val();
    var taskAssignee = $(this).parent().find('.taskAssignee').val();
    var thisTaskContainer = $(this).parent().parent().attr('id');

    $('#taskDescriptionLi input').val("Task: "+escapeHtml(taskDesc));
    $('#taskCreatorLi input').val("Creator: "+escapeHtml(taskCreator));
    $('#taskAssigneeLi input').val("Assignee: "+escapeHtml(taskAssignee));
    $('#taskCreatedLi input').val("Created date: "+escapeHtml(taskCreated));
    $('#taskDetailsLi textarea').val("Details: "+escapeHtml(taskExtra));
    $('#task-information-overlay').fadeIn();
    $('#taskDetailsLi textarea').autosize().show().trigger('autosize.resize');
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
    $('#task-information-overlay .tag-add-button').unbind( "click" );
    $('#task-information-overlay .tag-add-button').click(hideDescriptionOverlay);
    $('#task-information-overlay .tag-edit-button').unbind( "click" );
    $('#task-information-overlay .tag-edit-button').click(function(){initEditOverlay(taskId, taskDesc, taskCreator, taskCreated, taskExtra, taskAssignee, thisTaskContainer)});
    if(sessionUser==taskCreator){
        $('#task-information-overlay .tag-edit-button').show();
    }
    else{
        $('#task-information-overlay .tag-edit-button').hide();
    }
}
function initEditOverlay(taskId, taskDesc, taskCreator, taskCreated, taskExtra, taskAssignee, thisTaskContainer) {
    $('.labelclass').show();
    $('#task-information-overlay .readOnlyToggle').attr('readonly', false);
    $('#taskIdContainer').val(escapeHtml(taskId));
    $('#taskDescriptionLi input').val(escapeHtml(taskDesc));
    $('#taskDescriptionLabel').text('Task:');
    $('#taskCreatorLi input').val(escapeHtml(taskCreator));
    $('#taskCreatorLabel').text('Creator:');
    $('#taskAssigneeLi input').val(escapeHtml(taskAssignee));
    $('#taskAssigneeLabel').text('Assignee:');
    $('#taskCreatedLi input').val(escapeHtml(taskCreated));
    $('#taskCreatedLabel').text('Created date:');
    $('#taskDetailsLi textarea').val(escapeHtml(taskExtra));
    $('#taskDetailsLabel').text('Details:');
    $('#task-information-overlay').fadeIn();
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
    $('#task-information-overlay .tag-add-button').unbind( "click" );
    $('#task-information-overlay .tag-add-button').text("CANCEL");
    $('#task-information-overlay .tag-add-button').click(function(){cancelEditOverlay(taskId, taskDesc, taskCreator, taskCreated, taskExtra, taskAssignee, thisTaskContainer)});
    $('#task-information-overlay .tag-edit-button').unbind( "click" );
    $('#task-information-overlay .tag-edit-button').text( "SAVE" );
    $('#task-information-overlay .tag-edit-button').click(function () {
        editTask(thisTaskContainer, taskAssignee)
    });
}
function cancelEditOverlay(taskId, taskDesc, taskCreator, taskCreated, taskExtra, taskAssignee, thisTaskContainer){
    $('.labelclass').hide();
    $('#task-information-overlay .readOnlyToggle').attr('readonly', true);
    $('#taskIdContainer').val(escapeHtml(taskId));
    $('#taskDescriptionLi input').val("Task: "+escapeHtml(taskDesc));
    $('#taskCreatorLi input').val("Creator: "+escapeHtml(taskCreator));
    $('#taskAssigneeLi input').val("Assignee: "+escapeHtml(taskAssignee));
    $('#taskCreatedLi input').val("Created date: "+escapeHtml(taskCreated));
    $('#taskDetailsLi textarea').val("Details: "+escapeHtml(taskExtra));
    $('#taskDetailsLi textarea').autosize().show().trigger('autosize.resize');
    $('#task-information-overlay .tag-add-button').unbind( "click" );
    $('#task-information-overlay .tag-add-button').text("DONE");
    $('#task-information-overlay .tag-add-button').click(hideDescriptionOverlay);
    $('#task-information-overlay .tag-edit-button').unbind( "click" );
    $('#task-information-overlay .tag-edit-button').text( "EDIT" );
    $('#task-information-overlay .tag-edit-button').click(function(){initEditOverlay(taskId, taskDesc, taskCreator, taskCreated, taskExtra, taskAssignee, thisTaskContainer)});
}
function hideDescriptionOverlay() {
    $("#task-information-overlay").fadeOut();
    $("#overlay").fadeOut();
}
function editTask(thisTaskContainer, originalAssignee) {
    var taskId = $('#taskIdContainer').val();
    var taskDesc = $('#taskDescriptionLi input').val();
    var taskCreator =  $('#taskCreatorLi input').val();
    var taskCreated =  $('#taskCreatedLi input').val();
    var taskExtra = $('#taskDetailsLi textarea').val();
    var taskStatus;
    var taskAssignee =  $('#taskAssigneeLi input').val();
    var taskAssigneeImg = $('.profile-picture img').attr("src");
    var thisTask = $( ".new-tasks input[value='"+taskId+"']").parent();

    if(validateEmail(taskAssignee)){
        if(originalAssignee==taskAssignee){
            if(thisTaskContainer == 'not-started-container'){
                taskStatus=1;
            }else if(thisTaskContainer == 'in-progress-container'){
                taskStatus=2;
            }else{
                taskStatus=3;
            }
        }else{
            taskStatus=1;
        }
        noty({
            text: 'Do you wish to save these changes?',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                    $noty.close();
                    $.post("/edittask?taskId=" + taskId + "&description=" + taskDesc + "&assignee=" + taskAssignee + "&extra=" + taskExtra,
                        function (data) {
                            if (data == "success") {
                                noty({text: 'Task successfully edited', timeout: 2000, type: 'success'});
                                thisTask.remove();
                                prependNewTask(taskId, taskDesc, taskExtra, taskStatus, taskAssignee, taskCreator, taskCreated, taskAssigneeImg);
                                callAfterPrepend();
                                cancelEditOverlay(taskId, taskDesc, taskCreator, taskCreated, taskExtra, taskAssignee, thisTaskContainer);
                            } else {
                                noty({text: 'Error editing this task!', timeout: 2000, type: 'error'});
                            }
                        });
                } },
                {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                    $noty.close();
                }}
            ]
        });
    }
}

function initNewTaskOverlay() {
    $('#new-task-overlay').fadeIn();
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
}
function hideNewTaskOverlay() {
    $("#new-task-overlay").fadeOut();
    $("#overlay").fadeOut();
}
function initFilterTaskOverlay() {
    isSelectedOne = $('.filter-tasks-icon-one').parent().find('input[type="button"]').hasClass('selected-bookmark') ? true : false;
    isSelectedTwo = $('.filter-tasks-icon-two').parent().find('input[type="button"]').hasClass('selected-bookmark') ? true : false;
    isSelectedThree = $('.filter-tasks-icon-three').parent().find('input[type="button"]').hasClass('selected-bookmark') ? true : false;
    isSelectedFour = $('.filter-tasks-icon-four').parent().find('input[type="button"]').hasClass('selected-bookmark') ? true : false;
    isSelectedFive = $('.filter-tasks-icon-five').parent().find('input[type="button"]').hasClass('selected-bookmark') ? true : false;
    $('#filter-task-overlay').fadeIn();
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
}
function cancelFilterTaskOverlay() {
    if (!isSelectedOne && $('.filter-tasks-icon-one').parent().find('input[type="button"]').hasClass('selected-bookmark')) {
        $('.filter-tasks-icon-one').parent().find('input[type="button"]').removeClass('selected-bookmark');
        $('.filter-tasks-icon-one').find('span').removeClass('selected-bookmark');
    } else if (isSelectedOne && $('.filter-tasks-icon-one').parent().find('input[type="button"]').hasClass('selected-bookmark') == false) {
        $('.filter-tasks-icon-one').parent().find('input[type="button"]').addClass('selected-bookmark');
        $('.filter-tasks-icon-one').find('span').addClass('selected-bookmark');
    }

    if (!isSelectedTwo && $('.filter-tasks-icon-two').parent().find('input[type="button"]').hasClass('selected-bookmark')) {
        $('.filter-tasks-icon-two').parent().find('input[type="button"]').removeClass('selected-bookmark');
        $('.filter-tasks-icon-two').find('span').removeClass('selected-bookmark');
    } else if (isSelectedTwo && $('.filter-tasks-icon-two').parent().find('input[type="button"]').hasClass('selected-bookmark') == false) {
        $('.filter-tasks-icon-two').parent().find('input[type="button"]').addClass('selected-bookmark');
        $('.filter-tasks-icon-two').find('span').addClass('selected-bookmark');
    }

    if (!isSelectedThree && $('.filter-tasks-icon-three').parent().find('input[type="button"]').hasClass('selected-bookmark')) {
        $('.filter-tasks-icon-three').parent().find('input[type="button"]').removeClass('selected-bookmark');
        $('.filter-tasks-icon-three').find('span').removeClass('selected-bookmark');
    } else if (isSelectedThree && $('.filter-tasks-icon-three').parent().find('input[type="button"]').hasClass('selected-bookmark') == false) {
        $('.filter-tasks-icon-three').parent().find('input[type="button"]').addClass('selected-bookmark');
        $('.filter-tasks-icon-three').find('span').addClass('selected-bookmark');
    }

    if (!isSelectedFour && $('.filter-tasks-icon-four').parent().find('input[type="button"]').hasClass('selected-bookmark')) {
        $('.filter-tasks-icon-four').parent().find('input[type="button"]').removeClass('selected-bookmark');
        $('.filter-tasks-icon-four').find('span').removeClass('selected-bookmark');
    } else if (isSelectedFour && $('.filter-tasks-icon-four').parent().find('input[type="button"]').hasClass('selected-bookmark') == false) {
        $('.filter-tasks-icon-four').parent().find('input[type="button"]').addClass('selected-bookmark');
        $('.filter-tasks-icon-four').find('span').addClass('selected-bookmark');
    }

    if (!isSelectedFive && $('.filter-tasks-icon-five').parent().find('input[type="button"]').hasClass('selected-bookmark')) {
        $('.filter-tasks-icon-five').parent().find('input[type="button"]').removeClass('selected-bookmark');
        $('.filter-tasks-icon-five').find('span').removeClass('selected-bookmark');
    } else if (isSelectedFive && $('.filter-tasks-icon-four').parent().find('input[type="button"]').hasClass('selected-bookmark') == false) {
        $('.filter-tasks-icon-five').parent().find('input[type="button"]').addClass('selected-bookmark');
        $('.filter-tasks-icon-five').find('span').addClass('selected-bookmark');
    }

    $("#filter-task-overlay").fadeOut();
    $("#overlay").fadeOut();
}
function toggleFilterTasks() {
    // this is where we click on the filter to set or unset them.
    if ($(this).hasClass("selected-bookmark")) {
        //this happens when we unclick a filter
        if($(this).val()=="Only my tasks"){
            filterMyTasksOnlyVar=false;
        }
        if($(this).val()=="Show 8 tasks"){
            filterNewTasks8Var=false;
        }
        if( $(this).val()=="Show 15 tasks"){
            filterNewTasks15Var=false;
        }
        if($(this).val()=="Show 30 tasks"){
            filterNewTasks30Var=false;
        }
        if($(this).val()=="Not assigned"){
            filterUnassignedTasksOnlyVar=false;
        }

        $(this).removeClass("selected-bookmark");
        $(this).parent().find('span').removeClass("selected-bookmark");
    } else {
        //this is what happens when we click a filter
        if($(this).val()=="Only my tasks"){
            filterMyTasksOnlyVar=true;
            //filterUnassignedTasksOnlyVar=false;
            //$('.filter-tasks-icon-five').parent().find('input[type="button"]').removeClass('selected-bookmark');
            //$('.filter-tasks-icon-five').find('span').removeClass('selected-bookmark');
        }
        if($(this).val()=="Show 8 tasks"){
            filterNewTasks8Var=true;
            filterNewTasks15Var=false;
            filterNewTasks30Var=false;
            $('.filter-tasks-icon-three').parent().find('input[type="button"]').removeClass('selected-bookmark');
            $('.filter-tasks-icon-three').find('span').removeClass('selected-bookmark');
            $('.filter-tasks-icon-four').parent().find('input[type="button"]').removeClass('selected-bookmark');
            $('.filter-tasks-icon-four').find('span').removeClass('selected-bookmark');
        }
        if( $(this).val()=="Show 15 tasks"){
            filterNewTasks8Var=false;
            filterNewTasks15Var=true;
            filterNewTasks30Var=false;
            $('.filter-tasks-icon-two').parent().find('input[type="button"]').removeClass('selected-bookmark');
            $('.filter-tasks-icon-two').find('span').removeClass('selected-bookmark');
            $('.filter-tasks-icon-four').parent().find('input[type="button"]').removeClass('selected-bookmark');
            $('.filter-tasks-icon-four').find('span').removeClass('selected-bookmark');
        }
        if($(this).val()=="Show 30 tasks"){
            filterNewTasks8Var=false;
            filterNewTasks15Var=false;
            filterNewTasks30Var=true;
            $('.filter-tasks-icon-two').parent().find('input[type="button"]').removeClass('selected-bookmark');
            $('.filter-tasks-icon-two').find('span').removeClass('selected-bookmark');
            $('.filter-tasks-icon-three').parent().find('input[type="button"]').removeClass('selected-bookmark');
            $('.filter-tasks-icon-three').find('span').removeClass('selected-bookmark');
        }
        if($(this).val()=="Not assigned"){
            filterUnassignedTasksOnlyVar=true;
           // filterMyTasksOnlyVar=false;
           // $('.filter-tasks-icon-one').parent().find('input[type="button"]').removeClass('selected-bookmark');
           // $('.filter-tasks-icon-one').find('span').removeClass('selected-bookmark');
        }

        $(this).addClass("selected-bookmark");
        $(this).parent().find('span').addClass("selected-bookmark");
    }
}
function saveFilterTaskOverlay() {
    //this is where we press the apply button to trigger the filter function
    callFilters();
    $("#filter-task-overlay").fadeOut();
    $("#overlay").fadeOut();
}
function callFilters(){
    $('#not-started-container li').show();
    $('#in-progress-container li').show();
    $('#completed-container li').show();
    filterMyTasksOnly();
    filterUnassignedTasksOnly();
    filterNewTasksOnly();
}
function filterMyTasksOnly(){
    if(filterMyTasksOnlyVar){
        if(filterUnassignedTasksOnlyVar){
            $('.task-container').children().each(function () {
                var assignee = $(this).find(".taskAssignee").val();
                if (assignee == sessionUser || assignee == "" || assignee == null){
                    $(this).show();
                }
                else{
                    $(this).hide();
                }
            });
        }
        else{
            $('.task-container').children().each(function () {
                var assignee = $(this).find(".taskAssignee").val();
                if (assignee != sessionUser){
                    $(this).hide();
                }
            });
        }
    }
}
function filterUnassignedTasksOnly(){
    if(filterUnassignedTasksOnlyVar){
        if(filterMyTasksOnlyVar!=true){
            $('.task-container').children().each(function () {
                var assignee = $(this).find(".taskAssignee").val();
                if (assignee == null || assignee == ""){
                    $(this).show();
                }
                else{
                    $(this).hide();
                }
            });
        }
    }
}
function filterNewTasksOnly(){
    if(filterNewTasks8Var){
        $('#not-started-container li:visible:gt(7)').hide();
        $('#in-progress-container li:visible:gt(7)').hide();
        $('#completed-container li:visible:gt(7)').hide();
    }
    if(filterNewTasks15Var){
        $('#not-started-container li:visible:gt(14)').hide();
        $('#in-progress-container li:visible:gt(14)').hide();
        $('#completed-container li:visible:gt(14)').hide();
    }
    if(filterNewTasks30Var){
        $('#not-started-container li:visible:gt(29)').hide();
        $('#in-progress-container li:visible:gt(29)').hide();
        $('#completed-container li:visible:gt(29)').hide();
    }
}
function searchForTask(){
    var mapObj = {
        æ: "ae",
        ø: "oe",
        å: "aa",
        Æ: "AE",
        Ø: "OE",
        Å: "AA"
    };
    var sTerm = $('#taskSearch').val();
    sTerm = sTerm.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
        return mapObj[matched];
    });
    var regXSearch = "\\b" + sTerm;
    var regX = new RegExp(regXSearch, "gi");

    $('.task-container').children().each(function () {
        var thisTaskDesc = $(this).find(".taskDesc").text();
        var thisTaskExtra = $(this).find(".taskExtra").val();
        thisTaskDesc = thisTaskDesc.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
            return mapObj[matched];
        });
        thisTaskExtra = thisTaskExtra.replace(/æ|ø|å|Æ|Ø|Å/gi, function (matched) {
            return mapObj[matched];
        });
        var doesTaskMatch1 = regX.test(thisTaskDesc);
        var doesTaskMatch2 = regX.test(thisTaskExtra);
        if(doesTaskMatch1||doesTaskMatch2){
            $(this).show();
            //$(this).css("visibility", "visible");
            //$(this).css("opacity", "1");
            if(sTerm==""){
                //don't call filters if you go with opacity option.
                callFilters();
                //$(this).show();
                //$(this).css("visibility", "visible");
                //$(this).css("opacity", "1");
            }
        }
        else{
            $(this).hide();
            //$(this).css("visibility", "hidden");
            //$(this).css("opacity", "0.3");
        }
    });
}

function initAdoptTask(){
    var taskId = $(this).parent().find('.taskId').val();
    var taskDesc = $(this).parent().find('.taskDesc').text();
    var taskStatus = 1;
    var taskCreator = $(this).parent().find('.taskCreator').val();
    var taskCreated = $(this).parent().find('.taskCreated').val();
    var taskExtra = $(this).parent().find('.taskExtra').val();
    var taskAssignee = sessionUser;
    var taskAssigneeImg = $('.profile-picture img').attr("src");

    var thisTask =  $(this).parent();
    noty({
        text: 'Do you wish to take this Task?',
        buttons: [
            {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                $noty.close();
                $.post("/adopttask?taskId=" + taskId,
                    function (data) {
                        if (data == "success") {
                            noty({text: 'Task successfully adopted', timeout: 2000, type: 'success'});
                            thisTask.remove();
                            prependNewTask(taskId, taskDesc, taskExtra, taskStatus, taskAssignee, taskCreator, taskCreated, taskAssigneeImg);
                            callAfterPrepend();
                        } else {
                            noty({text: 'Error adopting this task!', timeout: 2000, type: 'error'});
                        }
                    });
            }
            },
            {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                $noty.close();
            }}
        ]
    });
}

function initLeaveTask(){
    var taskId = $(this).parent().find('.taskId').val();
    var taskDesc = $(this).parent().find('.taskDesc').text();
    var taskStatus = 1;
    var taskCreator = $(this).parent().find('.taskCreator').val();
    var taskCreated = $(this).parent().find('.taskCreated').val();
    var taskExtra = $(this).parent().find('.taskExtra').val();
    var taskAssignee = null;
    var taskAssigneeImg = "assets/images/noImage.gif"
    var thisTask =  $(this).parent();
    noty({
        text: 'Do you wish to leave this Task?',
        buttons: [
            {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                $noty.close();
                $.post("/leavetask?taskId=" + taskId,
                    function (data) {
                        if (data == "success") {
                            noty({text: 'Task successfully left', timeout: 2000, type: 'success'});
                            thisTask.remove();
                            prependNewTask(taskId, taskDesc, taskExtra, taskStatus, taskAssignee, taskCreator, taskCreated, taskAssigneeImg);
                            callAfterPrepend();
                        } else {
                            noty({text: 'Error leaving this task!', timeout: 2000, type: 'error'});
                        }
                    });
            }},
            {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                $noty.close();
            }}
        ]
    });
}

function insertTask() {

    var desc = $('#newTaskDesc').val();
    var assignee = $('#newTaskAssignee').val();
    var extra = $('#newTaskExtra').val();
    var status = 1;
    if(desc == null || desc == ""){
        noty({text: 'You need to give a description!',timeout: 2000, type: 'error'});
        return false;
    }
    if(validateEmail(assignee)){
        $.post("/inserttask?description=" + desc + "&assignee=" + assignee + "&extra=" + extra,
            function (data) {
                if (data) {
                    if (data == "failure") {
                        noty({text: 'Error creating Task!', timeout: 2000, type: 'error'});
                    } else {
                        hideNewTaskOverlay();
                        var jsonTaskId = data._id.$oid;
                        var jsonDescription = data.desc;
                        var jsonAssignee = data.assignee;
                        var jsonCreator = data.creator;
                        var jsonAssigneeImg = data.assigneeImg;
                        var jsonExtra = data.extra;
                        var jsonStatus = data.status;
                        var createdDateObj = formatDate(data.created.$date);
                        noty({text: 'Task successfully created', timeout: 2000, type: 'success'});
                        $('#newTaskDesc').val("");
                        $('#newTaskAssignee').val("");
                        $('#newTaskExtra').val("");
                        prependNewTask(jsonTaskId, jsonDescription, jsonExtra, jsonStatus, jsonAssignee, jsonCreator, createdDateObj, jsonAssigneeImg);
                        callAfterPrepend();
                    }
                } else {
                    noty({text: 'Error creating Task!', timeout: 2000, type: 'error'});
                }
            }
        );
    }
}

function validateEmail(email){
    if(email==""||email==null){
        return true;
    }
    else{
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
        if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
            noty({text: 'You need to enter a valid email.',timeout: 2000, type: 'error'});
            return false;
        }
        else{
            return true;
        }
    }

}
function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    if(text == null || text == ""){
        return text
    }
    else{
        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }

}
function prependNewTask(taskId,taskDesc,taskExtra,taskStatus,taskAssignee,taskCreator,taskCreated, taskAssigneeImg){
    var content='' +
        '<li id="' + taskId + '" class="new-tasks">' +
        '<img src='+"'"+escapeHtml(taskAssigneeImg)+"'"+ 'alt="pic" style="float:left" /> ' +
        //'<img src='+"'https://dallr.s3-eu-west-1.amazonaws.com/Users/"+ escapeHtml(taskAssigneeImg )+"'"+ 'alt="pic" style="float:left" /> ' +
        '<span class="taskDesc" style="float:left; font-size:14px;color:#999">'+escapeHtml(taskDesc)+'</span>'+
        '<input class="taskExtra" type="hidden" value='+"'"+escapeHtml(taskExtra)+"'"+'>'+
        '<input class="taskCreator" type="hidden" value='+"'"+escapeHtml(taskCreator)+"'"+'>'+
        '<input class="taskCreated" type="hidden" value='+"'"+escapeHtml(taskCreated)+"'"+'>'+
        '<input class="taskAssignee" type="hidden" value='+"'"+escapeHtml(taskAssignee)+"'"+'>'+
        '<input class="taskId" type="hidden" value='+"'"+escapeHtml(taskId)+"'"+'>';

    var span=''+
        '<span original-title="View / Edit Details" class="fontawesome-info-sign scnd-font-color"></span>' +
       '</li>';

    var assignSpan=''+
        '<span original-title="View / Edit Details" class="fontawesome-info-sign scnd-font-color"></span>' +
        '<span original-title="Adopt Task" class="adopt-task-icon fontawesome-plus-sign scnd-font-color"></span>'+
        '</li>';

    var leaveSpan=''+
        '<span original-title="View / Edit Details" class="fontawesome-info-sign scnd-font-color"></span>' +
        '<span original-title="Leave Task" class="leave-task-icon fontawesome-minus-sign scnd-font-color"></span>'+
        '<span original-title="Delete Task" class="delete-task-icon fontawesome-remove scnd-font-color"></span>' +
        '</li>';

    if(taskStatus==1){
        if(!taskAssignee){
            $('#not-started-container').prepend(content+assignSpan);
        }
        else{
            if(taskAssignee==sessionUser){
                $('#not-started-container').prepend(content+leaveSpan)
            }
            else{
                $('#not-started-container').prepend(content+span)
            }
        }
    }
    else if(taskStatus==2){
        if(taskAssignee==sessionUser){
            $('#in-progress-container').prepend(content+leaveSpan)
        }
        else{
            $('#in-progress-container').prepend(content+span)
        }
    }
    else {
        if(taskAssignee==sessionUser){
            $('#completed-container').prepend(content+leaveSpan)
        }
        else{
            $('#completed-container').prepend(content+span)
        }
    }
}
function callAfterPrepend(){
    $(".tipsy").remove();
    $('.tasks-page .menu-box-menu li span').unbind('tipsy').tipsy({offset: 5});
    $('.tasks-page .fontawesome-info-sign').unbind('click').click(initDescriptionOverlay);
    $('.tasks-page .delete-task-icon').unbind('click').click(initDeleteTask);
    $('.tasks-page .fontawesome-plus-sign').unbind('click').click(initAdoptTask);
    $('.tasks-page .fontawesome-minus-sign').unbind('click').click(initLeaveTask);
    initDraggableTasks();
    callFilters();
    searchForTask();
    /////////////////////////////////////////////////////////
    //check these to see if they also bind multiple handlers.
    /////////////////////////////////////////////////////////
}
function updateTaskStatus(newStatus, element, thisRow) {
    var taskId = element.find('.taskId').val();
    var taskAssignee = element.find('.taskAssignee').val();
    $.post("/swaptaskstatus?taskId=" + taskId + "&status=" + newStatus,
        function (data) {
            if (data == 'success') {

            } else {
                // TODO: ADD REVERT TO ORIGINAL POSITION HERE
                noty({text: 'Error updating Task!', timeout: 2000, type: 'error'});
            }
        }
    );
}
function initDeleteTask(){
    var thisElement = $(this);
    noty({
        text: 'Do you wish to delete this Task?',
        buttons: [
            {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                $noty.close();
                deleteTask(thisElement);
            }},
            {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                $noty.close();
            }}
        ]
    });
}
function deleteTask(thisElement){
    var taskId = thisElement.parent().find('.taskId').val();
    $.post("/deletetask?taskId=" + taskId,
        function (data) {
            if (data == "success") {
                noty({text: 'Task successfully deleted', timeout: 2000, type: 'success'});
                thisElement.parent().remove();
                callFilters();
                searchForTask();
            } else {
                noty({text: 'Error deleting Task!', timeout: 2000, type: 'error'});
            }
        }
    );
}
function renderTasksHtml(tasks) {
    for (var i = 0; i < tasks.length; i++) {
        var taskId = tasks[i]._id.$oid;
        if ($('#' + taskId).length != 0) $('#' + taskId).remove();
        var taskDesc = tasks[i].desc;
        var taskExtra = tasks[i].extra;
        var taskStatus = tasks[i].status;
        var taskCreator = tasks[i].creator;
        var taskCreated = formatDate(tasks[i].created.$date);
        var taskAssigneeImg = tasks[i].assigneeImg;
        var taskAssignee = "";
        if (tasks[i].assignee) {
            taskAssignee = tasks[i].assignee;
        }
        prependNewTask(taskId, taskDesc, taskExtra, taskStatus, taskAssignee, taskCreator, taskCreated, taskAssigneeImg)
    }
    callAfterPrepend();
}
function formatDate(date) {
    var dateObj = new Date(date);
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return dateObj.getDate() + " " + monthNames[dateObj.getMonth()] + " " + dateObj.getFullYear();
}
// function updateTasks(tasks){
//     for(var i = 0; i < tasks.length; i++) {
//         var taskId = tasks[i]['_id'];
//         $('.task-container').children().each(function () {
//             var thisTaskId = $(this).find(".taskId").val();
//             if(thisTaskId==taskId){
//                 $(this).remove();
//             }
//         });
//         var taskDesc = tasks[i]['desc'];
//         var taskExtra = tasks[i]['extra'];
//         var taskStatus = tasks[i]['status'];
//         var taskCreator = tasks[i]['creator'];
//         var createdDateObj = new Date(tasks[i]['created']);
//         var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
//         var taskCreated = createdDateObj.getDate() +" "+ monthNames[createdDateObj.getMonth()] +" "+ createdDateObj.getFullYear();
//         var taskAssigneeImg = tasks[i]['assigneeImg'];
//         //var taskAssignee = tasks[i]['assignee'];
//         var taskAssignee = "";
//         if(tasks[i]['assignee']){
//             taskAssignee = tasks[i]['assignee']
//         }
//         prependNewTask(taskId,taskDesc,taskExtra,taskStatus,taskAssignee,taskCreator,taskCreated, taskAssigneeImg)
//     }
//     callAfterPrepend();
// }

function removeDeletedTasks(deletedObjects){
    //alert(deleted_objects);
    for(var i = 0; i < deletedObjects.length; i++) {

        var taskId = deletedObjects[i]['deletedId'];
        // when merging with jason implement type check!
        $('.task-container').children().each(function () {
            var thisTaskId = $(this).find(".taskId").val();
            if(thisTaskId==taskId){
                $(this).remove();
            }
        });
    }
    callAfterPrepend();
}

/*
(function updateTaskList() {
    setTimeout(function() {
        $.ajax({ url: "/tasks/updateTaskList", success: function(data) {
            for (var i = 0; i < data.length; i = i + 1) {
                var taskId;
                if(data[i].hasOwnProperty('Deleted_Object')){
                     taskId = data[i]['Deleted_Object']['_id'];
                }
                else if(data[i].hasOwnProperty('Task')){
                    taskId = data[i]['Task']['_id'];
                }
                $('.task-container').children().each(function () {
                    var thisTaskId = $(this).find(".taskId").val();
                    if(thisTaskId==taskId){
                        $(this).remove();
                    }
                });
               if(data[i].hasOwnProperty('Task')){
                   var taskDesc = data[i]['Task']['desc'];
                   var taskStatus = data[i]['Task']['status'];
                   var taskExtra = data[i]['Task']['extra'];
                   var taskCreator = data[i]['Task']['creator'];
                   var taskCreated = data[i]['Task']['created'];
                   var taskAssignee = "";
                   var taskAssigneeImg = data[i]['Task']['assigneeImg'];
                   if(data[i]['Task']['assignee']){
                       taskAssignee = data[i]['Task']['assignee']
                   }
                   prependNewTask(taskId,taskDesc,taskExtra,taskStatus,taskAssignee,taskCreator,taskCreated,taskAssigneeImg);
               }
            callAfterPrepend();
            }
        }, dataType: "json", complete: updateTaskList });
    }, 30000);
})();
*/









