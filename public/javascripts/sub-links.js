var boolAdmin;
var bNotificationSetting = localNoficationSettings();
if(bNotificationSetting == false || bNotificationSetting == null){
    notifcationEnabled();
}

$(document).ready(function () {
    // var socket =  io.connect('http://55.55.55.55:3000'); // we get from socket.io.js - DEVELOPMENT VERSION
//    var socket =  io.connect('http://dallr.com:3000'); // we get from socket.io.js - PRODUCTION VERSION

    // socket.emit('new user', 1, function(data){
    //     if(data == false){
    //         noty({
    //             text: 'Something went wrong, try to refresh browser og login again', timeout: 3000
    //         });
    //     }
    // });

    // var pathArray = window.location.pathname.split( '/' );
    //
    // if(pathArray[1] == "tasks"){
    //
    //     taskTimemanager();
    //     setInterval(function(){taskTimemanager()}, 5000);
    //
    // }
    //
    // var tasktempLastLoadedDate = getTaskTime();
    //
    // setTimeout(function(){
    //     socket.emit('get notifications', {date:tasktempLastLoadedDate});
    // }, 500);
    //
    // setInterval(function(){
    //     tasktempLastLoadedDate = getTaskTime();
    //     socket.emit('get notifications', {date:tasktempLastLoadedDate});
    //
    // }, 5000);


    // socket.on("global message notifications", function(data){
    //     if(data == 0){
    //         $(".header-menu li:nth-child(4) .header-menu-number").text("");
    //     } else {
    //         $(".header-menu li:nth-child(4) .header-menu-number").text(data);
    //     }
    // });
//
//     if(pathArray[1] != "tasks"){
//         socket.on("task global notifications", function(data){
//             if(data == 0){
//                 $(".header-menu li:nth-child(5) .header-menu-number").text("");
//             } else {
//                 $(".header-menu li:nth-child(5) .header-menu-number").text(data);
//             }
//         });
//     } else {
// //        setTimeout(function(){
//             socket.on("task global notifications", function(data){
//                 if(data == 0){
//                     $(".header-menu li:nth-child(5) .header-menu-number").text("");
//                 } else {
//                     $(".header-menu li:nth-child(5) .header-menu-number").text(data);
//                 }
//             });
// //        }, 5000);
//     }

    $('.nav-close-link, .profile-menu').click(toggleBarMenu);
    $('.settings-sublink').click(function(e){
        e.preventDefault();
        $('#settings-overlay').fadeIn();
        var docHeight = $(document).height();
        $("#overlay").height(docHeight).fadeIn();
        return false;
    });
    $('#settings-overlay .tag-add-button').click(saveSettingsOverlay);
    $('#settings-overlay .tag-cancel-button').click(cancelSettingsOverlay);
    $('.invite-sublink').click(initInviteOverlay);
    $('.disabled-invite-sublink').click(disabledInvite);
    $('#invite-overlay .tag-add-button').click(submitEmailOverlay);
    $('#invite-overlay .tag-cancel-button').click(cancelEmailOverlay);
    $('.help-sublink').click(initHelpOverlay);
    $('#help-overlay .tag-cancel-button').click(cancelHelpOverlay);
    $('#help-overlay .tag-add-button').click(backToMainHelp);
    $('#help-overlay input[type="button"]').click(openHelpSubject);
    $('.browser-noty-button').click(enableNotification);
});
function toggleBarMenu(){
    $( ".dropdown-bar" ).toggle( "blind", 600 );
    if($( ".arrow-expand" ).hasClass("entypo-down-open"))
        $( ".arrow-expand" ).removeClass("entypo-down-open").addClass("entypo-up-open");
    else
        $( ".arrow-expand" ).removeClass("entypo-up-open").addClass("entypo-down-open");
}

function saveSettingsOverlay() {
    $("#settings-overlay").fadeOut();
    $("#overlay").fadeOut();

    var notificationVal = $(".individual-task-filter input:first-child").val();
    handleNotificationVal(notificationVal);

}

function cancelSettingsOverlay() {
    $("#settings-overlay").fadeOut();
    $("#overlay").fadeOut();
}
function enableNotification(){
    if ($(this).hasClass("selected-bookmark")) {
        $(this).removeClass("selected-bookmark");
        $(this).val("Browser notifications disabled");
        $(this).parent().find('span').removeClass("selected-bookmark");
    } else {
        $(this).addClass("selected-bookmark");
        $(this).val("Browser notifications enabled");
        $(this).parent().find('span').addClass("selected-bookmark");
    }
}
function initInviteOverlay() {
    $('#invite-overlay').fadeIn();
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
}
function submitEmailOverlay() {
    $(this).unbind("click");
    var email = $("#invite-overlay input[type='text']").val();
    if(validateEmail(email)){
        $('html').addClass('wait-mouse-cursor');
        $("#invite-overlay").fadeOut();
        $("#overlay").fadeOut();
        $.ajax({
            type: "POST",
            url: "/inviteuser?email=" + email
        }).done(function (result) {
            if (result == 'success') {
                    noty({text: 'Invite sucessfully sent to '+email,timeout: 2000, type: 'success'});
                }else{
                noty({text: 'Error sending invite!', timeout: 2000, type: 'error'});
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                noty({text: 'Error sending invite!', timeout: 2000, type: 'error'});
            })
            .always(function () {
                $("#invite-overlay input[type='text']").val("");
                $('#invite-overlay .tag-add-button').click(submitEmailOverlay);
                $('html').removeClass('wait-mouse-cursor');
            });
    }else{
        $('#invite-overlay .tag-add-button').click(submitEmailOverlay);
    }
}
function validateEmail(email){
    if(email == null || email == "" ){
        noty({text: 'You need to enter a email!',timeout: 2000, type: 'error'});
        return false;
    }
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        noty({text: 'You need to enter a valid email.',timeout: 2000, type: 'error'});
        return false;
    }
    else{
        return true;
    }
}
function cancelEmailOverlay() {
    $("#invite-overlay").fadeOut();
    $("#overlay").fadeOut();
}

function disabledInvite(){
    noty({text: 'Public inviting has been disabled by the admin', timeout: 2000, type: 'error'});
}

function initHelpOverlay() {
    $('#help-overlay').fadeIn();
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
}

function cancelHelpOverlay() {
    $("#help-overlay").fadeOut();
    $("#overlay").fadeOut();
}

function openHelpSubject(){
    $('#help-overlay input[type="button"]').fadeOut(0);
    $('#help-overlay .icon').fadeOut(0);
    $('#help-overlay .titular').text($(this).val().toUpperCase());
    var subjectName = $(this).val();

    $.getJSON("/js/help.json", function(json) {

        console.log(json[subjectName]);
        $("#help-text").html(json[subjectName]);

    });
    $('#help-overlay #help-text').fadeIn(0);
    $('#help-overlay .tag-add-button').fadeIn(0).css("display","inline-block");
}

function backToMainHelp(){
    $('#help-overlay input[type="button"]').fadeIn(0);
    $('#help-overlay .icon').fadeIn(0);
    $('#help-overlay .titular').text("HELP");
    $('#help-overlay #help-text').fadeOut(0);
    $('#help-overlay .tag-add-button').fadeOut(0);
}

function handleNotificationVal(value){

    if(value == "Browser notifications enabled"){
        notifcationEnabled();
    } else if(value == "Browser notifications disabled"){
        notifcationDisabled();
    } else {
        console.log("handleNotificationVal not getting right value");
    }

}

function notifcationDisabled() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        try {
            localStorage.setItem('notification', false);
        } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
                alert('Quota exceeded!');
            }
        }
    } else {
        localStorageLimitExceeded();
    }
}

function notifcationEnabled() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        try {
            localStorage.setItem('notification', true);
        } catch (e) {
            console.log("localstorage error in notification enabled");
        }
    } else {
        localStorageLimitExceeded();
    }
}

function localStorageLimitExceeded(){
    noty({text: 'Cannot store user preferences as your browser do not support local storage', timeout: 2000, type: 'error'});
}

function getTaskTime() {


    var notificationValue = localStorage.getItem('Task time');
    if(notificationValue == null || notificationValue == undefined){
        var setDate = new Date();
        setDate = setDate.toISOString();
        localStorage.setItem("Task time", setDate);
        notificationValue = setDate;
    }

    return notificationValue;
}

function taskTimemanager() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        try {
            var lsDate = new Date();
//            date = new Date(date.setSeconds(date.getSeconds()+15));
            lsDate = lsDate.toISOString();
            localStorage.setItem("Task time", lsDate);
        } catch (e) {
            console.log("localstorage error in sublink");
        }

    } else {
        localStorageLimitExceeded();
    }
}


function localNoficationSettings(){

    if (localStorage.length != 0) {
        notificationValue = localStorage.getItem('notification');
    } else {
        notificationValue = false;
    }

    return notificationValue;

}

function notifcationEnabled() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        try {
            localStorage.setItem('notification', true);
        } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
                localStorageLimitExceeded();
            }
        }
    } else {
        localStorageLimitExceeded();
    }
}