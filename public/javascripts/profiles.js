/**
 * Created by Jason on 14-08-14.
 */
var sessionUser;
var originalFileName;

$(document).ready(function () {
    $('.profile-page .middle-container .add-button span').tipsy({offset: 5});
    $('.profile-page .right-container-single .change-status span').tipsy({offset: 5});
    $('.profile-page .right-container-single .change-status span').click(switchStatus);
    $('.profile-page .fontawesome-info-sign').click(initDescriptionOverlay);
    $('#overlay, #task-information-overlay a').click(hideDescriptionOverlay);
    $('.profile-page .add-button .fontawesome-edit').click(initEditProfile);
    $('.profile-page .add-button .fontawesome-cogs').click(initPromoteToAdmin);
    $('.profile-page .add-button .fontawesome-remove').click(initDeleteUser);
    $('#add-file-overlay input[type="text"]').click(initUploadFile);  // this is the textfield in the popup. when you click it it will init the choose file event
    $('#add-file-overlay input[type=file]').change(showFileName);   // this shows the file path+name of the file you chose
    //$('.input-icon-attach').click(initUploadOverlay); // this is the button that will open the overlay for attaching file
    $('#add-file-overlay .tag-add-button').click(saveUploadOverlay); // this button will save the file and close the window
    $('#add-file-overlay .tag-cancel-button').click(cancelUploadOverlay); //this button will cancel and not save the file
    $('#taslDetailsLi').autosize();
    addActiveLink();
    setStatus();
    setSessionUser();
    loadUserTasks();
});
function addActiveLink() {
    $('.main-profiles-link').addClass('activelink');
}

function setSessionUser(){
    console.log("we set session here? why?")
    // $.post( "/accounts/setSessionUser",
    //     function(data) {
    //         if(data){
    //             sessionUser=data;
    //         }
    //     });
}

function setStatus(){
    if($('.status-text').text() == "ONLINE"){
        $('.user-status-icon').addClass("online");
        $('.left-container .titular').addClass("online");
        $('.add-button').addClass("online");
        $('.likes').addClass("online");
        $('.change-status').addClass("online");
        $('.working-until-text').fadeIn(0);
        $('.last-online-text').fadeOut(0);
    }
}

function swapStatus(){
    if($('.status-text').text() == "OFFLINE"){
        $('.user-status-icon').addClass("online");
        $('.left-container .titular').addClass("online");
        $('.add-button').addClass("online");
        $('.likes').addClass("online");
        $('.change-status').addClass("online");
        $('.status-text').text("ONLINE");
        $('.last-online-text').fadeOut(0);
        $('.working-until-text').fadeIn(0);
    }else{
        $('.status-text').text("OFFLINE");
        $('.likes').removeClass("online");
        $('.user-status-icon').removeClass("online");
        $('.add-button').removeClass("online");
        $('.change-status').removeClass("online");
        $('.left-container .titular').removeClass("online");
        $('.last-online-text').fadeIn(0);
        $('.working-until-text').fadeOut(0);
    }
}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    if(text == null || text == ""){
        return text
    }
    else{
        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }

}

function initDescriptionOverlay() {
    var taskDesc = $(this).parent().find('.taskDesc').text();
    var taskCreator = $(this).parent().find('.taskCreator').val();
    var taskCreated = $(this).parent().find('.taskCreated').val();
    var taskExtra = $(this).parent().find('.taskExtra').val();
    $('#taskDescriptionLi').text("Task: "+escapeHtml(taskDesc));
    $('#taskCreatorLi ').text("Creator: "+escapeHtml(taskCreator));
    $('#taskCreatedLi ').text("Created date: "+escapeHtml(taskCreated));
    $('#taskDetailsLi ').text("Details: "+escapeHtml(taskExtra));
    $('#task-information-overlay').fadeIn();
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
}
function hideDescriptionOverlay() {
    $("#task-information-overlay").fadeOut();
    $("#overlay").fadeOut();
}
function loadUserTasks() {
    $('html').addClass('wait-mouse-cursor');
    var user = $("#email").text();
    var addURL = "/getUserTasks?user=" + user;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: addURL
    }).done(function (result) {
        renderTasksHtml(result)
    }).fail(function (jqXHR, textStatus, errorThrown) {
        noty({text: 'Error loading tasks in progress!', timeout: 2000, type: 'error'});
    }).always(function () {
        $('html').removeClass('wait-mouse-cursor');
    });
}
function formatDate(date) {
    var dateObj = new Date(date);
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return dateObj.getDate() + " " + monthNames[dateObj.getMonth()] + " " + dateObj.getFullYear();
}
function renderTasksHtml(tasks) {
    for (var i = 0; i < tasks.length; i++) {
        var taskDesc = tasks[i].desc;
        var taskExtra = tasks[i].extra;
        var taskCreator = tasks[i].creator;
        var taskCreated = formatDate(tasks[i].created.$date);
        var taskAssigneeImg = tasks[i].assigneeImg;
        var taskAssignee = "";
        if (tasks[i].assignee) {
            taskAssignee = tasks[i].assignee;
        }
        prependNewTask(taskDesc, taskExtra, taskAssignee, taskCreator, taskCreated, taskAssigneeImg)
    }
    $('.fontawesome-info-sign').unbind('click').click(initDescriptionOverlay);
}

function prependNewTask(taskDesc, taskExtra, taskAssignee, taskCreator, taskCreated, taskAssigneeImg) {
    var content = '' +
        '<li class="new-tasks">' +
        '<img src=' + "'" + escapeHtml(taskAssigneeImg) + "'" + 'alt="pic" style="float:left" /> ' +
        '<span class="taskDesc" style="float:left; font-size:14px;color:#999">' + escapeHtml(taskDesc) + '</span>' +
        '<input class="taskExtra" type="hidden" value=' + "'" + escapeHtml(taskExtra) + "'" + '>' +
        '<input class="taskCreator" type="hidden" value=' + "'" + escapeHtml(taskCreator) + "'" + '>' +
        '<input class="taskCreated" type="hidden" value=' + "'" + escapeHtml(taskCreated) + "'" + '>' +
        '<input class="taskAssignee" type="hidden" value=' + "'" + escapeHtml(taskAssignee) + "'" + '>' +
        '<span original-title="View / Edit Details" class="fontawesome-info-sign scnd-font-color"></span>' +
        '</li>';

    $('.right-container-single ul.menu-box-menu').prepend(content)
}

function initEditProfile(){
    $('.profile-page .add-button .fontawesome-edit').one("click", cancelEditProfile);
    $('.left-container li .hidden-field').show();
    $('.labelclass-profile').show();
    $('.labelclass').show();
    $('.user-name').editable({
        type: 'text',
        send: 'always',
        url: '/editprofile',
        validate: function (value) {
            if ($.trim(value) == '') {
                return "The field cannot be empty";
            }
            if (!value.match(/^(?:\b\S{1,15}\b\s*)+$/)) {
                return "Name is too long";
            }
            if (value.length > 30) {
                return "Maximum of 30 characters!";
            }
        },
        error: function (errors) {
            alert(errors);
            var msg = '';
            if (errors && errors.responseText) { //ajax error, errors = xhr object
                msg = errors.responseText;
            } else { //validation error (client-side or server-side)
                $.each(errors, function (k, v) {
                    msg += k + ": " + v + "<br>";
                });
            }
        },
        success: function (response, newValue) {
            var type = $(this).attr('id');
            if (response != "success") {
                noty({text: 'Problem updating ' + type, timeout: 2000, type: 'error'});
                var oldValue = $('#' + type).text();
                $('#' + type).text(oldValue);
            } else {
                noty({text: type + ' successfully updated', timeout: 2000, type: 'success'});
            }
        }
    }).animate({"borderWidth": 5}, 500).animate({"borderWidth": 1}, 500);

    $('.working-until-text').editable({
        type: 'text',
        send: 'always',
        url: '/editprofile',
        validate: function (value) {
            if (value.length > 200) {
                return "there is a maximum of 200 characters!";
            }
            if (!value.match(/^(?:\b\S{1,20}\b\s*)+$/)) {
                return "No single word can be longer than 20 characters"
            }
        },
        success: function (response, newValue) {
            var type = $(this).attr('id');
            if (response != "success") {
                noty({text: 'Problem updating ' + type, timeout: 2000, type: 'error'});
                var oldValue = $('#' + type).text();
                $('#' + type).text(oldValue);
            } else {
                noty({text: type + ' successfully updated', timeout: 2000, type: 'success'});
            }
        }
    }).animate({"borderWidth": 5}, 500).animate({"borderWidth": 1}, 500);

           //$('.profile-description p:first-child').editable({

    $('.profile-description p:nth-child(2)').editable({
        type: 'text',
        send: 'always',
        url: '/editprofile',
        pk: sessionUser,
        validate: function (value) {
            if (value.length > 400) {
                return "there is a maximum of 400 characters!";
            }
        },
        success: function (response, newValue) {
            var type = $(this).attr('id');
            if (response != "success") {
                noty({text: 'Problem updating ' + type, timeout: 2000, type: 'error'});
                var oldValue = $('#' + type).text();
                $('#' + type).text(oldValue);
            } else {
                noty({text: type + ' successfully updated', timeout: 2000, type: 'success'});
            }
        }
    }).animate({"borderWidth": 5}, 500).animate({"borderWidth": 1}, 500);

    $('.left-container li .text-about span').editable({
        type: 'text',
        send: 'always',
        url: '/editprofile',
        validate: function (value) {
            var type = $(this).attr('id');
            if ($.trim(value) == '') {
                return "The field cannot be empty";
            }
            if (value.length > 150) {
                return "there is a maximum of 150 characters!";
            }
            if (type == "email") {
                if (value.length > 60) {
                    return "there is a maximum of 60 characters!";
                }
                if (!value.match(/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/)) {
                    return "this is not a valid email"
                }
            }
            if (type == "phonenumber") {
                if (!value.match(/^((\+)?)([\-\s.\(\)]*\d{1}){8,12}$/)) {
                    return "this is not a valid phonenumber";
                }
            }
        },
        success: function (response, newValue) {
            var type = $(this).attr('id');
            if (response != "success") {
                noty({text: 'Problem updating ' + type, timeout: 2000, type: 'error'});
                var oldValue = $('#' + type).text();
                $('#' + type).text(oldValue);
            } else {
                noty({text: type + ' successfully updated', timeout: 2000, type: 'success'});
            }
        }
    }).animate({"borderWidth": 5}, 500).animate({"borderWidth": 1}, 500);

    $('.left-container li .text-about-password span').editable({
        type: 'address',
        send: 'always',
        emptytext: 'Change password',
        url: '/editpassword',
        value: {
            oldPassword: "",
            newPassword: "",
            newPasswordCheck: ""
        },
        validate: function (value) {
            if (!value.oldPassword.match(/^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%]{6,20}$/)) {
                return "passwords must have one letter, one number and be between 6-20 characters";
            }
            if (!value.newPassword.match(/^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%]{6,20}$/)) {
                return "passwords must have one letter, one number and be between 6-20 characters";
            }
            if (!value.newPasswordCheck.match(/^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%]{6,20}$/)) {
                return "passwords must have one letter, one number and be between 6-20 characters";
            }
            if (value.newPassword != value.newPasswordCheck) {
                return "the new password and the password check doesnt match"
            }
        },
        display: function (value) {
            $(this).empty();
        },
        success: function (response, newValue) {
            var type = $(this).attr('id');
            if (response != "success") {
                noty({text: 'Problem updating ' + type, timeout: 2000, type: 'error'});
                var oldValue = $('#' + type).text();
                $('#' + type).text(oldValue);
            } else {
                noty({text: type + ' successfully updated', timeout: 2000, type: 'success'});
            }
        }
    }).animate({"borderWidth": 5}, 500).animate({"borderWidth": 1}, 500);


           ///////////////////////////////////////////////////////////////////

           $('#profilePicture').addClass('dashed-blue-border').click(initUploadOverlay).animate({"borderWidth": 8},500).animate({"borderWidth": 5},500);

           ///////////////////////////////////////////////////////////////////


}


//ADDING TRIGGER TO MAKE CLICKING INPUT BOX TO MAKE IT TRIGGER FILE UPLOADING
function initUploadFile() {
    $('#add-file-overlay input[type=file]').trigger('click');
}

//FUNCTION TO SHOW THE CORRECT FILE NAME IN THE INPUT BOX IN THE UPLOAD FILE OVERLAY
function showFileName() {
    $('#add-file-overlay .text-input').val($(this).val());
    originalFileName = $(this).val();
}

//INITIALIZING THE UPLOAD FILE OVERLAY
function initUploadOverlay() {
    var docHeight = $(document).height();
    $("#overlay").height(docHeight).fadeIn();
    $("#add-file-overlay").fadeIn();
}

//CANCEL THE UPLOAD FILE OVERLAY
function cancelUploadOverlay() {
    $('#add-file-overlay input[type=file]').val("");
    $("#add-file-overlay").fadeOut(function () {
        $('#add-file-overlay .text-input').val("");
    });
    $("#overlay").fadeOut();
}

//SAVE THE FILE SO IT IS READY TO BE ADDED WHEN A NEW POST IS CREATED
function saveUploadOverlay() {
    $('html').addClass('wait-mouse-cursor');
    var loc = window.location;
    var username = loc.pathname.substring(loc.pathname.lastIndexOf('/')+1);
    var file = $('#add-file-overlay #my_file')[0].files[0];
    // BECAUSE A FILE IS ADDED IT WORKED BEST WITH SENT AS FORMDATA
    var data = new FormData();
    data.append("data[pk]", sessionUser);
    data.append("data[name]", "profilePicture");
    data.append("data[value]", file);

    //TODO: MAKE THIS WORK WITH SCALA PLAY
    // $.ajax({
    //     type: "POST",
    //     url: "/profiles/"+username+"/edit",
    //     cache: false,
    //     //dataType: "json",
    //     processData: false, // Don't process the files
    //     contentType: false, // Set content type to false as jQuery will tell the server its a query string request
    //     data: data
    // })
    // .done(function (result) {
    //     var responseData = JSON.parse(result);
    //     if(responseData['Response']['status']=="success"){
    //         $("#profilePicture img").attr("src",responseData['Response']['content']+"?modified");
    //         noty({text: responseData['Response']['response'], timeout: 2000, type: 'success'});
    //     }
    //         else{
    //         noty({text: responseData['Response']['response'], timeout: 2000, type: 'error'});
    //     }
    //         $('html').removeClass('wait-mouse-cursor');
    // })
    // .fail(function (jqXHR, textStatus, errorThrown) {
    //         $('html').removeClass('wait-mouse-cursor');
    //     noty({text: 'Error uploading image!', timeout: 2000, type: 'error'});
    // });
    $("#add-file-overlay").fadeOut();
    $("#overlay").fadeOut();
}


function resetValue(field,oldValue){
    // we need this function because it is not possible
    // to prevent x-editable from saving if the ajax is a success.
    // even though the ajax returns a backend validation error.
    setTimeout(
        function()
        {
            $('#'+field).text(oldValue);
        }, 1500);

}

function cancelEditProfile(){
    $('.profile-page .add-button .fontawesome-edit').finish().one("click", initEditProfile);
    $('.left-container li .text-about span').finish().editable('destroy');
    $('.left-container li .text-about-password span').finish().editable('destroy');
    $('.left-container li .hidden-field').finish().hide();
    //$('.profile-description p:first-child').finish().editable('destroy');
    $('.profile-description p:nth-child(2)').finish().editable('destroy');
    $('.working-until-text').finish().editable('destroy');
    $('.user-name').finish().editable('destroy');
    $('#profilePicture').finish().removeClass('dashed-blue-border').addClass('big-profile-picture').unbind('click');
    $('.labelclass-profile').hide();
    $('.labelclass').hide();
}

function initPromoteToAdmin(){
    noty({
        text: 'Do you wish to promote this user to admin?',
        buttons: [
            {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                //TODO: MAKE THIS WORK WITH SCALA PLAY
                var user = $('#email').text();
                $.post("/promoteadmin?username=" + user,
                    function (data) {
                        if (data) {
                            if (data == "success") {
                                $('.profile-page .add-button .fontawesome-cogs').fadeOut();
                                noty({text: user + ' successfully promoted to admin', timeout: 2000, type: 'success'});
                            } else {
                                noty({text: 'Error promoting user!', timeout: 2000, type: 'error'});
                            }
                        } else {
                            noty({text: 'Error promoting user!', timeout: 2000, type: 'error'});
                        }
                    }
                );
                $noty.close();
            }
            },
            {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                $noty.close();
            }
            }
        ]
    });
}
function initDeleteUser(){
    noty({
        text: 'Do you want to delete this user?',
        buttons: [
            {addClass: 'btn btn-primary', text: 'Yes', onClick: function($noty) {
                $noty.close();
                var username = $('#email').text();
                //TODO: MAKE THIS WORK WITH SCALA PLAY

                $.post("/deleteuser?username=" + username,
                    function (data) {

                    }
                );

            }
            },
            {addClass: 'btn btn-danger', text: 'No', onClick: function($noty) {
                $noty.close();
            }
            }
        ]
    });
}

function switchStatus(){
    $.post("/toggleonlinestatus",
        function (data) {
            if (data) {
                if (data == "success") {
                    swapStatus();
                    noty({text: 'Your status is now swapped', timeout: 2000, type: 'success'});
                } else {
                    noty({text: 'Error swapping your status!', timeout: 2000, type: 'error'});
                }
            } else {
                noty({text: 'Error switching your status!', timeout: 2000, type: 'error'});
            }
        }
    );

}