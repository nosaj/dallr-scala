package models
import org.joda.time.DateTime
import play.api.libs.json.Json

/**
  * Created by jason on 9/30/16.
  */
//TODO: DO WE NEED THE SESSION?
case class User(
                 firstname: String,
                 lastname: String,
                 jobtitle: String,
                 username: String,
                 phone: Int,
                 img: String,
                 admin: Boolean,
                 isOnline: Boolean,
                 showOnline: Boolean,
                 lastOnline: DateTime,
                 onlineUntil:String,
                 password:String,
                 sQuestion:Int,
                 sAnswer:String,
                 rBlock:DateTime,
                 rAttempts:Int,
                 created:DateTime,
                 modified:DateTime,
                 session_id:String
               )

object User {
  implicit val formatter = Json.format[User]
}
