/**
  * Created by jason on 9/29/16.
  */

  package controllers
  import javax.inject.Inject

  import org.joda.time.DateTime
  import org.mindrot.jbcrypt.BCrypt
  import play.api.libs.json.{JsNumber, _}
  import play.modules.reactivemongo.json._
  import play.api.mvc._
  import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
  import reactivemongo.api.ReadPreference
  import reactivemongo.play.json.collection.JSONCollection

  import scala.concurrent.{ExecutionContext, Future}
  import scala.util.{Failure, Random, Success}
  import play.api.libs.mailer._
  import java.io.File

  import org.apache.commons.mail.EmailAttachment


  class AccountController @Inject()(mailerClient: MailerClient)(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext) extends Controller with MongoController with ReactiveMongoComponents {

    def showPage = Action { request =>
      if (request.session.get("connected").isEmpty) {
        val content = views.html.Account.login()
        Ok(content)
      } else {
        Redirect(routes.PostController.showPage())
      }
    }

    def validateCompany(companyName: String, password: String): Future[Option[JsObject]] = {
      database.map(_.collection[JSONCollection]("companies")).flatMap {
        companies =>
          // find all users with username `username` $options i means it is case insensitive
          companies.find(Json.obj("password" -> BCrypt.hashpw(password, BCrypt.gensalt(12)), "name" -> Json.obj("$regex" -> companyName, "$options" -> "i"))).
            // perform the query and get a cursor of JsObject then return the head
            cursor[JsObject](ReadPreference.primary).headOption
      }
    }

    def signupCompany = Action.async {
      //Return and do not complete the signup if the user already exists.
      response => {
        val companyName = response.body.asFormUrlEncoded.get("companyName").head
        val companyPassword = response.body.asFormUrlEncoded.get("companyPassword").head
        val username = response.body.asFormUrlEncoded.get("username").head
        val password = response.body.asFormUrlEncoded.get("password").head
        val sQuestion = response.body.asFormUrlEncoded.get("sQuestion").head
        val sAnswer = response.body.asFormUrlEncoded.get("sAnswer").head

        val jsonCompany = Json.obj(
          "name" -> companyName,
          "password" -> BCrypt.hashpw(companyPassword, BCrypt.gensalt(12)),
          "created" -> DateTime.now(),
          "users" -> Json.arr(
            Json.obj(
              "username" -> username
            )
          )
        )

        val jsonUser = Json.obj(
          "firstname" -> "Firstname",
          "lastname" -> "Lastname",
          "jobtitle" -> "Jobtitle",
          "username" -> username,
          "companyName" -> companyName,
          "phone" -> "Phone Number",
          "image" -> "/assets/images/noImage.gif",
          "admin" -> false,
          "isOnline" -> true,
          "showOnline" -> true,
          "lastOnline" -> Json.obj("$date" -> DateTime.now().getMillis),
          "onlineUntil" -> "Online Until?",
          "password" -> BCrypt.hashpw(password, BCrypt.gensalt(12)),
          "sQuestion" -> sQuestion,
          "sAnswer" -> sAnswer,
          "created" -> Json.obj("$date" -> DateTime.now().getMillis),
          "modified" -> Json.obj("$date" -> DateTime.now().getMillis),
          "statusMessage" -> Json.obj(
            "content" -> "Hello fellow colleagues",
            "created" -> Json.obj("$date" -> DateTime.now().getMillis)
          )
        )
        val jsonSettings = Json.obj(
          "invites" -> false
        )
        for {
          findUser <- userExists(username)
          findCompany <- companyExists(companyName)
          users <- usersFuture
          companies <- companiesFuture
          settings <- settingsFuture(companyName)
        } yield {
          //TODO: CHECK RESEULTS OF INSERT BEFORE RETURNING SUCCESS OR FAILURE (By making individual creates)
          //TODO: GIVE MORE SPECIFIC ERROR MESSAGES, eg. User already exists.
          if (findUser.isEmpty && findCompany.isEmpty) {
            users.insert(jsonUser)
            companies.insert(jsonCompany)
            settings.insert(jsonSettings)
            Ok("success")
          } else {
            Ok("failure")
          }
        }
    }
    }

    //TODO: ADD SANATIZING IN THE MODEL

    def settingsFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("settings_" + companyName))

    def joinCompany = Action.async {
      response => {
        val companyName = response.body.asFormUrlEncoded.get("companyName").head
        val companyPassword = response.body.asFormUrlEncoded.get("companyPassword").head
        val username = response.body.asFormUrlEncoded.get("username").head
        val password = response.body.asFormUrlEncoded.get("password").head
        val sQuestion = response.body.asFormUrlEncoded.get("sQuestion").head
        val sAnswer = response.body.asFormUrlEncoded.get("sAnswer").head

        val jsonUser = Json.obj(
          "firstname" -> "Firstname",
          "lastname" -> "Lastname",
          "jobtitle" -> "Jobtitle",
          "username" -> username,
          "companyName" -> companyName,
          "phone" -> "Phone Number",
          "image" -> "/assets/images/noImage.gif",
          "admin" -> false,
          "isOnline" -> true,
          "showOnline" -> true,
          "lastOnline" -> Json.obj("$date" -> DateTime.now().getMillis),
          "onlineUntil" -> "Online Until?",
          "password" -> BCrypt.hashpw(password, BCrypt.gensalt(12)),
          "sQuestion" -> sQuestion,
          "sAnswer" -> sAnswer,
          "created" -> Json.obj("$date" -> DateTime.now().getMillis),
          "modified" -> Json.obj("$date" -> DateTime.now().getMillis),
          "statusMessage" -> Json.obj(
            "content" -> "Hello fellow colleagues",
            "created" -> Json.obj("$date" -> DateTime.now().getMillis)
          )
        )

        val selector = Json.obj("name" -> companyName, "password" -> companyPassword)
        val modifier = Json.obj(
          "$push" -> Json.obj(
            "users" -> Json.obj(
              "username" -> username
            )
          )
        )
        for {
          findUser <- userExists(username)
          company <- companyExists(companyName)
          users <- usersFuture
          invites <- invitesFuture(companyName)
          companies <- companiesFuture
        } yield {
          if (company.nonEmpty && findUser.isEmpty) {
            users.insert(jsonUser)
            invites.update(Json.obj("invitedEmail" -> username), Json.obj("$set" -> Json.obj("userJoined" -> true, "modified" -> Json.obj("$date" -> DateTime.now().getMillis))))
            companies.update(selector, modifier)
            Ok("success")
          } else {
            Ok("failure")
          }
        }
      }
    }

    //Checking if the user exists before signup
    def userExists(username: String): Future[Option[JsObject]] = {
      usersFuture.flatMap { users =>
        // find all users with username `username` $options i means it is case insensitive
        users.find(Json.obj("username" -> Json.obj("$regex" -> username, "$options" -> "i"))).
          // perform the query and get a cursor of JsObject then return the head
          cursor[JsObject](ReadPreference.primary).headOption
      }
    }

    def invitesFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("invites_" + companyName))

    //Checking if the company exists before signup
    def companyExists(companyName: String): Future[Option[JsObject]] = {
      companiesFuture.flatMap { companies =>
        // find all people with name `name` $options i means it is case insensitive
        companies.find(Json.obj("name" -> companyName)).
          // perform the query and get a cursor of JsObject then return the head
          cursor[JsObject](ReadPreference.primary).headOption
      }
    }

    def companiesFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("companies"))

    def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

    def login() = Action.async {
      response => {
        val username = response.body.asFormUrlEncoded.get("username").head
        val password = response.body.asFormUrlEncoded.get("password").head

        val futureUser: Future[Option[JsObject]] = usersFuture.flatMap {
          // find all users with username `username`
          _.find(Json.obj("username" -> username)).
            // perform the query and get a cursor of JsObject then return the head
            cursor[JsObject](ReadPreference.primary).headOption
        }

        futureUser onFailure {
          case t => BadRequest
        }
        futureUser.map {
          case Some(jsonObj) => {
            val passwordString = jsonObj.\("password").as[String]
            val company = jsonObj.\("companyName").as[String]
            val admin = jsonObj.\("admin").get.toString()
            if (BCrypt.checkpw(password, passwordString)) Ok("correct").withSession("connected" -> username, "companyName" -> company, "admin" -> admin)
            else BadRequest
          }
          case None => BadRequest
        }
      }
    }

    def logout = Action {
      Redirect(routes.AccountController.showPage()).withNewSession
    }

    def resetPassword(email: String, sQuestion: Int, sAnswer: String) = Action.async {
      //Return and do not complete the signup if the user already exists.
      for {
        users <- usersFuture
        selector = Json.obj("username" -> email, "sQuestion" -> sQuestion, "sAnswer" -> sAnswer)
        newPasswordString = Random.alphanumeric.take(10).mkString
        modifier = Json.obj("$set" -> Json.obj("password" -> BCrypt.hashpw(newPasswordString, BCrypt.gensalt(12)), "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        updatePasswordResult <- users.update(selector, modifier)
      } yield {
        if (updatePasswordResult.n != 0 && updatePasswordResult.ok) {
          val message = "Hello <b>" + email + " </b> </br> On your request we have now reset you password </br> Your new password is:<b> " + newPasswordString + "</b></br></br> To complete the reset you have to set a new password once you have logged in</br> For security purposes it is very important to change your password as soon as possible."
          //SEND EMAIL
          sendEmail(email, "Reset Password", message)
          //          TODO: CHECK EMAIL WAS DELIVERED BEFORE RETURNING SUCCESS
          Ok("success")
        } else {
          Ok("failure")
        }
      }
    }

    def sendEmail(to: String, header: String, message: String) {
      val emailMessage = "\n\n\n\n<html>\n<head>\n    <style>\n\n        body {\n            background: none repeat scroll 0 0 #1f253d;\n        }\n        .main-container {\n            font-family: \"Ubuntu\",sans-serif;\n            margin: 3em auto;\n            width: 950px;\n        }\n        #inner-container{\n            width:450px;\n            background: none repeat scroll 0 0 #394264;\n            border-radius: 5px;\n            left:50%;\n            margin-left:-237px;\n            padding:25px;\n            position: absolute;\n        }\n        #logo{\n            width:150px;\n            height:70px;\n            margin: 0 auto;\n            position:absolute;\n            left:50%;\n            margin-left:-75px;\n        }\n        h2{\n            margin-top:70px;\n            margin-bottom:0;\n            border-top-left-radius: 5px;\n            border-top-right-radius: 5px;\n            display: block;\n            line-height: 60px;\n            text-align: center;\n            color: #fff;\n        }\n        p{\n            color: #ccc;\n            font-size: 12pt;\n            margin-right: 0.25em;\n            vertical-align: -0.4em;\n            font-family: \"Ubuntu\",sans-serif;\n        }\n        p.first{\n            margin-top:0;}\n        strong{\n            font-size:32pt;\n            font-weight:normal;\n            color:#fff;\n        }\n        a{\n            text-align:center;\n            color: #fff;\n            display:block;\n        }\n\n    </style>\n</head>\n<body>\n<div class=\"main-container\">\n    <div id=\"inner-container\">\n        <img id=\"logo\" alt=\"dallr\" src=\"https://s3-eu-west-1.amazonaws.com/dallr/dallr-logo2.png\">\n        <h2>" + header + "</h2>\n\n  <p>" + message + "</p></div>\n</div>\n</body>\n</html>"
      val email = Email(
        "dallr - " + header,
        "dallr.com <mail.dallr@gmail.com>",
        Seq(to),
        bodyHtml = Some(emailMessage)
      )
      mailerClient.send(email)
    }

    def inviteUser(email: String) = Action.async { request =>
      //Return and do not complete the signup if the user already exists.
      for {
        users <- usersFuture
        sessionUser = request.session.get("connected").get
        companyName = request.session.get("companyName").get
        invites <- invitesFuture(companyName)
        futureSettings <- settingsFuture(companyName)
        jsonInvite = Json.obj(
          "userJoined" -> 0,
          "userEmail" -> sessionUser,
          "invitedEmail" -> email,
          "created" -> Json.obj("$date" -> DateTime.now().getMillis),
          "modified" -> Json.obj("$date" -> DateTime.now().getMillis)
        )
        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
        publicInvites = settings.head.\("invites").as[Boolean]
        user <- users.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        userAdmin <- users.find(Json.obj("admin" -> true)).cursor[JsObject](ReadPreference.primary).headOption
        admin = userAdmin.head.\("username").as[String]
      } yield {
        if (publicInvites && user.isDefined) {
          invites.insert(jsonInvite)
          val message = "dallr is a state of the art communication platform designed for internal communication within workplaces.</b></br></br>" + sessionUser + " has invited you to join the company " + companyName + " on dallr.</b></br></br> Our invite system is currently under construction so please manually contact the admin: " + admin + " to receive the company password. <br><br><br> <a href='http://dallr.com/login'>Visit dallr.com to signup</a>"
          //SEND EMAIL
          sendEmail(email, "Join your colleagues", message)
          //TODO: CHECK EMAIL WAS DELIVERED & INSERT WORKED BEFORE RETURNING SUCCESS
          Ok("success")
        } else {
          Ok("failure")
        }
      }
    }
  }
