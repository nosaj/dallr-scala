package controllers

import javax.inject._

import actors.{ChatRoom, UserSocket}
import akka.actor._
import akka.stream.Materializer
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.JsValue
import play.api.libs.streams.ActorFlow
import play.api.mvc.{Action, Controller, WebSocket}
import scala.concurrent.{Future}

@Singleton
class ChatController @Inject()(val messagesApi: MessagesApi, system: ActorSystem, mat: Materializer) extends Controller with I18nSupport {
  val User = "user"

  implicit val implicitMaterializer: Materializer = mat
  implicit val implicitActorSystem: ActorSystem = system

  val chatRoom = system.actorOf(Props[ChatRoom], "chat-room")

  val nickForm = Form(single("nickname" -> nonEmptyText))

  def showPage = Action { implicit request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Redirect(routes.AccountController.showPage()).withNewSession
    } else {
      //      for {
      //        futureSettings <- settingsFuture(request.session.get("companyName").get)
      //        sessionUser = request.session.get("connected").get
      //        isAdmin = request.session.get("admin").get
      //        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
      //        publicInvites = settings.head.\("invites").as[Boolean]
      //      } yield {
      val content = views.html.Message.message(request.session.get("connected").get, request.session.get("admin").get, true)
      Ok(content)
    }
  }

  def index = Action { implicit request =>
    request.session.get(User).map { user =>
      Redirect(routes.ChatController.showPage()).flashing("info" -> s"Redirected to chat as $user user")
    }.getOrElse(Ok(views.html.Message.index(nickForm)))
  }

  def nickname = Action { implicit request =>
    nickForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.Message.index(formWithErrors))
      },
      nickname => {
        Redirect(routes.ChatController.showPage())
          .withSession(request.session + (User -> nickname))
      }
    )
  }

  def leave = Action { implicit request =>
    Redirect(routes.ChatController.index()).withNewSession.flashing("success" -> "See you soon!")
  }

  //  def chat = Action { implicit request =>
  //    request.session.get("connected").map { user =>
  //      Ok(views.html.Message.chat(user))
  //    }.getOrElse(Redirect(routes.ChatController.index()))
  //  }

  def socket = WebSocket.acceptOrResult[JsValue, JsValue] { request =>
    Future.successful(request.session.get("connected") match {
      case None => Left(Forbidden)
      case Some(uid) =>
        Right(ActorFlow.actorRef(UserSocket.props(uid)))
    })
  }
}
