/**
  * Created by jason on 9/28/16.
  */
package controllers
import play.api.mvc._
import play.libs.mailer.MailerClient
import javax.inject.Inject

import play.api.libs.mailer.Email

class HomeController @Inject() (mailerClient: MailerClient) extends Controller {

  implicit val myCustomCharset = Codec.javaSupported("iso-8859-1")
  val content = views.html.Home.index()
  def index = Action {
    Ok(content)
  }

  //TODO: MAKE THE MAILER PLUGIN WORK
//  def sendEmail {
//    val email = Email(
//      subject = "subject",
//      from = "pari.XXXXX@gmail.com",
//      to = List("pari.XXXXX@gmail.com"),
//      bodyHtml = Some("bodyHTML"),
//      bodyText = Some("Hello"),
//      replyTo = None
//    )
//    mailerClient.send(email)
//  }
}
