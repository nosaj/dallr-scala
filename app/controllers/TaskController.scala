package controllers

import javax.inject.Inject

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.stream.Materializer
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import play.api.libs.json.{JsObject, Json}
import play.api.libs.streams.ActorFlow
import play.api.mvc.{Action, Controller, WebSocket}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.ReadPreference
import reactivemongo.play.json.collection.JSONCollection
import play.modules.reactivemongo.json._
import reactivemongo.bson.BSONObjectID

import scala.concurrent.{ExecutionContext, Future}

class TaskController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext, system: ActorSystem, materializer: Materializer) extends Controller with MongoController with ReactiveMongoComponents {
  def showPage = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureSettings <- settingsFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected").get
        isAdmin = request.session.get("admin").get
        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
        publicInvites = settings.head.\("invites").as[Boolean]
      } yield {
        val content = views.html.Task.task(sessionUser, isAdmin, publicInvites)
        Ok(content)
      }
      }
    }

  def settingsFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("settings_" + companyName))

  def loadTasks = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      for {
        tasksFut <- tasksFuture(request.session.get("companyName").get)
        companyName = request.session.get("companyName")
        taskscursor <- tasksFut.find(Json.obj()).sort(Json.obj("modified" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
        tasks = taskscursor
      } yield {
        Ok(Json.toJson(tasks))
      }
    }
  }

  def insertTask(description: String, assignee: String, extra: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val id = BSONObjectID.generate()
      val jsonId = Json.obj("$oid" -> id.stringify)
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        companyName = request.session.get("companyName").get
        user <- futureUser.find(Json.obj("username" -> assignee)).cursor[JsObject](ReadPreference.primary).headOption
        tasks <- tasksFuture(companyName)
      } yield {
        val assigneeOrEmpty = if (user.isDefined) sessionUser.get else ""
        val image = if (user.isDefined) user.head.\("image").get.as[String] else "/assets/images/noImage.gif"
        val jsonTask = Json.obj(
          "_id" -> jsonId,
          "desc" -> description,
          "extra" -> extra,
          "creator" -> sessionUser,
          "assignee" -> assigneeOrEmpty,
          "assigneeImg" -> image,
          "status" -> 1,
          "created" -> Json.obj("$date" -> DateTime.now().getMillis),
          "modified" -> Json.obj("$date" -> DateTime.now().getMillis)
        )
        tasks.insert(jsonTask)
        Ok(Json.toJson(jsonTask)) //TODO: MAKE A CASE IF THE INSERT FAILS
      }
    }
  }

  /** GENERAL FUNCTIONALITY */
  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

  def deleteTask(taskId: String) = Action.async {
    { request =>
      //Checking the user has not changed his session cookie manually
      if (request.session.get("connected").isEmpty) {
        Future.apply {
          BadRequest.withNewSession
        }
      } else {
        val jsonTaskId = Json.obj("$oid" -> taskId)
        for {
          tasks <- tasksFuture(request.session.get("companyName").get)
          sessionUser = request.session.get("connected")
          selector = Json.obj("_id" -> jsonTaskId, "creator" -> sessionUser)
          deleteTaskResult <- tasks.remove(selector)
        } yield {
          if (deleteTaskResult.ok && deleteTaskResult.n == 1) Ok("success") else BadRequest
        }
      }
    }
  }

  def leaveTask(taskId: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val jsonId = Json.obj("$oid" -> taskId)
      for {
        tasks <- tasksFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected")
        selector = Json.obj("_id" -> jsonId, "assignee" -> sessionUser)
        modifier = Json.obj("$set" -> Json.obj("assignee" -> "", "status" -> 1, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        tasksResult <- tasks.update(selector, modifier)
      } yield {
        if (tasksResult.ok && tasksResult.n == 1) Ok("success") else Ok("failure")
      }
    }
  }

  def tasksFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("tasks_" + companyName))

  def adoptTask(taskId: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val jsonId = Json.obj("$oid" -> taskId)
      for {
        tasks <- tasksFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected")
        selector = Json.obj("_id" -> jsonId, "assignee" -> "")
        modifier = Json.obj("$set" -> Json.obj("assignee" -> sessionUser, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        tasksResult <- tasks.update(selector, modifier)
      } yield {
        if (tasksResult.ok && tasksResult.n == 1) Ok("success") else Ok("failure") // make this return JSON object!
      }
    }
  }

  def swapTaskStatus(taskId: String, status: Int) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val jsonId = Json.obj("$oid" -> taskId)
      for {
        tasks <- tasksFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected")
        selector = Json.obj("_id" -> jsonId, "assignee" -> sessionUser)
        modifier = Json.obj("$set" -> Json.obj("status" -> status, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        tasksResult <- tasks.update(selector, modifier)
      } yield {
        if (tasksResult.ok && tasksResult.n == 1) Ok("success") else Ok("failure") // make this return JSON object!
      }
    }
  }

  def editTask(taskId: String, description: String, assignee: String, extra: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val jsonId = Json.obj("$oid" -> taskId)
      for {
        tasks <- tasksFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected")
        selector = Json.obj("_id" -> jsonId, "creator" -> sessionUser)
        modifier = Json.obj("$set" -> Json.obj("desc" -> description, "assignee" -> assignee, "extra" -> extra, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        tasksResult <- tasks.update(selector, modifier)
      } yield {
        if (tasksResult.ok && tasksResult.n == 1) Ok("success") else Ok("failure") // make this return JSON object!
      }
    }
  }


  //  $or : [
  //  {$and : [{modified: {$gt: data.date}},{"creator.email": { $ne: data.email}}]},
  //  {"comments.modified": {$gt: data.date}}
  //  ]
  //  Json.obj("$or"->Json.arr(
  //    Json.obj("$and"->Json.arr(Json.obj("modified" ->Json.obj("$gt"->"DATEOBJHERE")),Json.obj("creator.email"->Json.obj("$ne"->"EMAILOBJHERE")))),
  //    Json.obj("comments.modified"-> Json.obj("$gt"->"DATEOBJHERE"))
  //  ))


  def socket = WebSocket.accept[String, String] { request =>
    val sessionCompany = request.session.get("companyName").get
    val sessionUser = request.session.get("companyName").get
    ActorFlow.actorRef(out => TaskWebSocketActor.props(out, sessionCompany, sessionUser))
  }

  case class InEvent(dateInSecs: String)

  case class OutEvent(dateInSecs: String)

  class TaskWebSocketActor(out: ActorRef, sessionCompany: String, sessionUser: String) extends Actor {
    def receive = {
      case msg: String =>
        for {
          tasksFut <- tasksFuture(sessionCompany)
          date = Json.obj("$date" -> DateTime.parse(msg).getMillis)
          taskSelector = Json.obj("$and" -> Json.arr(Json.obj("modified" -> Json.obj("$gt" -> date)), Json.obj("creator" -> Json.obj("$ne" -> sessionUser))))
          taskscursor <- tasksFut.find(taskSelector).sort(Json.obj("modified" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
          tasksObj = taskscursor
        } yield {
          out ! Json.toJson(tasksObj).toString
        }
    }
  }

  object TaskWebSocketActor {
    def props(out: ActorRef, sessionCompany: String, sessionUser: String) = Props(new TaskWebSocketActor(out, sessionCompany, sessionUser))
  }
}
