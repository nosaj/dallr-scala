/**
  * Created by jason on 10/30/16.
  */

package controllers

import javax.inject.Inject
import play.api.libs.json.{JsNumber, _}
import play.modules.reactivemongo.json._
import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.ReadPreference
import reactivemongo.play.json.collection.JSONCollection
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


class GeneralController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext) extends Controller with MongoController with ReactiveMongoComponents {
  def getUsers = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      //TODO: SAVE ALL USERS IN SEPERATE COLLECTION CALLED "Users_CompanyName"
      for {
        usersFut <- usersFuture
        companyName = request.session.get("companyName")
        usersscursor <- usersFut.find(Json.obj("companyName" -> companyName), Json.obj("firstname" -> 1, "lastname" -> 1, "jobtitle" -> 1, "username" -> 1, "image" -> 1, "_id" -> 1)).sort(Json.obj("firstname" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
      } yield {
        Ok(Json.toJson(usersscursor))
      }
    }
  }

  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

}