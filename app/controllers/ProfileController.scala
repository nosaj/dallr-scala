package controllers

import javax.inject.Inject

import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{Action, Controller}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.ReadPreference
import reactivemongo.play.json.collection.JSONCollection
import play.modules.reactivemongo.json._

import scala.concurrent.{ExecutionContext, Future}

class ProfileController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext) extends Controller with MongoController with ReactiveMongoComponents {
  def showPage(user: String = "session") = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      val realUser: String = if (user == "session") request.session.get("connected").get else user
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        companyName = request.session.get("companyName").get
        futureTasks <- tasksFuture(companyName)
        userSession <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        user <- futureUser.find(Json.obj("username" -> realUser)).cursor[JsObject](ReadPreference.primary).headOption
        tasks <- futureTasks.find(Json.obj("assignee" -> realUser, "status" -> 2)).cursor[JsObject](ReadPreference.primary).collect[List]()
        futureSettings <- settingsFuture(request.session.get("companyName").get)
        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
        publicInvites = settings.head.\("invites").as[Boolean]
      } yield {
        if (userSession.isDefined) {
          val isAdmin = userSession.head.\("admin").as[Boolean]
          if (user.isEmpty) {
            Redirect(routes.ProfileController.showPage("session"))
          } else {
            val content = views.html.Profile.profile(request.session.get("connected").get, Json.toJson(user.head), isAdmin, publicInvites)
            Ok(content)
          }
        } else {
          Redirect(routes.AccountController.showPage()).withNewSession
        }
      }
    }
  }

  def settingsFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("settings_" + companyName))

  def getUserTasks(user: String) = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest
      }
    } else {
      for {
        futureTasks <- tasksFuture(request.session.get("companyName").get)
        tasks <- futureTasks.find(Json.obj("assignee" -> user, "status" -> 2)).cursor[JsObject](ReadPreference.primary).collect[List]() //TODO: NOSQL INJECTION???
      } yield {
        Ok(Json.toJson(tasks))
      }
    }
  }

  def tasksFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("tasks_" + companyName))

  def swapStatus = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        user <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        isOnline = user.head.\("isOnline").get.as[Boolean]
        selector = Json.obj("username" -> sessionUser)
        modifier = Json.obj("$set" -> Json.obj("isOnline" -> !isOnline))
        userResult <- futureUser.update(selector, modifier)
      } yield {
        if (userResult.ok) Ok("success")
        else BadRequest
      }
    }
  }

  def editProfile = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      val profileSection = request.body.asFormUrlEncoded.get("name").head
      val value = request.body.asFormUrlEncoded.get("value").head
      val selector = Json.obj("username" -> request.session.get("connected").get)
      val modifier = profileSection match {
        case "firstname" => Json.obj("$set" -> Json.obj("firstname" -> value, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        case "lastname" => Json.obj("$set" -> Json.obj("lastname" -> value, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        case "onlineUntil" => Json.obj("$set" -> Json.obj("onlineUntil" -> value, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        case "jobtitle" => Json.obj("$set" -> Json.obj("jobtitle" -> value, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        case "statusMessage" => Json.obj("$set" -> Json.obj("statusMessage.content" -> value, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        case "phonenumber" => Json.obj("$set" -> Json.obj("phone" -> value, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        case _ => Json.obj("$set" -> Json.obj("modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
      }

      //THIS IS AN ALTERNATIVE OF WRITING FOR/YIELD EXPRESSIONS
      usersFuture.flatMap {
        futureUser => futureUser.update(selector, modifier).map {
          result =>
            if (result.ok) Ok("success")
            else BadRequest
        }
      }
    }
  }

  /** GENERAL FUNCTIONALITY */
  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

  def editPassword = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        user <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        passwordHashed = user.head.\("password").get.as[String]
        oldPassword = request.body.asFormUrlEncoded.get("value[oldPassword]").head
        newPassword = request.body.asFormUrlEncoded.get("value[newPassword]").head
        passwordMatches = BCrypt.checkpw(oldPassword, passwordHashed)
        selector = Json.obj("username" -> sessionUser)
        modifier = Json.obj("$set" -> Json.obj("password" -> BCrypt.hashpw(newPassword, BCrypt.gensalt(12)), "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
      } yield {
        //        TODO: MAKE THIS RETURN SUCCESS ONLY IF THE PASSWORD UPDATE WAS OK
        if (passwordMatches) {
          futureUser.update(selector, modifier)
          Ok("success")
        } else {
          Ok("failure")
        }
      }
    }
  }

  def deleteUser(username: String) = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        user <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        deleteUser <- futureUser.find(Json.obj("username" -> username)).cursor[JsObject](ReadPreference.primary).headOption
        isAdmin = user.head.\("admin").get.as[Boolean] // TODO: IF SESSIONUSER DOES NOT EXISTS THIS CAUSES A EXCEPTIONS
      } yield {
        //TODO: MAKE THIS RETURN SUCCESS ONLY IF USER WAS REMOVED
        if (isAdmin && deleteUser.isDefined) {
          futureUser.remove(Json.obj("username" -> username))
          if (sessionUser == username) {
            Ok("success").withNewSession
          } else {
            Ok("success")
          }
        } else {
          Ok("failure")
        }
      }
    }
  }

  def promoteToAdmin(username: String) = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        user <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        isAdmin = user.head.\("admin").get.as[Boolean] // TODO: IF SESSIONUSER DOES NOT EXISTS THIS CAUSES A EXCEPTIONS
        selector = Json.obj("username" -> username)
        modifier = Json.obj("$set" -> Json.obj("admin" -> true))
        userResult <- futureUser.update(selector, modifier)
      } yield {
        if (userResult.ok && userResult.n == 1) Ok("success")
        else BadRequest
      }
    }
  }

}
