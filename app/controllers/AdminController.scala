package controllers

import javax.inject.Inject

import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{Action, Controller}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.ReadPreference
import reactivemongo.play.json.collection.JSONCollection
import play.modules.reactivemongo.json._

import scala.concurrent.{ExecutionContext, Future}

class AdminController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext) extends Controller with MongoController with ReactiveMongoComponents {
  def showPage = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureUsers <- usersFuture
        futureSettings <- settingsFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected").get
        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
        user <- futureUsers.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        publicInvites = settings.head.\("invites").as[Boolean]
        userAdmin = user.head.\("admin").as[Boolean]
      } yield {
        if (userAdmin) {
          val content = views.html.Admin.admin(sessionUser, publicInvites)
          Ok(content)
        } else {
          Redirect(routes.PostController.showPage())
        }
      }

    }
  }

  def getAdmins = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest
      }
    } else {
      for {
        futureUsers <- usersFuture
        users <- futureUsers.find(Json.obj("admin" -> true)).cursor[JsObject](ReadPreference.primary).collect[List]()
      } yield {
        Ok(Json.toJson(users))
      }
    }
  }

  /** GENERAL FUNCTIONALITY */
  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

  def getInvites = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest
      }
    } else {
      for {
        futureInvites <- invitesFuture(request.session.get("companyName").get)
        invites <- futureInvites.find(Json.obj()).sort(Json.obj("created" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
      } yield {
        Ok(Json.toJson(invites))
      }
    }
  }

  def invitesFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("invites_" + companyName))

  def toggleInvites = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest
      }
    } else {
      for {
        futureUsers <- usersFuture
        companyName = request.session.get("companyName").get
        futureSettings <- settingsFuture(companyName)
        sessionUser = request.session.get("connected").get
        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
        user <- futureUsers.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        publicInvites = settings.head.\("invites").as[Boolean]
        userAdmin = user.head.\("admin").as[Boolean]
      } yield {
        if (userAdmin) {
          futureSettings.update(Json.obj(), Json.obj("$set" -> Json.obj("invites" -> !publicInvites)))
          Ok("success")
        } else {
          Ok("failure")
        }
      }
    }
  }

  def settingsFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("settings_" + companyName))

  def demoteAdmin(userId: String) = Action.async { request =>
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest
      }
    } else {
      for {
        futureUsers <- usersFuture
        companyName = request.session.get("companyName").get
        sessionUser = request.session.get("connected").get
        user <- futureUsers.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        userAdmin = user.head.\("admin").as[Boolean]
        adminCount <- futureUsers.count(Some(Json.obj("admin" -> true)))
      } yield {
        if (userAdmin && adminCount > 1) {
          val jsonId = Json.obj("$oid" -> userId)
          futureUsers.update(Json.obj("_id" -> jsonId, "admin" -> true), Json.obj("$set" -> Json.obj("admin" -> false, "modified" -> Json.obj("$date" -> DateTime.now().getMillis))))
          Ok("success")
        } else {
          Ok("failure")
        }
      }
    }
  }
}
