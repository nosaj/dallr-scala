package controllers

import java.util.concurrent.TimeUnit
import javax.inject.Inject

import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.{QueryOpts, ReadPreference}
import reactivemongo.play.json.collection.JSONCollection
import akka.actor._
import akka.cluster.Cluster
import akka.cluster.ddata.Replicator._
import akka.cluster.ddata.{DistributedData, LWWMap, LWWMapKey}
import akka.event.LoggingReceive
import akka.stream.Materializer
import org.joda.time.DateTime
import play.api.libs.json.{JsObject, _}
import play.api.libs.streams.ActorFlow
import play.modules.reactivemongo.json._
import reactivemongo.bson.BSONObjectID
import akka.util.Timeout
import play.api.libs.ws._
import play.api.libs.Codecs._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class MessageController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext, system: ActorSystem, mat: Materializer, ws: WSClient) extends Controller with MongoController with ReactiveMongoComponents {
  implicit val timeout = Timeout(5, TimeUnit.SECONDS)

  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))
  def showPage = Action.async { implicit request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      val content = views.html.Message.message(request.session.get("connected").get, request.session.get("admin").get, true)
      Future.apply {
        Ok(content)
      }
    }
  }

  def filterMessage(json: List[JsObject], id: String) = List[List[List[JsObject]]] {
    json.map {
      individualJson =>
        val messages = individualJson.\("messages").get.as[List[JsObject]]
        messages.filter {
          x => x.\("_id").\("$oid").get.as[String].equals(id)
        }
    }
  }


  def getBCValidation(parentId: String, messageId: String) = Action.async { request =>
    val selector = Json.obj("_id" -> Json.obj("$oid" -> parentId))
    val companyName: String = request.session.get("companyName").get
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      for {
        futureMessage <- messagesFuture(companyName)
        futureMessageCursor <- futureMessage.find(selector).one[JsObject]
        messages = futureMessageCursor.get.\("messages").get.as[List[JsObject]]
        filteredMessages = messages.filter(x => x.\("_id").\("$oid").get.as[String].equals(messageId)).head
        date = filteredMessages.\("date").get.\("$date").get.toString() // date always fails unless done in this way
        id = filteredMessages.\("_id").\("$oid").get.as[String]
        msg = filteredMessages.\("msg").get.as[String]
        from = filteredMessages.\("from").get.as[String]
        hashOfMessage = sha1(id + from + msg + date)
        response = blockchainUrlQuery(hashOfMessage)
        responseResult <- response
      } yield {
        Ok(responseResult.toString)
      }
    }
  }

  def loadMessages = Action.async { request =>
    val sessionUser: String = request.session.get("connected").get
    val companyName: String = request.session.get("companyName").get

    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> sessionUser), Json.obj("users.1.username" -> sessionUser)))
      for {
        messagesFut <- messagesFuture(companyName)
        messagesCursor <- messagesFut.find(selector).sort(Json.obj("date" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
        verifiedMessages = blockchainQuery(messagesCursor)
        futureListFutureList = Future.sequence(verifiedMessages)
        listFuture <- futureListFutureList
        futureList = Future.sequence(listFuture)
        jsonList <- futureList
      } yield {
        Ok(Json.toJson(jsonList))
      }
    }
  }

  def blockchainQuery(jsonList: List[JsObject]): List[Future[Future[JsObject]]] = {
    jsonList.map {
      individualJson =>
        val date = individualJson.value.get("date").get.\("$date").get.toString() // date always fails unless done in this way
      val id = individualJson.\("_id").\("$oid").get.as[String]
        val users = individualJson.\("users").get.as[List[JsObject]]
        val to = users.head.\("username").get.as[String]
        val from = users.tail.head.\("username").get.as[String]
        val messages = individualJson.\("messages").as[List[JsObject]]
        val messageBlockchained = blockchainAddValidity(messages)
        val futureListMessages = Future.sequence(messageBlockchained)
        val combinedHash = sha1(id + date + to + from)
        val response = blockchainUrlQuery(combinedHash)
        //          System.out.println("Top Hash in Query is: "+combinedHash)
        futureListMessages.map {
          msgs =>
            response.map {
              err => Json.obj("_id" -> id, "date" -> Json.obj("$date" -> date), "messages" -> msgs, "errorBC" -> err, "users" -> Json.arr(Json.obj("username" -> to), Json.obj("username" -> from)))
            }
        }
    }
  }

  def singleBlockchainQuery(jsonObj: JsObject): Future[JsObject] = {
    val date = jsonObj.value.get("date").get.\("$date").get.toString() // date always fails unless done in this way
    val id = jsonObj.\("_id").\("$oid").get.as[String]
    val users = jsonObj.\("users").get.as[List[JsObject]]
    val to = users.head.\("username").get.as[String]
    val from = users.tail.head.\("username").get.as[String]
    val messages = jsonObj.\("messages").as[List[JsObject]]
    val messageBlockchained = blockchainAddValidity(messages)
    val futureListMessages = Future.sequence(messageBlockchained)
    val combinedHash = sha1(id + date + to + from)
    val response = blockchainUrlQuery(combinedHash)
    //          System.out.println("Top Hash in Query is: "+combinedHash)
    futureListMessages.flatMap {
      msgs =>
        response.map {
          err => Json.obj("_id" -> id, "date" -> Json.obj("$date" -> date), "messages" -> msgs, "errorBC" -> err.toString, "users" -> Json.arr(Json.obj("username" -> to), Json.obj("username" -> from)))
        }
    }


  }
  def blockchainAddValidity(jsonList: List[JsObject]): List[Future[JsObject]] = {
    jsonList.map {
      individualJson =>
        val from = individualJson.\("from").as[String]
        val msg = individualJson.\("msg").as[String]
        val date = individualJson.value.get("date").get.\("$date").get.toString() // date always fails unless done in this way
      val id = individualJson.\("_id").\("$oid").get.as[String]

        val combinedHash = sha1(id + from + msg + date)
        //        System.out.println("Msg: "+msg+" gives this hash: "+combinedHash)

        //        val url = "http://localhost:46657/abci_query?path=\"\"&data=\"" + combinedHash + "\"&prove=false"
        blockchainUrlQuery(combinedHash).map {
          x => Json.obj("_id" -> Json.obj("$oid" -> id), "from" -> from, "msg" -> msg, "date" -> date, "errorBC" -> x)
        }
    }
  }

  def blockchainUrlQuery(query: String): Future[Boolean] = {
    val url = "http://localhost:46657/abci_query?path=\"\"&data=\"" + query + "\"&prove=false"
    ws.url(url).get().map {
      response =>
        Json.parse(response.body).as[JsObject].value.get("result").head.\("response").\("log").as[String].equals("exists")
    }
  }


  def insertMultiMessage(toEmail: List[String], message: String) = Action.async { request =>
    val fromEmail: String = request.session.get("connected").get
    val companyName: String = request.session.get("companyName").get
    val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> fromEmail, "users.1.username" -> toEmail), Json.obj("users.1.username" -> fromEmail, "users.0.username" -> toEmail)))

    val futureMessage: Future[Option[JsObject]] = messagesFuture(companyName).flatMap {
      // find all users with username `username`
      _.find(selector).
        // perform the query and get a cursor of JsObject then return the head
        cursor[JsObject](ReadPreference.primary).headOption
    }

    futureMessage onFailure {
      case t => BadRequest
    }

    futureMessage.map {
      case Some(jsonObj) => {
        //ADD MESSAGE TO EXISTING CONVERSATION
        val jsonMessage = Json.obj("from" -> fromEmail, "msg" -> message, "date" -> Json.obj("$date" -> DateTime.now().getMillis))
        val modifier = Json.obj(
          "$push" -> Json.obj(
            "messages" -> jsonMessage
          )
        )
        val updateResult = messagesFuture(companyName).flatMap {
          _.update(selector, modifier)
        }
        Ok("success")
      }
      case None => {
        val jsonMessage = Json.obj("messages" -> Json.arr(Json.obj("from" -> fromEmail, "msg" -> message, "date" -> Json.obj("$date" -> DateTime.now().getMillis))), "users" -> Json.arr(Json.obj("username" -> toEmail), Json.obj("username" -> fromEmail)))
        val insertResult = messagesFuture(companyName).flatMap {
          _.insert(jsonMessage)
        }
        Ok("success")
      }
    }
  }

  def messagesFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("messages_" + companyName))


  def insertMessage(toEmail: String, message: String) = Action.async { request =>
    val fromEmail: String = request.session.get("connected").get
    val companyName: String = request.session.get("companyName").get
    val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> fromEmail, "users.1.username" -> toEmail), Json.obj("users.1.username" -> fromEmail, "users.0.username" -> toEmail)))

    val futureMessage: Future[Option[JsObject]] = messagesFuture(companyName).flatMap {
      // find all users with username `username`
      _.find(selector).
        // perform the query and get a cursor of JsObject then return the head
        cursor[JsObject](ReadPreference.primary).headOption
    }

    futureMessage onFailure {
      case t => BadRequest
    }

    futureMessage.map {
      case Some(jsonObj) => {
        val jsonId = jsonObj.\("_id").get

        //ADD MESSAGE TO EXISTING CONVERSATION
        val jsonMessage = Json.obj("from" -> fromEmail, "msg" -> message, "date" -> Json.obj("$date" -> DateTime.now().getMillis))
        val modifier = Json.obj(
          "$push" -> Json.obj(
            "messages" -> jsonMessage
          )
        )
        val updateResult = messagesFuture(companyName).flatMap {
          _.update(selector, modifier)
        }

        Ok(jsonId)
      }
      case None => {
        val id = BSONObjectID.generate()
        val jsonId = Json.obj("$oid" -> id.stringify)
        val jsonMessage = Json.obj("_id" -> jsonId, "messages" -> Json.arr(Json.obj("from" -> fromEmail, "msg" -> message, "date" -> Json.obj("$date" -> DateTime.now().getMillis))), "users" -> Json.arr(Json.obj("username" -> toEmail), Json.obj("username" -> fromEmail)))
        val insertResult = messagesFuture(companyName).flatMap {
          _.insert(jsonMessage)
        }
        Ok(jsonId)
      }
    }
  }

  def insertLocalMessage(toEmail: String, message: String, fromEmail: String, companyName: String): Future[Future[JsValue]] = {
    val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> fromEmail, "users.1.username" -> toEmail), Json.obj("users.1.username" -> fromEmail, "users.0.username" -> toEmail)))

    val futureMessage: Future[Option[JsObject]] = messagesFuture(companyName).flatMap {
      // find all users with username `username`
      _.find(selector).
        // perform the query and get a cursor of JsObject then return the head
        cursor[JsObject](ReadPreference.primary).headOption
    }

    futureMessage onFailure {
      case t => "failure"
    }

    futureMessage.map {
      case Some(jsonObj) => {
        val jsonId = jsonObj.\("_id").get
        //ADD MESSAGE TO EXISTING CONVERSATION
        val dateTimeNow = DateTime.now().getMillis
        val nestedMsgid = BSONObjectID.generate()
        val nestedMsgjsonId = Json.obj("$oid" -> nestedMsgid.stringify)
        val jsonMessage = Json.obj("_id" -> nestedMsgjsonId, "from" -> fromEmail, "msg" -> message, "date" -> Json.obj("$date" -> dateTimeNow))
        val modifier = Json.obj(
          "$push" -> Json.obj(
            "messages" -> jsonMessage
          )
        )
        val hashOfMessage = sha1(nestedMsgid.stringify + fromEmail + message + dateTimeNow.toString)
        val url = "http://127.0.0.1:46657/broadcast_tx_sync?tx=\"" + hashOfMessage + "\""
        val insertResponse = ws.url(url).get().map { response =>
          System.out.println(response.body)
          response.json.\("error").as[String]
        }

        val updateResult = messagesFuture(companyName).flatMap {
          _.update(selector, modifier)
        }

        //        THIS IS WHEN UPDATING DATE WAS USED
        //        val modifierDate = Json.obj("$set" -> Json.obj("date" -> Json.obj("$date" -> DateTime.now().getMillis)))
        //
        //        val updateDateResult = messagesFuture(companyName).flatMap {
        //          _.update(selector, modifierDate)
        //        }

        updateResult.map {
          x => Json.obj("parentID" -> jsonId, "messageID" -> nestedMsgjsonId)
        }
      }
      case None => {
        val id = BSONObjectID.generate()
        val dateTimeNow = DateTime.now().getMillis
        val jsonId = Json.obj("$oid" -> id.stringify)
        val nestedMsgid = BSONObjectID.generate()
        val nestedMsgjsonId = Json.obj("$oid" -> nestedMsgid.stringify)
        val jsonMessage = Json.obj("_id" -> jsonId, "date" -> Json.obj("$date" -> dateTimeNow), "messages" -> Json.arr(Json.obj("_id" -> nestedMsgjsonId, "from" -> fromEmail, "msg" -> message, "date" -> Json.obj("$date" -> DateTime.now().getMillis))), "users" -> Json.arr(Json.obj("username" -> toEmail), Json.obj("username" -> fromEmail)))
        val insertResult = messagesFuture(companyName).flatMap {
          _.insert(jsonMessage)
        }

        //val hashForTop = sha1(id.stringify + dateTimeNow + toEmail + fromEmail)
        val hashForMessage = sha1(nestedMsgid.stringify + fromEmail + message + dateTimeNow.toString)

        val url = "http://127.0.0.1:46657/broadcast_tx_sync?tx=\"" + hashForMessage + "\""

        for {
          insertResultMapped <- insertResult
          insertMessageResponse <- ws.url(url).get()
          resultMessageResponse = insertMessageResponse.body
        } yield {
          System.out.println(insertMessageResponse)
          Json.obj("parentID" -> jsonId, "messageID" -> nestedMsgjsonId)
        }
      }
    }
  }

  // THIS FUNCTION IS FOR LOADING SINGLE MESSAGES ON THE LOAD TEST
  def getNewestMessageLoadTest = Action.async { request =>
    val sessionUser: String = request.queryString.get("user").flatMap(_.headOption).get
    val companyName: String = "test"
    //Checking the user has not changed his session cookie manually
    if (request.queryString.get("user").flatMap(_.headOption).isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> sessionUser), Json.obj("users.1.username" -> sessionUser)))
      for {
        messagesFut <- messagesFuture(companyName)
        //        messagesCursor <- messagesFut.find(selector).sort(Json.obj("date" -> 1)).one[JsObject]
        messagesCursor <- messagesFut.find(selector, Json.obj("messages" -> Json.obj("$slice" -> 1))).sort(Json.obj("date" -> 1)).one[JsObject]
        message = messagesCursor
        verifiedMessage <- singleBlockchainQuery(message.get)

      } yield {
        Ok(verifiedMessage)
      }
    }
  }

  // THIS FUNCTION IS FOR LOADING EACH MESSAGE FOR ALL USERS, USED ON THE 1 to N LOAD TEST
  def getNewestMessageAllUsersLoadTest = Action.async { request =>
    val sessionUser: String = request.queryString.get("user").flatMap(_.headOption).get
    val companyName: String = "test"
    //Checking the user has not changed his session cookie manually
    if (request.queryString.get("user").flatMap(_.headOption).isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> sessionUser), Json.obj("users.1.username" -> sessionUser)))
      for {
        messagesFut <- messagesFuture(companyName)
        //        messagesCursor <- messagesFut.find(selector).sort(Json.obj("date" -> 1)).one[JsObject]
        messagesCursor <- messagesFut.find(selector, Json.obj("messages" -> Json.obj("$slice" -> 1))).sort(Json.obj("date" -> 1)).one[JsObject]
        message = messagesCursor
        verifiedMessage <- singleBlockchainQuery(message.get)
      } yield {
        Ok(verifiedMessage)
      }
    }
  }

  def getNewestMessage(user: String, companyName: String): Future[JsObject] = {
    val selector = Json.obj("$or" -> Json.arr(Json.obj("users.0.username" -> user), Json.obj("users.1.username" -> user)))
    val futureMessage: Future[Option[JsObject]] = messagesFuture(companyName).flatMap {
      // find all users with username `username`
      _.find(selector).sort(Json.obj("date" -> -1)).one[JsObject]
    }

    futureMessage onFailure {
      case t => Json.obj("type" -> "failure")
    }

    futureMessage.map {
      case Some(jsonObj) => {
        jsonObj
      }
      case None => {
        Json.obj("type" -> "failure")
      }
    }
  }

  def socket = WebSocket.acceptOrResult[JsValue, JsValue] { request =>
    Future.successful(request.queryString.get("user").flatMap(_.headOption) match {
      case None => Left(Forbidden)
      case Some(uid) =>
        val companyID = "test"
        //        val companyID = request.session.get("companyName").get
        Right(ActorFlow.actorRef(MessageSocket.props(uid, companyID)))
    })
  }

  class MessageSocket(uid: String, companyID: String, out: ActorRef) extends Actor {
    val replicator = DistributedData(context.system).replicator
    implicit val node = Cluster(context.system)
    def receive = LoggingReceive {
      case g@GetSuccess(key, req) =>
        val keyString = key.id
        val element = g.get[LWWMap[ActorRef]](userMapKey(keyString)).get(keyString)
        element match {
          case None => ;
          case Some(a) => a ! Json.obj("type" -> "recieveMsg", "to" -> keyString, "message" -> "new message")
        }

      case js: JsValue =>
        ((js \ "type").as[String]) match {
          case "subscribe" =>
            replicator ! Update(userMapKey(uid), LWWMap.empty[ActorRef], WriteLocal) {
              map => map.put(uid, self)
            }

          case "recieveMsg" =>
            val to = (js \ "to").as[String]
            val msg = (js \ "message").as[String]
            for {
            //TODO: MAKE IT ONLY RETURN A SINGLE MESSAGE
              newMessage <- getNewestMessage(uid, companyID)
            } yield {
              val jsonMessage = Json.obj("type" -> "newMessage", "errorBC" -> "new", "data" -> newMessage, "date" -> DateTime.now().getMillis)
              val lastMessage = newMessage
              out ! Json.toJson(jsonMessage)
            }

          case "sendMsg" =>
            val to = (js \ "to").as[String]
            val msg = (js \ "message").as[String]
            for {
              insertResult <- insertLocalMessage(to, msg, uid, companyID)
              nextFuture <- insertResult
              parentID = nextFuture.\("parentID").get
              messageID = nextFuture.\("messageID").get
            } yield {
              if (nextFuture.toString() != "failure") {
                replicator ! Get(userMapKey(to), ReadMajority(timeout = 5.seconds))
                out ! Json.obj("type" -> "messageSent", "_id" -> messageID, "parentID" -> parentID, "from" -> uid, "msg" -> msg, "status" -> "success", "errorBC" -> "new", "date" -> DateTime.now().getMillis)
              } else {
                out ! Json.obj("type" -> "messageSent", "status" -> "failure")
              }
            }
        }
    }

    def userMapKey(a: String) = LWWMapKey[ActorRef](a)
  }

  object MessageSocket {
    def props(user: String, companyID: String)(out: ActorRef) = Props(new MessageSocket(user, companyID, out))
  }

}