package controllers
import javax.inject.Inject

import akka.actor.{Actor, ActorRef, Props}
import akka.actor.ActorSystem
import akka.stream.Materializer
import org.joda.time.DateTime
import play.api.libs.json._
import play.modules.reactivemongo.json._
import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.ReadPreference
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.collection.JSONCollection
import play.api.libs.streams.ActorFlow
import play.api.mvc.WebSocket.MessageFlowTransformer

import scala.concurrent.{ExecutionContext, Future}

class PostController @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit exec: ExecutionContext, system: ActorSystem, materializer: Materializer) extends Controller with MongoController with ReactiveMongoComponents {
  implicit val inEventFormat = Json.format[InEvent]
  implicit val outEventFormat = Json.format[OutEvent]
  implicit val messageFlowTransformer = MessageFlowTransformer.jsonMessageFlowTransformer[InEvent, OutEvent]

  /** GENERAL FUNCTIONALITY */
  def showPage = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        Redirect(routes.AccountController.showPage()).withNewSession
      }
    } else {
      for {
        futureSettings <- settingsFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected").get
        isAdmin = request.session.get("admin").get
        settings <- futureSettings.find(Json.obj()).cursor[JsObject](ReadPreference.primary).headOption
        publicInvites = settings.head.\("invites").as[Boolean]
      } yield {
        val content = views.html.Post.posts(sessionUser, isAdmin, publicInvites)
        Ok(content)
      }
    }
  }

  def settingsFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("settings_" + companyName))

  def loadPosts = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      for {
        postsFut <- postsFuture(request.session.get("companyName").get)
        companyName = request.session.get("companyName")
        postscursor <- postsFut.find(Json.obj()).sort(Json.obj("modified" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
        postsObj = postscursor
      } yield {
        Ok(Json.toJson(postsObj))
      }
    }
  }

  /** POSTS SPECIFIC FUNCTIONALITY */
  def insertPost(content: String, tag: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val id = BSONObjectID.generate()
      val jsonId = Json.obj("$oid" -> id.stringify)
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        companyName = request.session.get("companyName").get
        user <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        posts <- postsFuture(companyName)
        firstname = user.head.\("firstname").get
        jobtitle = user.head.\("jobtitle").get
        image = user.head.\("image").get
        jsonPost = Json.obj(
          "_id" -> jsonId,
          "content" -> content,
          "tag" -> tag,
          "created" -> Json.obj("$date" -> DateTime.now().getMillis),
          "modified" -> Json.obj("$date" -> DateTime.now().getMillis),
          "creator" -> Json.obj(
            "username" -> sessionUser,
            "firstname" -> firstname,
            "image" -> image,
            "jobtitle" -> jobtitle
          ),
          "comments" -> Json.arr(
          ),
          "bookmarks" -> Json.arr(
          ))
        postResult <- posts.insert(jsonPost)
      } yield {
        if (postResult.ok) Ok(Json.toJson(List(jsonPost))) else BadRequest
      }
    }
  }

  def postsFuture(companyName: String): Future[JSONCollection] = database.map(_.collection[JSONCollection]("posts_" + companyName))

  def usersFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("users"))

  def editPost(postId: String, content: String, tag: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val jsonId = Json.obj("$oid" -> postId)
      for {
        posts <- postsFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected")
        selector = Json.obj("_id" -> jsonId, "creator.username" -> sessionUser)
        modifier = Json.obj("$set" -> Json.obj("content" -> content, "tag" -> tag, "modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        postResult <- posts.update(selector, modifier)
      } yield {
        if (postResult.ok) Ok("success") else BadRequest
      }
    }
  }

  def deletePost(postId: String) = Action.async {
    { request =>
      //Checking the user has not changed his session cookie manually
      if (request.session.get("connected").isEmpty) {
        Future.apply {
          BadRequest.withNewSession
        }
      } else {
        val jsonPostId = Json.obj("$oid" -> postId)
        for {
          posts <- postsFuture(request.session.get("companyName").get)
          sessionUser = request.session.get("connected")
          selector = Json.obj("_id" -> jsonPostId, "creator.username" -> sessionUser)
          deletePostResult <- posts.remove(selector)
        } yield {
          if (deletePostResult.ok && deletePostResult.n == 1) Ok("success") else BadRequest
        }
      }
    }
  }

  /** COMMENTS SPECIFIC FUNCTIONALITY */
  def insertComment(postId: String, content: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val id = BSONObjectID.generate()
      val jsonCommentId = Json.obj("$oid" -> id.stringify)
      val jsonPostId = Json.obj("$oid" -> postId)
      for {
        futureUser <- usersFuture
        sessionUser = request.session.get("connected")
        companyName = request.session.get("companyName").get
        user <- futureUser.find(Json.obj("username" -> sessionUser)).cursor[JsObject](ReadPreference.primary).headOption
        posts <- postsFuture(companyName)
        firstname = user.head.\("firstname").get
        jobtitle = user.head.\("jobtitle").get
        image = user.head.\("image").get
        jsonComment = Json.obj(
          "_id" -> jsonCommentId,
          "content" -> content,
          "created" -> Json.obj("$date" -> DateTime.now().getMillis),
          "modified" -> Json.obj("$date" -> DateTime.now().getMillis),
          "creator" -> Json.obj(
            "username" -> sessionUser,
            "firstname" -> firstname,
            "image" -> image,
            "jobtitle" -> jobtitle
          ))
        selector = Json.obj("_id" -> jsonPostId)
        modifier = Json.obj(
          "$push" -> Json.obj(
            "comments" -> jsonComment
          )
        )
        commentResult <- posts.update(selector, modifier)
      } yield {
        if (commentResult.ok) Ok(Json.toJson(List(jsonComment.++(Json.obj("postId" -> postId))))) else BadRequest
      }
    }
  }

  def editComment(commentId: String, content: String) = Action.async { request =>
    //Checking the user has not changed his session cookie manually
    if (request.session.get("connected").isEmpty) {
      Future.apply {
        BadRequest.withNewSession
      }
    } else {
      val jsonCommentId = Json.obj("$oid" -> commentId)
      for {
        posts <- postsFuture(request.session.get("companyName").get)
        sessionUser = request.session.get("connected")
        selector = Json.obj("comments" -> Json.obj("$elemMatch" -> Json.obj("_id" -> jsonCommentId, "creator.username" -> sessionUser)))
        modifier = Json.obj("$set" -> Json.obj("comments.$.content" -> content, "comments.$.modified" -> Json.obj("$date" -> DateTime.now().getMillis)))
        commentResult <- posts.update(selector, modifier)
      } yield {
        if (commentResult.ok) Ok("success") else BadRequest
      }
    }
  }

  def deleteComment(postId: String, commentId: String) = Action.async {
    { request =>
      //Checking the user has not changed his session cookie manually
      if (request.session.get("connected").isEmpty) {
        Future.apply {
          BadRequest.withNewSession
        }
      } else {
        val jsonPostId = Json.obj("$oid" -> postId)
        val jsonCommentId = Json.obj("$oid" -> commentId)
        for {
          posts <- postsFuture(request.session.get("companyName").get)
          sessionUser = request.session.get("connected")
          selector = Json.obj("comments" -> Json.obj("$elemMatch" -> Json.obj("_id" -> jsonCommentId, "creator.username" -> sessionUser)))
          modifier = Json.obj(
            "$pull" -> Json.obj(
              "comments" -> Json.obj(
                "_id" -> jsonCommentId
              )
            )
          )
          deleteCommentResult <- posts.update(selector, modifier)
        } yield {
          if (deleteCommentResult.ok)
            Ok("success")
          else BadRequest
        }
      }
    }
  }

  /** BOOKMARKS SPECIFIC FUNCTIONALITY */
  def addBookmark(postId: String) = Action.async {
    { request =>
      //Checking the user has not changed his session cookie manually
      if (request.session.get("connected").isEmpty) {
        Future.apply {
          BadRequest.withNewSession
        }
      } else {
        val jsonPostId = Json.obj("$oid" -> postId)
        for {
          posts <- postsFuture(request.session.get("companyName").get)
          sessionUser = request.session.get("connected")
          selector = Json.obj("_id" -> jsonPostId)
          modifier = Json.obj(
            "$push" -> Json.obj(
              "bookmarks" -> Json.obj(
                "username" -> sessionUser
              )
            )
          )
          bookmarkResult <- posts.update(selector, modifier)
        } yield {
          if (bookmarkResult.ok) Ok("success") else BadRequest
        }
      }
    }
  }

  def removeBookmark(postId: String) = Action.async {
    { request =>
      //Checking the user has not changed his session cookie manually
      if (request.session.get("connected").isEmpty) {
        Future.apply {
          BadRequest.withNewSession
        }
      } else {
        val jsonPostId = Json.obj("$oid" -> postId)
        for {
          posts <- postsFuture(request.session.get("companyName").get)
          sessionUser = request.session.get("connected")
          selector = Json.obj("_id" -> jsonPostId)
          modifier = Json.obj(
            "$pull" -> Json.obj(
              "bookmarks" -> Json.obj(
                "username" -> sessionUser
              )
            )
          )
          bookmarkResult <- posts.update(selector, modifier)
        } yield {
          if (bookmarkResult.ok) Ok("success") else BadRequest
        }
      }
    }
  }

  def socket = WebSocket.accept[String, String] { request =>
    val sessionCompany = request.session.get("companyName").get
    val sessionUser = request.session.get("companyName").get
    ActorFlow.actorRef(out => PostWebSocketActor.props(out, sessionCompany, sessionUser))
  }

  case class InEvent(dateInSecs: String)
  case class OutEvent(dateInSecs: String)

  class PostWebSocketActor(out: ActorRef, sessionCompany: String, sessionUser: String) extends Actor {
    def receive = {
      case msg: String =>
        for {
          postsFut <- postsFuture(sessionCompany)
          date = Json.obj("$date" -> DateTime.parse(msg).getMillis)
          postSelector = Json.obj("$or" -> Json.arr(
            Json.obj("$and" -> Json.arr(Json.obj("modified" -> Json.obj("$gt" -> date)), Json.obj("creator.email" -> Json.obj("$ne" -> sessionUser)))),
            Json.obj("comments.modified" -> Json.obj("$gt" -> date))
          ))
          postscursor <- postsFut.find(postSelector).sort(Json.obj("modified" -> 1)).cursor[JsObject](ReadPreference.primary).collect[List]()
          postsObj = postscursor
        } yield {
          out ! Json.toJson(postsObj).toString
        }
    }
  }

  object PostWebSocketActor {
    def props(out: ActorRef, sessionCompany: String, sessionUser: String) = Props(new PostWebSocketActor(out, sessionCompany, sessionUser))
  }



}