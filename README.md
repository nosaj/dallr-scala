# dallr - Scala Rebuild with Tendermint Blockchain #

This application is a rebuild of www.dallr.com which is built using CakePHP 2.x, NodeJS, Socket.IO, Redis, MongoDB.


Technologies used for this rebuild contain: Scala play 2.5(using a coast to coast design pattern), Akka, Websockets, ReactJS, MongoDB(with reactive mongo driver) & Tendermint.